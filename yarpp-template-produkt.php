<?php
/*
YARPP Template: Product
Description: Used in single-produkt.php
Author: lonchbox (Pancho Pérez)
*/ ?>
<?php if ( is_tttdevice('desktop') ): ?>
	<hr>
	<div class="large-4 medium-4 columns">
		<h3 class="section-title">
			<?php _e('Was ihnen<br>vielleicht<br>auch<br>gefällt', 'callwey') ?>
		</h3>
		<h4 class="section-subtitle"><?php _e('Blogposts zum thema', 'callwey'); ?><br>
			<?php
				$book_kat = wp_get_object_terms(
	                $post->ID, 
	                array(
	                    'kategorie', 
	                    'post_tag'
	                ), 
	                array(
	                    'fields' => 'all', 
	                    'exclude' => array(19,4,9,3)
	                )
	            );
				if(!empty($book_kat)){
				  if(!is_wp_error( $book_kat )){
				    foreach($book_kat as $term){
				      echo '<a href="'.get_term_link($term->slug, 'kategorie').'">'.$term->name.'</a>'; 
				    }
				  }
				}				
			?>	
		</h4>
	</div>
	<div class="large-14 medium-14 columns">
		<div class="row">
		<ul class="medium-block-grid-3 related-posts">
<?php elseif ( is_tttdevice('tablet') ): ?>
	<div class="medium-18 columns">
		<div class="row">
		<h3 class="section-title text-center">
			<?php _e('Was ihnen vielleicht auch gefällt', 'callwey') ?>
		</h3>
		<h4 class="section-subtitle text-center"><?php _e('Blogposts zum thema', 'callwey'); ?><br>
			<?php
				$book_kat = wp_get_object_terms(
	                $post->ID, 
	                array(
	                    'kategorie', 
	                    'post_tag'
	                ), 
	                array(
	                    'fields' => 'all', 
	                    'exclude' => array(19,4,9,3)
	                )
	            );
				if(!empty($book_kat)){
				  if(!is_wp_error( $book_kat )){
				    foreach($book_kat as $term){
				      echo '<a href="'.get_term_link($term->slug, 'kategorie').'">'.$term->name.'</a>'; 
				    }
				  }
				}				
			?>	
		</h4>
		<hr>	
		<ul class="medium-block-grid-3 related-posts">
<?php else: ?>
	<div class="small-18 columns">
		<div class="row">
		<h3 class="section-title text-left">
			<?php _e('Was ihnen vielleicht auch gefällt', 'callwey') ?>
		</h3>	
		<hr>
		<ul class="small-block-grid-2 related-posts">
<?php endif; ?>
		<?php if (have_posts()):?>
			<?php while (have_posts()) : the_post(); ?>
				<?php if (has_post_thumbnail()):?>
				<li>
					<a class="entry-thumbnail text-center" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<?php the_post_thumbnail('yarpp-produkt-thumb'); ?>
					</a>
					<h2 class="entry-title text-center"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				</li>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php else: ?>
		<?php endif; wp_reset_query();?>
		</ul>
		</div>
	</div>