<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<header class="page-header medium-15 medium-centered columns">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	<div class="medium-17 medium-centered columns"><hr></div>
	<?php //if ( is_tttdevice('desktop') ): ?>	
	<div class="large-13 medium-18 columns">
		<div id="primary" class="content-area large-17 large-uncentered large-push-1 medium-17 medium-centered small-18 small-centered columns">
			<main id="main" class="site-main row" role="main">		
	<?php //else: ?>
<!--
	<div class="medium-17 medium-centered columns">
		<div id="primary" class="content-area">	
			<main id="main" class="site-main" role="main">
-->
	<?php //endif; ?>
	
				<?php while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part( 'partials/content', 'page' ); ?>
	
				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->
			<?php if ( is_tttdevice('desktop') ): ?>	
			<section id="timeline" class="row">
			<?php else: ?>
			<section id="timeline">
			<?php endif; ?>
				<div class="timeline-wrapper row">
					<h4 class="section-title"><?php _e('Historie','callwey'); ?></h4>				
					<div class="medium-9 columns">
					<?php
						if ( !is_tttdevice('mobile') ):
							$_first = true;
							$CrossOrder = new CrossOrder();
							$products_query = $CrossOrder->order('verlag_left');
						else:
							$products_mobile = array(
									'post_type'	 =>	'timeline',
									'posts_per_page' => -1,
									'post_status' => 'publish',
									'order' => 'ASC'
								);
							
							$products_query = new WP_Query($products_mobile);
						endif;
					?>
					<?php if ($products_query->have_posts()) : ?>
						<?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
						<?php $topmargin = get_post_meta( $post->ID, '_clwy_timeline_top_margin', true ); ?>
						<div class="timeline-item impar" style="margin-top:<?php echo $topmargin; ?>px">
							<div class="timeline-entry">
								<div class="timeline-date">
									<?php the_title(); ?>
									<div class="timeline-line"><div class="timeline-dot"></div></div>
								</div>
								<div class="timeline-content">
									<?php the_content(); ?>
								</div>
								<?php if ( has_post_thumbnail() ): ?>
									<?php the_post_thumbnail('single-book'); ?>
								<?php endif; ?>
							</div>
						</div>						
						<?php endwhile; ?>
					<?php endif; ?>
					</div>
					
				<?php if ( !is_tttdevice('mobile') ): ?>
					<div class="medium-9 columns">					
					<?php
						$_first = true;
						$CrossOrder = new CrossOrder();
						$products_query = $CrossOrder->order('verlag_right');
					?>
					<?php $_count = 1; ?>
					<?php if ($products_query->have_posts()) : ?>
						<?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
						<?php $topmargin = get_post_meta( $post->ID, '_clwy_timeline_top_margin', true ); ?>
						<div class="timeline-item par <?php if ($_count == 1) : ?>top-margin<?php endif; $_count++; ?>" style="margin-top:<?php echo $topmargin; ?>px">
							<div class="timeline-entry">
								<div class="timeline-date">
									<?php the_title(); ?>
									<div class="timeline-line"><div class="timeline-dot"></div></div>
								</div>
								<div class="timeline-content">
									<?php the_content(); ?>
								</div>
								<?php if ( has_post_thumbnail() ): ?>
									<?php the_post_thumbnail('single-book'); ?>
								<?php endif; ?>
							</div>
						</div>						
						<?php endwhile; ?>
					<?php endif; ?>
					</div>
				<?php endif; ?>
					<p class="timeline-footer">
						<?php _e('Die Verlagsgeschichte ist nicht abgeschlossen. Die Gestaltung neuer Themen wird sie fortschreiben. So bleibt der Verlag seinen Lesern auch in Zukunft verpflichtet.','callwey'); ?>
					</p>
				</div>
			</section>
		</div><!-- #primary -->
	</div>

	<?php get_sidebar('page'); ?>

</div>

<?php get_footer(); ?>
