<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no," />
<meta name="apple-mobile-web-app-capable" content="yes" />
<!-- Setting favicon and Apple Touch Icon -->
<link rel="apple-touch-icon" href="<?php echo of_get_option('touch_icon_iphone'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo of_get_option('touch_icon_iphone'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo of_get_option('touch_icon_iphone4'); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo of_get_option('touch_icon_ipad3'); ?>">
<link rel="icon" type="image/png" href="<?php echo of_get_option('favicon_ico'); ?>">
<link rel="canonical" href="<?php bloginfo ('url'); ?>"/>
<link rel="shortcut icon" href="<?php echo of_get_option('favicon_png'); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="author" href="<?php bloginfo ('template_url'); ?>/humans.txt" />

	<title><?php
			/*
			 * Print the <title> tag based on what is being viewed.
			 */
			global $page, $paged;
		
			wp_title( '|', true, 'right' );
		
			// Add the blog name.
			//bloginfo( 'name' );
		
			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";
		
			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Page %s', 'callwey' ), max( $paged, $page ) );
		
			?>
	</title>

<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<!--[if lt IE 9]>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.min.css">
<![endif]-->


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header" role="banner">
		<?php if ( is_tttdevice('desktop') || is_tttdevice('tablet') ): ?>
			<div class="row">
				<div class="site-branding large-3 medium-3 small-18 columns">
					<div class="row">
						<h1 id="logotype" class="site-title">
							<a class="right" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<img class="right" src="<?php echo of_get_option('logo'); ?>" border="0" alt="<?php bloginfo( 'name' ); ?>" />
							</a>
						</h1>
					</div>
					<h2 class="site-description hide"><?php bloginfo( 'description' ); ?></h2>
				</div>
				<div class="large-5 large-offset-3 medium-5 medium-offset-1 columns">
				</div>
				<div class="large-7 medium-9 columns">
				</div>
			</div>
			<div class="nav-wrapper">
				<div class="row">
				<div class="nav-inner-wrapper large-13 large-centered medium-18 small-18 columns">
				<nav id="site-navigation" class="main-navigation top-bar row" role="navigation">
					<ul class="title-area hide-for-medium-portrait">
						<!-- Title Area -->
						<li class="name">
							<h1><a href="#">Menu</a></h1>
						</li>
						<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
						<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
					</ul>
					<section class="top-bar-section">
						<?php wp_nav_menu( array( 
							'theme_location' => 'primary',
							'menu_class' => 'left',
							'container' => '',
							'container_class' => '',
						) ); ?>
					</section>
				</nav><!-- #site-navigation -->
				</div>
				</div>
			</div>
		<?php elseif ( is_tttdevice('mobile') ): ?>

			<div id="mobile-header" class="row">
				<div class="small-4 columns text-center">
					<a href="#" data-dropdown="mobile_menu_dropdown"><span aria-hidden="true" class="icon-menu"></span></a>
				</div>
				<div class="small-10 columns text-center">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php echo of_get_option('logo'); ?>" border="0" alt="<?php bloginfo( 'name' ); ?>" />
					</a>
				</div>
				<div class="small-4 columns text-center">
				</div>
			</div>

            <div id="mobile_menu_dropdown" class="mobile_menu_dropdown" data-dropdown-content>

                <nav id="site-navigation" class="main-navigation row" role="navigation">
                    <ul class="title-area hide">
                        <!-- Title Area -->
                        <li class="name">
                        </li>
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
                    </ul>		
                    <section class="top-bar-section">
                        <?php wp_nav_menu( array( 
                            'theme_location' => 'primary',
                            'menu_class' => 'left',
                            'container' => '',
                            'container_class' => '',
                        ) ); ?>
                    </section>
                </nav><!-- #site-navigation -->
            </div>
		<?php endif; ?>
	</header><!-- #masthead -->
	
	<div id="content" class="site-content">
