<?php if (is_front_page()): ?>
	<div class="row">
		<?php if ( !is_tttdevice('tablet') ): ?>
			<div class="large-13 large-centered medium-13 medium-centered small-18 columns">
		<?php else: ?>
			<div class="medium-14 medium-centered slider-medium-portrait columns">
		<?php endif; ?>
				<div id="slider-width" class="row"></div>
			</div>
	</div>
	<?php
		$_first = true;
		$CrossOrder = new CrossOrder();
		$home_slider_query = $CrossOrder->order('home_slider');
	?>
	<?php if ($home_slider_query->have_posts()) : ?>
	<div class="row slider">
		<div id="background-nearby" style="opacity: 0">

			<ul data-orbit data-options="timer_container_class:hide;timer_progress_class:hide;next_on_click:false;timer:true;animation:nearby;bullets:false;variable_height:true;slide_number:false;nearbyopacity:3;nearbyparent:#slider-width;">	
				<?php while ($home_slider_query->have_posts()) : $home_slider_query->the_post(); ?>
                        <li>
							<a class="entry-thumbnail" href="<?php the_permalink(); ?>">								
                                <?php
                                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'slider');
                                    if ($large_image_url):
                                ?>

                                <img src="<?php echo $large_image_url[0]; ?>">

                                <?php else: ?>
                                    <img src="" width="938" height="480">
                                <?php endif; ?>
                                <div class="orbit-caption">
                                    <div class="caption-container">
                                    <div class="caption">
                                    <div class="caption-box">
                                        <h2 class="entry-title">
                                            <?php the_title(); ?>
                                        </h2>
                                    </div>
                                    </div>
                                    </div>
                                </div>
							</a>
                        </li>

				<?php endwhile; ?>
			</ul>
		</div>
	</div>
	<?php endif; wp_reset_postdata();?>	
<?php endif; ?>
