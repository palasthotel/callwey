module.exports = function(grunt) {

    // All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // CSS minification
        cssmin: {
            combine: {
                options: {
                    // Remove comments
                    keepSpecialComments: 0
                },
                // Everything in src/css is combined to a single assets/css/style.css
                files: {
                  'css/style.min.css': ['css/style.css'],
                  //'css/adrotate.min.css': ['css/adrotate.css'],
                  //'css/ies.min.css': ['css/ies.css'],
                  //'css/icons.min.css': ['css/icons.css'],
                  //'css/editor.min.css': ['css/editor.css'],
                  //'css/autor-editor.min.css': ['css/autor-editor.css'],
                }
            }
        },

        // SASS Compilation
        sass: {
            dist: {
                options: {                       // Target options
                    // includePaths: [
                    //     'scss/',
                    //     'inc/foundation/scss',
                    //     'inc/foundation/ie-scss'
                    // ],
                    loadPath: require('node-bourbon').includePaths,
                    // includePaths: require('node-bourbon').includePaths,
                    style: 'expanded',
                },
                files: {
                    'css/style.css': 'scss/style.scss',
                    //'css/adrotate.css': 'scss/adrotate.scss',
                    //'css/ies.css': 'scss/ies.scss',
                    //'css/icons.css': 'scss/icons.scss',
                    //'css/editor.css': 'scss/editor.scss',
                    //'css/autor-editor.css': 'scss/autor-editor.scss',
                }
            },
        },

        watch: {
            sass: {
                files: ['scss/*.scss'],
                tasks: ['sass','cssmin'],
            },
        }
    });

    // Tell Grunt we plan to use these plugins.
    // grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Tell Grunt what to do when we type "grunt" into the terminal.
    // grunt.registerTask('localdev', ['watch']);
    // grunt.registerTask('localstyles', ['less', 'cssmin', 'concat']);
    grunt.registerTask('default', ['sass', 'cssmin']);

};
