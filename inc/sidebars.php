<?php
	register_sidebar( array(
		'name'          => __( 'General', 'callwey' ),
		'description'   => 'Show in all site in first position.',		
		'id'            => 'sidebar-general',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Home Right', 'callwey' ),
		'description'   => 'Show ONLY in home right top.',
		'id'            => 'sidebar-home-right',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Home left', 'callwey' ),
		'description'   => 'Show ONLY in home left bottom.',
		'id'            => 'sidebar-home-left',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Blog Content', 'callwey' ),
		'description'   => 'Show in Blog, Post, Rezepte, Categorie & Tag archives.',		
		'id'            => 'sidebar-blog',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Book & Ebook', 'callwey' ),
		'description'   => 'Show ONLY in Kategorie Term archives.',
		'id'            => 'sidebar-kategorie',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Pages', 'callwey' ),
		'description'   => 'Show ONLY in pages.',		
		'id'            => 'sidebar-pages',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );	
	register_sidebar( array(
		'name'          => __( 'Jobs', 'callwey' ),
		'description'   => 'Show ONLY JOBS page template.',		
		'id'            => 'sidebar-jobs',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );	
	register_sidebar( array(
		'name'          => __( 'Presse', 'callwey' ),
		'description'   => 'Show ONLY Presse page template.',		
		'id'            => 'sidebar-presse',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Buchhandel', 'callwey' ),
		'description'   => 'Show ONLY Buchhandel page template.',		
		'id'            => 'sidebar-buchhandel',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Tablet', 'callwey' ),
		'description'   => 'Home sidebar JUST for tablet devices.',		
		'id'            => 'sidebar-tablet',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Mobile', 'callwey' ),
		'description'   => 'Home sidebar JUST for mobile devices.',		
		'id'            => 'sidebar-mobile',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-container">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
?>