<?php

class TTTRezeptes_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Rezepte','description=rezept des tages.');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
		?>
			<?php

                $name_monat[1]    =  "Januar"; 
                $name_monat[2]    =  "Februar"; 
                $name_monat[3]    =  "März"; /* M? d?aber auch funktionieren */ 
                $name_monat[4]    =  "April"; 
                $name_monat[5]    =  "Mai"; 
                $name_monat[6]    =  "Juni"; 
                $name_monat[7]    =  "Juli"; 
                $name_monat[8]    =  "August"; 
                $name_monat[9]    =  "September"; 
                $name_monat[10]  =  "Oktober"; 
                $name_monat[11]  =  "November"; 
                $name_monat[12]  =  "Dezember"; 

				$recepi = array(
						'post_type'	 =>	'rezepte',
						'posts_per_page' => 1,
                        'ignore_sticky_posts' => true,
                       /*
 'year'  => date('Y'),
                        'month' => date('m'),
                        'day'   => date('d'),
*/
			    );
				
				$recepi_query = new WP_Query($recepi);
			?>
			<?php if ($recepi_query->have_posts()) : ?>
				<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
					<div class="medium-6 small-9 columns">
				<?php endif; ?>		    
					<aside id="day-recipe" class="widget">
						<div class="widget-container">
							<h4 class="widget-title"><?php _e('REZEPT<br>DER WOCHE', 'callwey'); ?></h4>
						<?php while ($recepi_query->have_posts()) : $recepi_query->the_post(); ?>
		                    <?php
		                    $post_time = get_the_time('U',get_the_ID());
		                    ?>
		
		
							<div class="widget-recipe" itemscope itemtype="http://schema.org/Recipe">
								<a class="recipe-thumbnail" href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('recipe-widget', array('itemprop' => 'image')); ?>
									<span class="recipe-meta"><?php echo mb_substr($name_monat[ (int) date('m',$post_time) ],0,3); ?> <?php echo date('d', $post_time);?></span>
								</a>
								<h3 class="recipe-title" itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		
								<meta itemprop="datePublished" content="<?php echo date(DateTime::ISO8601, $post_time); ?>">
							</div>
						<?php endwhile; ?>
						</div>
					</aside>
				<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>			
		<?php
        }

}
register_widget( 'TTTRezeptes_widget' );

?>
