<?php

class TTTAuthorTagebuch_widget extends WP_Widget {
    public function __construct() {
        // widget actual processes
        parent::WP_Widget(false,'TTT Autor Tagebuch','description=Blog Posts Author promo widget.');
    }
    
    public function form( $instance ) {
        
        $author = (int) esc_attr($instance['author']);

        $query_args = wp_array_slice_assoc( $args, array( 'orderby', 'order', 'login' ) );
        $query_args['fields'] = 'ids';
        $authors = get_users( $query_args );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('author'); ?>">
                <?php _e('Author:'); ?>
            </label>
            
            <select name="<?php echo $this->get_field_name('author'); ?>">
            <?php foreach( $authors as $key => $value ): ?>
                <?php $user_info = get_userdata( $value ); ?>
                <?php if ( $author == $value ): ?>
                    <option value="<?php echo $value; ?>" selected><?php echo $user_info->display_name; ?></option>
                <?php else: ?>
                    <option value="<?php echo $value; ?>"><?php echo $user_info->display_name; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
            </select>
        </p>
        <?php   
    }
    
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['author'] = strip_tags($new_instance['author']);
        return $instance;
    }
    
    public function widget( $args, $instance ) {
    ?>
	<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
		<div class="medium-6 small-9 columns">
	<?php endif; ?>		    
        <aside id="autor-tagebuch" class="widget">
        	<div class="widget-container text-center">
        		<?php $user_info = get_userdata( (int) $instance['author'] );
        			$vuser_id = $user_info-> ID;
        			$first_name = $user_info-> user_firstname;
        			$last_name = $user_info-> user_lastname;
					$headline = $user_info-> headline;
					$underline = $user_info-> underline;					        			
        		?>
        		<h4 class="widget-title">
        			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID', $instance['author'] ) ); ?>">
        				<?php if($headline !== ''): ?>
	        				<?php echo $headline; ?>
        				<?php else: ?>
        					<?php echo $first_name; ?>
	        			<?php endif; ?>
        			</a>
        		</h4>
        		<a class="autor-avatar" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID', $instance['author'] ) ); ?>">
					<?php $local_avatars = get_user_meta( $vuser_id, 'simple_local_avatar', true ); ?>					
					<?php if (is_tttdevice('desktop') ): ?>
						<?php echo get_simple_local_avatar(get_the_author_meta( 'user_email', $instance['author'] ), apply_filters( '_s_author_bio_avatar_size', 220 ) ); ?>
						<?php //$image_attributes = wp_get_attachment_image_src( $local_avatars['media_id'], 'slideshow-widget' ); // returns an array ?>
					<?php elseif (is_tttdevice('tablet') ): ?>
						<?php $image_attributes = wp_get_attachment_image_src( $local_avatars['media_id'], 'autor-widget' ); // returns an array ?>
						<img src="<?php echo $image_attributes[0]; ?>" width="<?php echo $image_attributes[1]; ?>" height="<?php echo $image_attributes[2]; ?>">
					<?php endif; ?>
        		</a>
        		<h4 class="widget-subtitle">
        			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID', $instance['author'] ) ); ?>">
        				<?php if($underline !== ''): ?>
							<?php echo $underline; ?>
        				<?php else: ?>
        					<?php echo $lastname_name; ?>
	        			<?php endif; ?>
					</a>
				</h4>
        	</div>
        </aside>
	<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
		</div>
	<?php endif; ?>		
    <?php
    }
    
}
register_widget( 'TTTAuthorTagebuch_widget' );

?>
