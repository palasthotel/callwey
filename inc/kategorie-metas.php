<?php
/**
 * Registering meta sections for taxonomies
 *
 * All the definitions of meta sections are listed below with comments, please read them carefully.
 * Note that each validation method of the Validation Class MUST return value.
 *
 * You also should read the changelog to know what has been changed
 *
 */

// Hook to 'admin_init' to make sure the class is loaded before
// (in case using the class in another plugin)
add_action( 'admin_init', 'buch_kategorie_register_taxonomy_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function buch_kategorie_register_taxonomy_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Taxonomy_Meta' ) )
		return;

	$meta_sections = array();

	// First meta section
	$meta_sections[] = array(
		'title'      => 'Thumbnail size',             // section title
		'taxonomies' => array('kategorie'), // list of taxonomies. Default is array('category', 'post_tag'). Optional
		'id'         => 'kategorie_thumbnail_size',                 // ID of each section, will be the option name

		'fields' => array(                             // List of meta fields
			// SELECT
			array(
				'name'    => 'Select Thumbnail Size',
				'id'      => 'select_thumb_size',
				'type'    => 'select',
				'options' => array(                     // Array of value => label pairs for radio options
					'kategorie-portrait' => 'Portrait',
					'kategorie-landscape' => 'Landscape',
					'kategorie-half' => 'Half',
					'kategorie-quarter' => 'Quarter',
					'kategorie-square' => 'Square',
					'kategorie-full' => 'Full'
				),
			),
			// IMAGE
			array(
				'name' => 'Upload Image',
				'id'   => 'kategorie_image',
				'type' => 'image',
			),	
			// FILE
/*
			array(
				'name' => 'Upload File',
				'id'   => 'file',
				'type' => 'file',
			),					
*/
		),
	);

	foreach ( $meta_sections as $meta_section )
	{
		new RW_Taxonomy_Meta( $meta_section );
	}
}
