<?php

function swipebox_enqueue() {
    wp_enqueue_style( 'swipebox', get_template_directory_uri() . '/inc/swipebox/source/swipebox' );
	wp_enqueue_script('swipebox', get_template_directory_uri() . '/inc/swipebox/source/jquery.swipebox.js' );	
}	
	
add_action('wp_enqueue_scripts', 'swipebox_enqueue');
