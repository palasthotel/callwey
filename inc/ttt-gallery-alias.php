<?php

function custom_ttt_gallery_alias() {
    add_shortcode('callwey-image','custom_ttt_galley_alias_fn',224234);
}
add_action('init','custom_ttt_gallery_alias');


function custom_ttt_galley_alias_fn( $args ) {
    if (!is_array($args)) {
        return false;
    }
    $_shortcode = '';
    $_size = false;

    if (!isset($args['template'])) {
        $args['template'] = 'oneimage';
    }

    foreach ($args as $key => $value ) {
        if ( is_numeric($key) && in_array($value,array('landscape','portrait')) ) {
            $_size = $value;
            $_shortcode .= " ".'callweysize="'.$value.'"';
        }
        else {
            $_shortcode .= " ".$key.'="'.$value.'"';
        }
    }

    return do_shortcode('[ttt-gallery-image '.$_shortcode.']');


}


