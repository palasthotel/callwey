<?php
	/**
	 * Enable support for Post Thumbnails
	 */
	add_theme_support( 'post-thumbnails', array('page','post','rezepte','produkt','autor','fotografen','staff','ebook','timeline') );
	add_image_size( 'slider', 938, 480, true);
	add_image_size( 'book-home', 170, 400 ); /* bestseller */
	add_image_size( 'recipe-widget', 258, 290, true); 
	add_image_size( 'slideshow-widget', 273, 170, true);
	add_image_size( 'autor-widget', 262, 324, true);
	add_image_size( 'autor-single', 182, 182, true);		
	add_image_size( 'masonry-home', 234, 600 );
	add_image_size( 'magazin-home', 177, 400 );
	add_image_size( 'single', 737, 570, true );
	add_image_size( 'single-landscape', 737, 492, true );
	add_image_size( 'single-portrait', 363, 497, true );		
	add_image_size( 'single-book', 442, 800 );
	add_image_size( 'small-book', 150, 800 );		
	add_image_size( 'blog-posts', 737, 374, true );	
	add_image_size( 'yarpp-thumb', 200, 202, true );
	add_image_size( 'yarpp-produkt-thumb', 290, 202, true );	
	add_image_size( 'post-nav', 189, 94, true );	
	add_image_size( 'book-autor-fotog', 282, 282, true );
	add_image_size( 'book-gallery-portrait', 584, 800, true );
	add_image_size( 'book-gallery-landscape', 584, 390, true );	
	add_image_size( 'kategorie-landscape', 719, 570, true );
	add_image_size( 'kategorie-portrait', 450, 570, true );	
	add_image_size( 'kategorie-half', 584, 570, true );
	add_image_size( 'kategorie-quarter', 282, 570, true );
	add_image_size( 'kategorie-square', 282, 265, true );
	add_image_size( 'kategorie-full', 1189, 450, true );	
	add_image_size( 'page', 517, 800 );		
	add_image_size( 'staff', 310, 382, true );
	add_image_size( 'fullscreen', 2000, 1200 );
	add_image_size( 'fullscreen-mobile', 1500, 800 );

function local_ttt_crop_human_name($name) {
    switch( $name ) {
        case 'single-landscape';
            return 'Single Post landscape'; break;
        case 'recipe-widget';
            return 'Rezepte Widget'; break;
        case 'autor-widget';
            return 'Autor Widget'; break;
        case 'autor-single';
            return 'Autor Landingpage'; break;
        case 'masonry-home';
            return 'News-Stream'; break;
        case 'magazin-home';
            return 'Zeitschriften Thumbs Home'; break;
        case 'single-landscape';
            return 'Blogpost querformat & Blogpost Timeline'; break;
        case 'single-portrait';
            return 'Blogpost hochformat'; break;
/*
        case 'blog-posts';
            return 'Blogpost Timeline'; break;
*/
        case 'yarpp-thumb';
            return 'Blog Empfehlung Thumbs'; break;
        case 'post-nav';
            return 'Vorherige und letzte Blogpost'; break;
        case 'book-gallery-portrait';
            return 'Bücher Landingpage hochformat'; break;
        case 'book-gallery-landscape';
            return 'Bücher Landingpage querformat'; break;
/*
        case 'kategorie-landscape';
            return 'Blogpost Timeline'; break;
        case 'kategorie-portrait';
            return 'Blogpost Timeline'; break;
        case 'kategorie-quarter';
            return 'Blogpost Timeline'; break;
        case 'kategorie-square';
            return 'Blogpost Timeline'; break;
        case 'kategorie-full';
            return 'Blogpost Timeline'; break;
*/
        case 'staff';
            return 'Organisation Fotos'; break;
/*
        case 'fullscreen';
            return 'Blogpost Timeline'; break;
        case 'fullscreen-mobile';
            return 'Blogpost Timeline'; break;
*/

        default:
            return $name; break;
    }
}
add_filter('ttt_crop_human_name','local_ttt_crop_human_name');



/* Filter thumbnails edited sizes by CPTs */


add_action('init','custom_tttgallery_init',0);

function custom_tttgallery_init() {
    
    global $pagenow;

    add_filter('tttgallery-query_attachements_query', 'custom_tttgallery_query_attachements_query', 1);
    add_filter('tttgallery-query_attachements_arraymap', 'custom_tttgallery_query_attachements_arraymap');

    if( 'admin-ajax.php' == $pagenow && ( $_REQUEST['action'] == 'query-attachments' or $_REQUEST['action'] == 'save-attachment-compat' ) && isset($_REQUEST['post_id']) ) {
        $post_type = get_post_type($_REQUEST['post_id']);
        if ( $post_type == 'produkt' || $post_type == 'ebook' ) {
            remove_all_actions('wp_ajax_query-attachments');
            remove_all_actions('wp_ajax_nopriv_query-attachments');
            add_action('wp_ajax_query-attachments','custom_wp_ajax_query_attachments',1);
            add_action('wp_ajax_nopriv_query-attachments','custom_wp_ajax_query_attachments',1);

            
            add_action('edit_attachment', 'my_save_attachment_produtk_format' );
            add_filter('attachment_fields_to_edit', 'my_add_attachment_produtk_format_field', 10, 2 );
        }
        elseif ( $post_type == 'post' || $post_type == 'rezepte' ) {
            // remove_all_actions('wp_ajax_query-attachments');
            // remove_all_actions('wp_ajax_nopriv_query-attachments');
            // add_action('wp_ajax_query-attachments','custom_wp_ajax_query_attachments',1);
            // add_action('wp_ajax_nopriv_query-attachments','custom_wp_ajax_query_attachments',1);

            add_action('edit_attachment', 'my_save_attachment_post_format' );
            add_filter('attachment_fields_to_edit', 'my_add_attachment_post_format_field', 10, 2 );
            add_filter('attachment_fields_to_edit', 'my_add_attachment_post_author_field', 10, 2 );
        }

    }
    elseif ( 'admin-ajax.php' == $pagenow && $_REQUEST['action'] == 'ttt-crop_load' && isset($_REQUEST['post_id']) ) {
        $post_type = get_post_type($_REQUEST['post_id']);
		/* Calling functions for each CPT filtered sizes */
        if ( $post_type == 'produkt' || $post_type == 'ebook' ) {
            add_filter('tttcrop_image_sizes','custom_tttcrop_image_sizes_produkt');
        }
        elseif ( $post_type == 'fotografen') {
            add_filter('tttcrop_image_sizes','custom_tttcrop_image_sizes_fotografen');
        }
        elseif ( $post_type == 'autor') {
            add_filter('tttcrop_image_sizes','custom_tttcrop_image_sizes_autor');
        }
        elseif ( $post_type == 'post') {
            add_filter('tttcrop_image_sizes','custom_tttcrop_image_sizes_post');
        }
        elseif ( $post_type == 'rezepte') {
            add_filter('tttcrop_image_sizes','custom_tttcrop_image_sizes_rezepte');
        }

    }

}

/* CPT-Produkt just use portrati & thumbnail for gallery */

function custom_tttcrop_image_sizes_produkt($sizes) {

    foreach ($sizes as $key => $values) {
        if ($key == 'book-gallery-portrait')
            $new[ $key ] = $values;
        elseif ($key == 'book-gallery-landscape')
            $new[ $key ] = $values;
        elseif ($key == 'magazin-home')
            $new[ $key ] = $values;
    }

    return $new;
}
/* CPT-fotografen & CTP-autor use only one size */
function custom_tttcrop_image_sizes_autor($sizes) {

    foreach ($sizes as $key => $values) {
        if ($key == 'book-autor-fotog')
            $new[ $key ] = $values;
    }

    return $new;
}
function custom_tttcrop_image_sizes_fotografen($sizes) {

    foreach ($sizes as $key => $values) {
        if ($key == 'book-autor-fotog')
            $new[ $key ] = $values;
        elseif ($key == 'autor-single')
            $new[ $key ] = $values;            
    }

    return $new;
}
function custom_tttcrop_image_sizes_post($sizes) {

    foreach ($sizes as $key => $values) {
        if ($key == 'slider')
            $new[ $key ] = $values;
        elseif ($key == 'masonry-home')
            $new[ $key ] = $values;
/*
        elseif ($key == 'blog-posts')
            $new[ $key ] = $values;
*/
        elseif ($key == 'single')
            $new[ $key ] = $values;
        elseif ($key == 'yarpp-thumb')
            $new[ $key ] = $values;
        elseif ($key == 'single-portrait')
            $new[ $key ] = $values;
        elseif ($key == 'single-landscape')
            $new[ $key ] = $values;
        // elseif ($key == 'post-nav')
        //     $new[ $key ] = $values;
    }

    return $new;
}
function custom_tttcrop_image_sizes_rezepte($sizes) {

    foreach ($sizes as $key => $values) {
        if ($key == 'recipe-widget')
            $new[ $key ] = $values;
        elseif ($key == 'blog-posts')
            $new[ $key ] = $values;            
        elseif ($key == 'yarpp-produkt-thumb')
            $new[ $key ] = $values;
        elseif ($key == 'single-landscape')
            $new[ $key ] = $values;
        elseif ($key == 'single-portrait')
            $new[ $key ] = $values;
    }

    return $new;
}


function custom_tttgallery_query_attachements_arraymap($arraymap) {
    return 'custom_wp_prepare_attachment_for_js';
}

/* Attach more content to the gallery */
function custom_tttgallery_query_attachements_query($args) {
    $args['post_type'] = array('attachment','produkt','video','ebook');
    $args['post_status'] .= ',publish';
    return $args;

}

/**
 * Query for attachments.
 *
 * @since 3.5.0
 */
function custom_wp_ajax_query_attachments() {
    if ( ! current_user_can( 'upload_files' ) )
        wp_send_json_error();

    $query = isset( $_REQUEST['query'] ) ? (array) $_REQUEST['query'] : array();
    $query = array_intersect_key( $query, array_flip( array(
        's', 'order', 'orderby', 'posts_per_page', 'paged', 'post_mime_type',
        'post_parent', 'post__in', 'post__not_in',
    ) ) );
   
    unset($query['post_mime_type']);
    $query['post_type'] = array('attachment','produkt','video','ebook');
    // $query['post_type'] = array('produkt');
    $query['post_status'] = 'inherit,publish';
    if ( current_user_can( get_post_type_object( 'attachment' )->cap->read_private_posts ) )
        $query['post_status'] .= ',private';

    $query = new WP_Query( $query );

    $posts = array_map( 'custom_wp_prepare_attachment_for_js', $query->posts );
    $posts = array_filter( $posts );

    wp_send_json_success( $posts );
}

/**
 * Prepares an attachment post object for JS, where it is expected
 * to be JSON-encoded and fit into an Attachment model.
 *
 * @since 3.5.0
 *
 * @param mixed $attachment Attachment ID or object.
 * @return array Array of attachment details.
 */
function custom_wp_prepare_attachment_for_js( $attachment ) {
	if ( ! $attachment = get_post( $attachment ) )
		return;

    // if ( 'attachment' != $attachment->post_type )
    // 	return;

	$meta = wp_get_attachment_metadata( $attachment->ID );
	if ( false !== strpos( $attachment->post_mime_type, '/' ) )
		list( $type, $subtype ) = explode( '/', $attachment->post_mime_type );
	else
		list( $type, $subtype ) = array( $attachment->post_mime_type, '' );

	$attachment_url = wp_get_attachment_url( $attachment->ID );

	$response = array(
		'id'          => $attachment->ID,
		'title'       => $attachment->post_title,
		'filename'    => wp_basename( $attachment->guid ),
		'url'         => $attachment_url,
		'link'        => get_attachment_link( $attachment->ID ),
		'alt'         => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'author'      => $attachment->post_author,
		'description' => $attachment->post_content,
		'caption'     => $attachment->post_excerpt,
		'name'        => $attachment->post_name,
		'status'      => $attachment->post_status,
		'uploadedTo'  => $attachment->post_parent,
		'date'        => strtotime( $attachment->post_date_gmt ) * 1000,
		'modified'    => strtotime( $attachment->post_modified_gmt ) * 1000,
		'menuOrder'   => $attachment->menu_order,
		'mime'        => $attachment->post_mime_type,
		'type'        => ( empty($type) ? $attachement->post_type : $type),
        'post_type'   => $attachement->post_type,
		'subtype'     => $subtype,
		'icon'        => wp_mime_type_icon( $attachment->ID ),
		'dateFormatted' => mysql2date( get_option('date_format'), $attachment->post_date ),
		'nonces'      => array(
			'update' => false,
			'delete' => false,
		),
		'editLink'   => false,
	);

    if ( 'attachment' != $attachment->post_type ) {
        if ( $response['type'] != 'video' ) {
            $response['type'] = $attachment->post_type;
            $response['icon'] = wp_mime_type_icon( $attachment->ID );
            $response['filename'] = $response['type'].': '.$attachment->post_title;
            $response['icon'] =  get_template_directory_uri().'/icons/'.$response['type'].'.png';
        }
    }

	if ( current_user_can( 'edit_post', $attachment->ID ) ) {
		$response['nonces']['update'] = wp_create_nonce( 'update-post_' . $attachment->ID );
		$response['editLink'] = get_edit_post_link( $attachment->ID, 'raw' );
	}

	if ( current_user_can( 'delete_post', $attachment->ID ) )
		$response['nonces']['delete'] = wp_create_nonce( 'delete-post_' . $attachment->ID );

	if ( $meta && 'image' === $type ) {
		$sizes = array();
		$possible_sizes = apply_filters( 'image_size_names_choose', array(
			'thumbnail' => __('Thumbnail'),
			'medium'    => __('Medium'),
			'large'     => __('Large'),
			'full'      => __('Full Size'),
		) );
		unset( $possible_sizes['full'] );

		// Loop through all potential sizes that may be chosen. Try to do this with some efficiency.
		// First: run the image_downsize filter. If it returns something, we can use its data.
		// If the filter does not return something, then image_downsize() is just an expensive
		// way to check the image metadata, which we do second.
		foreach ( $possible_sizes as $size => $label ) {
			if ( $downsize = apply_filters( 'image_downsize', false, $attachment->ID, $size ) ) {
				if ( ! $downsize[3] )
					continue;
				$sizes[ $size ] = array(
					'height'      => $downsize[2],
					'width'       => $downsize[1],
					'url'         => $downsize[0],
					'orientation' => $downsize[2] > $downsize[1] ? 'portrait' : 'landscape',
				);
			} elseif ( isset( $meta['sizes'][ $size ] ) ) {
				if ( ! isset( $base_url ) )
					$base_url = str_replace( wp_basename( $attachment_url ), '', $attachment_url );

				// Nothing from the filter, so consult image metadata if we have it.
				$size_meta = $meta['sizes'][ $size ];

				// We have the actual image size, but might need to further constrain it if content_width is narrower.
				// Thumbnail, medium, and full sizes are also checked against the site's height/width options.
				list( $width, $height ) = image_constrain_size_for_editor( $size_meta['width'], $size_meta['height'], $size, 'edit' );

				$sizes[ $size ] = array(
					'height'      => $height,
					'width'       => $width,
					'url'         => $base_url . $size_meta['file'],
					'orientation' => $height > $width ? 'portrait' : 'landscape',
				);
			}
		}

		$sizes['full'] = array( 'url' => $attachment_url );

		if ( isset( $meta['height'], $meta['width'] ) ) {
			$sizes['full']['height'] = $meta['height'];
			$sizes['full']['width'] = $meta['width'];
			$sizes['full']['orientation'] = $meta['height'] > $meta['width'] ? 'portrait' : 'landscape';
		}

		$response = array_merge( $response, array( 'sizes' => $sizes ), $sizes['full'] );
	} elseif ( $meta && 'video' === $type ) {
		if ( isset( $meta['width'] ) )
			$response['width'] = (int) $meta['width'];
		if ( isset( $meta['height'] ) )
			$response['height'] = (int) $meta['height'];
	}

	if ( $meta && ( 'audio' === $type || 'video' === $type ) ) {
		if ( isset( $meta['length_formatted'] ) )
			$response['fileLength'] = $meta['length_formatted'];
	}

	if ( function_exists('get_compat_media_markup') )
		$response['compat'] = get_compat_media_markup( $attachment->ID, array( 'in_modal' => true ) );

	return apply_filters( 'wp_prepare_attachment_for_js', $response, $attachment, $meta );
}

function my_add_attachment_produtk_format_field( $form_fields, $post ) {
    $field_value = get_post_meta( $post->ID, 'produtk_format', true );
    $form_fields['produtk_format'] = array(
        'label' => __( 'Produtk format' ),
        'helps' => __( 'Set the image format at produkt page' ),
        'input' => 'html',
        'html' => "
            <select name='attachments[{$post->ID}][produtk_format]' id='attachments[{$post->ID}][produtk_format]'>
                <option value='' ".( $field_value == '' ? 'selected' : '' )."></option>
                <option value='book-gallery-portrait' ".( $field_value == 'book-gallery-portrait' ? 'selected' : '').">portrait</option>
                <option value='book-gallery-landscape' ".( $field_value == 'book-gallery-landscape' ? 'selected' : '').">landscape</option>
            </select>",
    );
    return $form_fields;
}

function my_save_attachment_produtk_format( $attachment_id ) {
    if ( isset( $_REQUEST['attachments'][$attachment_id]['produtk_format'] ) ) {
        $location = $_REQUEST['attachments'][$attachment_id]['produtk_format'];
        update_post_meta( $attachment_id, 'produtk_format', $location );
    }
}


function my_add_attachment_post_format_field( $form_fields, $post ) {
    $field_value = get_post_meta( $post->ID, 'post_format', true );
    $form_fields['post_format'] = array(
        'label' => __( 'Image format' ),
        'helps' => __( 'Set the image format at post' ),
        'input' => 'html',
        'html' => "
            <select name='attachments[{$post->ID}][post_format]' id='attachments[{$post->ID}][post_format]'>
                <option value='' ".( $field_value == '' ? 'selected' : '' )."></option>
                <option value='single-portrait' ".( $field_value == 'single-portrait' ? 'selected' : '').">portrait</option>
                <option value='single-landscape' ".( $field_value == 'single-landscape' ? 'selected' : '').">landscape</option>
                <option value='single-portrait-double' ".( $field_value == 'single-portrait-double' ? 'selected' : '').">portrait double</option>                
            </select>",
    );
    return $form_fields;
}

function my_add_attachment_post_author_field( $form_fields, $post ) {
    $field_value = get_post_meta( $post->ID, 'photo_author', true );
    $form_fields['photo_author'] = array(
        'label' => __( 'Author' ),
        'helps' => __( 'Set the name of the photo author' ),
        'input' => 'text',
        'value' => $field_value
        // 'html' => "
        //     <select name='attachments[{$post->ID}][post_format]' id='attachments[{$post->ID}][post_format]'>
        //         <option value='' ".( $field_value == '' ? 'selected' : '' )."></option>
        //         <option value='single-portrait' ".( $field_value == 'single-portrait' ? 'selected' : '').">portrait</option>
        //         <option value='single-landscape' ".( $field_value == 'single-landscape' ? 'selected' : '').">landscape</option>
        //         <option value='single-portrait-double' ".( $field_value == 'single-portrait-double' ? 'selected' : '').">portrait double</option>                
        //     </select>",
    );
    return $form_fields;
}

function my_save_attachment_post_format( $attachment_id ) {
    if ( isset( $_REQUEST['attachments'][$attachment_id]['post_format'] ) ) {
        $location = $_REQUEST['attachments'][$attachment_id]['post_format'];
        update_post_meta( $attachment_id, 'post_format', $location );
    }
    if ( isset( $_REQUEST['attachments'][$attachment_id]['photo_author'] ) ) {
        $location = $_REQUEST['attachments'][$attachment_id]['photo_author'];
        update_post_meta( $attachment_id, 'photo_author', $location );
    }
}





?>
