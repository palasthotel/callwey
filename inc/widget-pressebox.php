<?php
class TTTPressebox_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Pressebox','description=Display the Pressebox content. Order by CrossOrder filter.');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
			global $post;        
		?>
		<?php if (is_tttdevice('tablet') ): ?>
			<div class="medium-6 columns">
		<?php endif; ?>		
			<aside id="pressebox" class="widget <?php if (is_tttdevice('tablet') ): ?>medium-6 columns<?php endif; ?>">
				<div class="widget-container">
					<h4 class="widget-title"><?php _e('Aktuelle<br>Presse<br>mitteilungen', 'callwey'); ?></h4>				
					<?php
						$_first = true;
						$CrossOrder = new CrossOrder();
						$the_query = $CrossOrder->order('pressebox');
					?>
					<?php if ($the_query->have_posts()) : ?>
					<ul class="no-bullet">
						<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
							<?php
								$text = get_post_meta( $post->ID, '_clwy_update_text', true );
								$link = get_post_meta( $post->ID, '_clwy_update_link', true );
							?>
							<li>
							<article class="update-entry">
								<h3 class="update-title">
									<?php if ( get_post_meta( $post->ID, '_clwy_update_link', true ) ): ?>
										<a href="<?php echo $link; ?>"><?php the_title(); ?></a>
									<?php else: ?>
										<?php the_title(); ?>
									<?php endif; ?>
								</h3>
								<div class="update-date">
									<?php the_date('j.m.Y', '', ''); ?>
									<?php //the_time(get_option('j.m.Y')); ?>
								</div>
								<div class="update-content">
									<?php echo $text; ?>
								</div>
							</article>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') ): ?>
			</div>
		<?php endif; ?>		
		<?php
        }

}
register_widget( 'TTTPressebox_widget' );

?>