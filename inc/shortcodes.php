<?php

function shortcode_column_formater( $attr, $content ) {

	//$_content = preg_split("/[(&#8212;)\-]{3,99}[\n(<br>)]*/m",$content);

	$_content = explode("[column-divider]",$content);

    foreach( array(0,1,2,3) as $i ) {
        if (!isset($_content[$i])) continue;
        $_content[$i] = preg_replace('/<(\/p|p)>/','',$_content[$i]);
        $_content[$i] = preg_replace('/\/p>/','',$_content[$i]);
        $_content[$i] = preg_replace('/^(<br>)+/','',$_content[$i]);
        $_content[$i] = preg_replace('/^(<br\s*\/>)+/','',$_content[$i]);
        $_content[$i] = preg_split('/\n+/',$_content[$i]);
        $_content[$i] = '<p>'.implode('</p><p>',$_content[$i]).'</p>';
    }

	return $_content;
}


add_shortcode( 'column-half', 'shortcode_column_half' );
function shortcode_column_half( $attr, $content = null ) {

	$_content = shortcode_column_formater( $attr, $content);

	ob_start();
	?>
	<div class="editor-columns row">
		<div class="medium-9 columns">
			<?php echo do_shortcode($_content[0]); ?>
		</div>
		<div class="medium-9 columns">
			<p><?php echo do_shortcode($_content[1]); ?></p>
		</div>
	</div>
	<br/>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

add_shortcode( 'three-columns', 'shortcode_three_columns' );
function shortcode_three_columns( $attr, $content = null ) {

	$_content = shortcode_column_formater( $attr, $content);

	ob_start();
	?>
	<div class="editor-columns row">
		<ul class="large-block-grid-3">
			<li><p><?php echo do_shortcode($_content[0]); ?></p></li>
			<li><p><?php echo do_shortcode($_content[1]); ?></p></li>
			<li><p><?php echo do_shortcode($_content[2]); ?></p></li>
		</ul>
	</div>
	<br/>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}


add_shortcode( 'button', 'shortcode_button' );
function shortcode_button( $attr, $content = null ) {

	ob_start();
	?>
	<a class="button button-noshadow round" target="_blank" href="<?php echo $attr['href']; ?>"><?php echo $content; ?></a>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

add_shortcode( 'button-small', 'shortcode_button_small' );
function shortcode_button_small( $attr, $content = null ) {

	ob_start();
	?>
	<a class="button small button-noshadow round" target="_blank" href="<?php echo $attr['href']; ?>"><?php echo $content; ?></a>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}


add_shortcode( 'divider', 'shortcode_divider' );
function shortcode_divider( $attr, $content = null ) {

	ob_start();
	?>
	<hr/>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

add_shortcode( 'section-title', 'shortcode_section_title' );
function shortcode_section_title( $attr, $content = null ) {

	ob_start();
	?>
	<h3 class="section-title"><?php echo $content; ?></h3>
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

?>
