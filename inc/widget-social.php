<?php

class TTTSocial_links_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Social Links','description=Social networks links.');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
		?>
		<?php if (is_tttdevice('tablet') ): ?>
			<div class="medium-6 columns">
        <?php endif; ?>
            <style>
            aside#social-widget form.custom.newsletter button.button.button-newsletter {
                padding: 8px 13px 7px;
                margin: 15px 0 0;
                border: 0;
                font-size: 1em;
                letter-spacing: .15385em;
            }
            </style>
			<aside id="social-widget" class="widget">
				<div class="widget-container">
					<h4 class="widget-title hide">Social networks</h4>
					<ul class="inline-list social-networks">
						<?php if ( of_get_option('fb_page') ): ?>				
						<li>
							<a href="<?php echo of_get_option('fb_page'); ?>" target="_blank" aria-hidden="true" data-icon class="icon-facebook"></a>
						</li>
						<?php endif; ?>
						<?php if ( of_get_option('twitter') ): ?>				
						<li>
							<a href="<?php echo of_get_option('twitter'); ?>" target="_blank" aria-hidden="true" class="icon-twitter"></a>
						</li>
						<?php endif; ?>
						<?php if ( of_get_option('youtube') ): ?>				
						<li>
							<a href="<?php echo of_get_option('youtube'); ?>" target="_blank" aria-hidden="true" data-icon class="icon-youtube"></a>
						</li>
						<?php endif; ?>
                    </ul>
                    <form class="custom newsletter" name="ProfileForm" onsubmit="return CheckInputs(this);" action="http://news.callwey.de/u/register.php" method="get">
						<fieldset>
						    <legend><?php _e('Newsletter','callwey'); ?></legend>
                            <label><?php _e('Erhalten Sie per Email Informationen <br> rund um die Bücher aus dem Callwey Verlag.', 'callwey'); ?></label>

                            <input type="hidden" name="CID" value="128435595">
                            <input type="hidden" name="SID" value="<? echo $SID; ?>">
							<input type="hidden" name="UID" value="<? echo $UID; ?>">
							<input type="hidden" name="f" value="665">
							<input type="hidden" name="p" value="2">
							<input type="hidden" name="a" value="r">
							<input type="hidden" name="el" value="<? echo $el; ?>">
                            <input type="hidden" name="endlink" value="<?php echo get_bloginfo('url').'/newsletter/'; ?>">
							<input type="hidden" name="llid" value="<? echo $llid; ?>">
							<input type="hidden" name="c" value="<? echo $c; ?>">
							<input type="hidden" name="counted" value="<? echo $counted; ?>">
							<input type="hidden" name="RID" value="<? echo $RID; ?>">
							<input type="hidden" name="mailnow" value="<? echo $mailnow; ?>">
                            
                            <input type="text" class="round" name="inp_3" maxlength="255" value="<? echo $inp_3; ?>" placeholder="<?php _e('IHRE E-MAIL-ADRESSE','callwey'); ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IHRE E-MAIL-ADRESSE'">
							<input type="hidden" name="<?php echo of_get_option('newsletter_inp'); ?>" value="<?php echo of_get_option('newsletter_inp_value'); ?>">


<?php
// $ints = array();
// function cmp($a, $b) {
//     if (strlen($a) == strlen($b)) {
//         return 0;
//     } 
//     return (strlen($a) > strlen($b)) ? -1 : 1;
// }
// uasort($ints, "cmp");
// $myints = array();
// foreach($ints as $key => $value) {
//     $pos = strpos($interest[0], $value);
//     if($pos !== false) {
//         $myints[] = $key;$interest[0] = substr($interest[0],0,$pos) . substr($interest[0],$pos+strlen($value),strlen($interest[0]));
//     }
// }
?>
                            <!--
							<div class="medium-18 columns">
								<div class="meidum-18 columns">
                                    <input type="checkbox" name="optin" value="y" <? echo $optin == "Wahr" ? " CHECKED" : ""; ?>>
                                    <small><?php _e('Ja, ich möchte spezielle Produkt-/Serviceinformationen von EMarSys zu den angegebenen Interessen erhalten','callwey'); ?></small>
								</div>
							</div>
                            <input class="button button-newsletter" type="submit" name="submit1" value="Registrieren">
                            -->
                            <button name="newsletter-submit" class="button button-newsletter"><?php _e('JETZT ABONNIEREN!','callwey') ?></button>
						</fieldset>
					</form>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') ): ?>
			</div>
		<?php endif; ?>
		<?php
        }

}
register_widget( 'TTTSocial_links_widget' );

?>
