<?php

class TTTPresse_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Presse','description=Texte, BILDER & Rezessionen.');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
		?>
		<?php if (is_tttdevice('tablet') ): ?>
			<div class="medium-6 columns">
		<?php endif; ?>
			<aside id="presse" class="widget <?php if (is_tttdevice('tablet') ): ?>medium-6 columns<?php endif; ?>">
				<div class="widget-container">
					<h4 class="widget-title hide"><?php _e('Presse', 'callwey'); ?></h4>
					<a href="<?php echo get_permalink( 103 ); ?>">
						<div class="big-text txt-up">PRE</div>
						<div class="middle-text"><?php _e('Texte, BILDER<br>&<br>Rezensionen', 'callwey'); ?></div>
						<div class="big-text txt-down">SSE</div>
					</a>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') ): ?>
			</div>
		<?php endif; ?>
		<?php
        }

}
register_widget( 'TTTPresse_widget' );

?>
