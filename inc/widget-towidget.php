<?php
class TTTPromoted_page_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Promoted Page','description=This widget display only the last page that use "Template: To Widget."');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
			global $post;        
		?>
		<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
			<div class="medium-6 small-9 columns">
		<?php endif; ?>	
			<aside id="promoted-page" class="widget">
				<div class="widget-container">
					<?php
						$recipe = array(
								'post_type'	 =>	'page',
								'posts_per_page' => 1,
								'meta_key' => '_wp_page_template', 'meta_value' => 'page-template-towidget.php'
						);
						
						$recipe_query = new WP_Query($recipe);
					?>
					<?php if ($recipe_query->have_posts()) : ?>
						<?php while ($recipe_query->have_posts()) : $recipe_query->the_post(); ?>
							<?php
								$first_line = get_post_meta( $post->ID, '_clwy_first_line', true );
								$second_line = get_post_meta( $post->ID, '_clwy_second_line', true );
							?>
							<div class="autor-wrap">
								<div class="autor-thumbnail">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('autor-widget', array('itemprop' => 'image')); ?>
									</a>
								</div>
								<h4 class="widget-title"><?php echo $first_line; ?><br><?php echo $second_line; ?></h4>
							</div>						
							<h3 class="widget-subtitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
			</div>
		<?php endif; ?>		    
		<?php
        }

}
register_widget( 'TTTPromoted_page_widget' );

?>
