;(function ($, window, document, undefined) {
  'use strict';

  var noop = function() {};

  var Orbit = function(el, settings) {
    // Don't reinitialize plugin
    if (el.hasClass(settings.slides_container_class)) {
      return this;
    }

    var self = this,
        container,
        slides_container = el,
        number_container,
        bullets_container,
        timer_container,
        idx = 0,
        animate,
        timer,
        locked = false,
        adjust_height_after = false;

    slides_container.children().first().addClass(settings.active_slide_class);

    self.update_idx = function( newidx ) {
        idx = newidx;
    };

    self.update_slide_number = function(index) {
      if (settings.slide_number) {
        number_container.find('span:first').text(parseInt(index)+1);
        number_container.find('span:last').text(slides_container.children().length);
      }
      if (settings.bullets) {
        bullets_container.children().removeClass(settings.bullets_active_class);
        $(bullets_container.children().get(index)).addClass(settings.bullets_active_class);
      }
    };

    self.update_active_link = function(index) {
      var link = $('a[data-orbit-link="'+slides_container.children().eq(index).attr('data-orbit-slide')+'"]');
      link.parents('ul').find('[data-orbit-link]').removeClass(settings.bullets_active_class);
      link.addClass(settings.bullets_active_class);
    };

    self.build_markup = function() {
      slides_container.wrap('<div class="'+settings.container_class+'"></div>');
      container = slides_container.parent();
      slides_container.addClass(settings.slides_container_class);
      
      if (settings.navigation_arrows) {
        container.append($('<a href="#">').addClass(settings.prev_class).append('<span>'));
        container.append($('<a href="#">').addClass(settings.next_class).append('<span>'));
      }

      if (settings.timer) {
        timer_container = $('<div>').addClass(settings.timer_container_class);
        timer_container.append('<span>');
        timer_container.append($('<div>').addClass(settings.timer_progress_class));
        timer_container.addClass(settings.timer_paused_class);
        container.append(timer_container);
      }

      if (settings.slide_number) {
        number_container = $('<div>').addClass(settings.slide_number_class);
        number_container.append('<span></span> - <span></span>');
        container.append(number_container);
      }

      if (settings.bullets) {
        bullets_container = $('<ol>').addClass(settings.bullets_container_class);
        container.append(bullets_container);
        slides_container.children().each(function(idx, el) {
          var bullet = $('<li>').attr('data-orbit-slide', idx);
          bullets_container.append(bullet);
        });
      }

      if (settings.stack_on_small) {
        container.addClass(settings.stack_on_small_class);
      }

      self.update_slide_number(0);
      self.update_active_link(0);
    };

    self._goto = function(next_idx, start_timer) {
      // if (locked) {return false;}
      if (next_idx === idx) {return false;}
      if (typeof timer === 'object') {timer.restart();}
      var slides = slides_container.children();

      var dir = 'next';
      locked = true;
      if (next_idx < idx) {dir = 'prev';}
      if (next_idx >= slides.length) {next_idx = 0;}
      else if (next_idx < 0) {
        if (settings.animation === 'nearby')        
            next_idx = 0;
        else
            next_idx = slides.length - 1;
      }
      // For NearBy
      //else if (next_idx < 0) {next_idx = 0;}
      // Original
      //else if (next_idx < 0) {next_idx = slides.length - 1;}
      
      var current = $(slides.get(idx));
      var next = $(slides.get(next_idx));

      current.css('zIndex', 2);
      current.removeClass(settings.active_slide_class);
      slides_container.children().removeClass( settings.active_slide_class );
      next.css('zIndex', 4).addClass(settings.active_slide_class);

      slides_container.trigger('orbit:before-slide-change');
      settings.before_slide_change();
      self.update_active_link(next_idx);
      
      var callback = function() {
        var unlock = function() {
          idx = next_idx;
          locked = false;
          if (start_timer === true) {timer = self.create_timer(); timer.start();}
          self.update_slide_number(idx);
          slides_container.trigger('orbit:after-slide-change',[{slide_number: idx, total_slides: slides.length}]);
          settings.after_slide_change(idx, slides.length);
        };
        if (slides_container.height() != next.height() && settings.variable_height) {
          slides_container.animate({'height': next.height()}, 250, 'linear', unlock);
        } else {
          unlock();
        }
      };

      if (slides.length === 1) {callback(); return false;}

      var start_animation = function() {
        if (dir === 'next') {animate.next(current, next, callback);}
        if (dir === 'prev') {animate.prev(current, next, callback);}        
      };

      if (next.height() > slides_container.height() && settings.variable_height) {
        slides_container.animate({'height': next.height()}, 250, 'linear', start_animation);
      } else {
        start_animation();
      }
    };
    
    self.next = function(e) {
      e.stopImmediatePropagation();
      e.preventDefault();
      self._goto(idx + 1);
    };
    
    self.prev = function(e) {
      e.stopImmediatePropagation();
      e.preventDefault();
      self._goto(idx - 1);
    };

    self.link_custom = function(e) {
      e.preventDefault();
      var link = $(this).attr('data-orbit-link');
      if ((typeof link === 'string') && (link = $.trim(link)) != "") {
        var slide = container.find('[data-orbit-slide='+link+']');
        if (slide.index() != -1) {self._goto(slide.index());}
      }
    };

    self.link_bullet = function(e) {
      var index = $(this).attr('data-orbit-slide');
      if ((typeof index === 'string') && (index = $.trim(index)) != "") {
        self._goto(parseInt(index));
      }
    }

    self.timer_callback = function() {
      self._goto(idx + 1, true);
    }
    
    self.compute_dimensions = function() {
      var current = $(slides_container.children().get(idx));
      var h = current.height();
      if (!settings.variable_height) {
        slides_container.children().each(function(){
          if ($(this).height() > h) { h = $(this).height(); }
        });
      }
      slides_container.height(h);
    };

    self.create_timer = function() {
      var t = new Timer(
        container.find('.'+settings.timer_container_class), 
        settings, 
        self.timer_callback
      );
      return t;
    };

    self.start_timer = function() {
      if (typeof timer === 'undefined') { timer = self.create_timer();}
      timer.start();     
    };

    self.stop_timer = function() {
      if (typeof timer === 'object') timer.stop();
    };

    self.toggle_timer = function() {
      var t = container.find('.'+settings.timer_container_class);
      if (t.hasClass(settings.timer_paused_class)) {
        if (typeof timer === 'undefined') {timer = self.create_timer();}
        timer.start();     
      }
      else {
        if (typeof timer === 'object') {timer.stop();}
      }
    };

    self.init = function() {
      self.build_markup();
      if (settings.timer) {timer = self.create_timer(); timer.start();}
      animate = new FadeAnimation(settings, slides_container);
      if (settings.animation === 'slide') 
        animate = new SlideAnimation(settings, slides_container);
      if (settings.animation === 'push') 
        animate = new PushAnimation(settings, slides_container);
      if (settings.animation === 'nearby') 
        animate = new NearByAnimation(settings, slides_container, self);
      container.on('click', '.'+settings.next_class, self.next);
      container.on('click', '.'+settings.prev_class, self.prev);
      container.on('click', '[data-orbit-slide]', self.link_bullet);
      container.on('click', self.toggle_timer);
      if (settings.swipe) {
        container.on('touchstart.fndtn.orbit', function(e) {
          if (!e.touches) {e = e.originalEvent;}
          var data = {
            start_page_x: e.touches[0].pageX,
            start_page_y: e.touches[0].pageY,
            start_time: (new Date()).getTime(),
            delta_x: 0,
            is_scrolling: undefined
          };
          container.data('swipe-transition', data);
          e.stopPropagation();
        })
        .on('touchmove.fndtn.orbit', function(e) {
          if (!e.touches) { e = e.originalEvent; }
          // Ignore pinch/zoom events
          if(e.touches.length > 1 || e.scale && e.scale !== 1) return;

          var data = container.data('swipe-transition');
          if (typeof data === 'undefined') {data = {};}

          data.delta_x = e.touches[0].pageX - data.start_page_x;

          if ( typeof data.is_scrolling === 'undefined') {
            data.is_scrolling = !!( data.is_scrolling || Math.abs(data.delta_x) < Math.abs(e.touches[0].pageY - data.start_page_y) );
          }

          if (!data.is_scrolling && !data.active) {
            e.preventDefault();
            var direction = (data.delta_x < 0) ? (idx+1) : (idx-1);
            data.active = true;
            self._goto(direction);
          }
        })
        .on('touchend.fndtn.orbit', function(e) {
          container.data('swipe-transition', {});
          e.stopPropagation();
        })
      }
      container.on('mouseenter.fndtn.orbit', function(e) {
        if (settings.timer && settings.pause_on_hover) {
          self.stop_timer();
        }
      })
      .on('mouseleave.fndtn.orbit', function(e) {
        if (settings.timer && settings.resume_on_mouseout) {
          timer.start();
        }
      });
      
      $(document).on('click', '[data-orbit-link]', self.link_custom);
      $(window).on('resize', self.compute_dimensions);
      $(window).on('load', self.compute_dimensions);
      $(window).on('load', function(){
        container.prev('.preloader').css('display', 'none');
      });
      slides_container.trigger('orbit:ready');
    };

    self.init();
  };

  var Timer = function(el, settings, callback) {
    var self = this,
        duration = settings.timer_speed,
        progress = el.find('.'+settings.timer_progress_class),
        start, 
        timeout,
        left = -1;

    this.update_progress = function(w) {
      var new_progress = progress.clone();
      new_progress.attr('style', '');
      new_progress.css('width', w+'%');
      progress.replaceWith(new_progress);
      progress = new_progress;
    };

    this.restart = function() {
      clearTimeout(timeout);
      el.addClass(settings.timer_paused_class);
      left = -1;
      self.update_progress(0);
    };

    this.start = function() {
      if (!el.hasClass(settings.timer_paused_class)) {return true;}
      left = (left === -1) ? duration : left;
      el.removeClass(settings.timer_paused_class);
      start = new Date().getTime();
      progress.animate({'width': '100%'}, left, 'linear');
      timeout = setTimeout(function() {
        self.restart();
        callback();
      }, left);
      el.trigger('orbit:timer-started')
    };

    this.stop = function() {
      if (el.hasClass(settings.timer_paused_class)) {return true;}
      clearTimeout(timeout);
      el.addClass(settings.timer_paused_class);
      var end = new Date().getTime();
      left = left - (end - start);
      var w = 100 - ((left / duration) * 100);
      self.update_progress(w);
      el.trigger('orbit:timer-stopped');
    };
  };
  
  var SlideAnimation = function(settings, container) {
    var duration = settings.animation_speed;
    var is_rtl = ($('html[dir=rtl]').length === 1);
    var margin = is_rtl ? 'marginRight' : 'marginLeft';
    var animMargin = {};
    animMargin[margin] = '0%';

    this.next = function(current, next, callback) {
      next.animate(animMargin, duration, 'linear', function() {
        current.css(margin, '100%');
        callback();
      });
    };

    this.prev = function(current, prev, callback) {
      prev.css(margin, '-100%');
      prev.animate(animMargin, duration, 'linear', function() {
        current.css(margin, '100%');
        callback();
      });
    };
  };

  var FadeAnimation = function(settings, container) {
    var duration = settings.animation_speed;
    var is_rtl = ($('html[dir=rtl]').length === 1);
    var margin = is_rtl ? 'marginRight' : 'marginLeft';

    this.next = function(current, next, callback) {
      next.css({'margin':'0%', 'opacity':'0.01'});
      next.animate({'opacity':'1'}, duration, 'linear', function() {
        current.css('margin', '100%');
        callback();
      });
    };

    this.prev = function(current, prev, callback) {
      prev.css({'margin':'0%', 'opacity':'0.01'});
      prev.animate({'opacity':'1'}, duration, 'linear', function() {
        current.css('margin', '100%');
        callback();
      });
    };
  };

  var PushAnimation = function(settings, container) {

    var duration = settings.animation_speed;
    var is_rtl = ($('html[dir=rtl]').length === 1);
    var margin = is_rtl ? 'marginRight' : 'marginLeft';
    var animMargin = {};
    animMargin[margin] = '0%';

    var animMargin2 = {};
    animMargin2[margin] = '-100%';

    var animMargin3 = {};
    animMargin3[margin] = '100%';

    this.next = function(current, next, callback) {
      current.animate(animMargin2, duration,'linear');
      next.animate(animMargin, duration, 'linear', function() {
        current.css(margin, '100%');
        callback();
      });
    };

    this.prev = function(current, prev, callback) {
      current.animate(animMargin3, duration,'linear');
      prev.css(margin, '-100%');
      prev.animate(animMargin, duration, 'linear', function() {
        current.css(margin, '100%');
        callback();
      });
    };
  };

  var NearByAnimation = function(settings, container, _orbit) {

    jQuery.resize.delay = 200;

    var duration = settings.animation_speed;
    var is_rtl = ($('html[dir=rtl]').length === 1);
    var margin = is_rtl ? 'marginRight' : 'marginLeft';

    var localsettings = {
    	nearbyopacity: 10,
        nearbyscale: 10,
        nearbyparent: false
    }
    
    settings = $.extend(localsettings, settings);
    
    var filtersettings = ['nearbyopacity','nearbyscale']; 
    for ( var v in filtersettings ) {
        var i = filtersettings[ v ] ;
        if ( settings[ i ] == false ) {
            settings[ i ] = 0;
        }
        else if ( settings[ i ] >= 10 ) {
            settings[ i ] = 1;
        }
        else {
            settings[ i ] = '0.' + Number( settings[i] );
        }
    }

    var items   = container.find('li'),
        len     = items.length,
        current = 0,
        first   = items.filter(':first'),
        last    = items.filter(':last'),
        gallery = container.parent();

    items.css({ 'white-space': 'nowrap', 'float': 'left' });
    items.each(function() {
        $(this).attr('data-item-n',$(this).index()+1);
    });

    first.before(last.prev().clone(true).addClass('clone')); 
    first.before(last.clone(true).addClass('clone')); 

    last.after(first.next().clone(true).addClass('clone').removeClass('active')); 
    last.after(first.clone(true).addClass('clone').removeClass('active')); 


    //container.find('.clone').css({ border: '1px solid blue' });

    //container.css({ overflow: 'visible' });

    /* 2. Set button handlers */
    container.on('orbit:nearby-click', function(event, dir, callback) {

        if (container.is(':not(:animated)')) {
        
            var cycle = false,
                diff = 0,
                delta = (dir === "prev")? -1 : 1,
                move = $(settings.nearbyparent).width() * -1,
                ratio = container.position().left / move,
                origmove = move;
            
            if (  parseFloat(ratio) != parseInt(ratio, 10) ) {
                diff = container.position().left - ( move * parseInt(ratio) );
                move = move - diff;
            }
        
            container.animate({ left: "+=" + (move * delta) },
                {
                    duration: 800,
                    done: function() {
        
                        current += delta;

                        /** 
                         * we're cycling the slider when the the value of "current" 
                         * variable (after increment/decrement) is 0 or when it exceeds
                         * the initial gallery length
                         */          

                        if ( delta > 0 ) {
                            cycle = !!(current >= len+1);
                            //console.log('cycle', cycle, 'current', current, 'len', len, 'real len', $(this).find('li').length );
                            if (cycle) {
                                var orig = current;
                                current = current - len;
                                if ( diff != 0 ) {
                                    move = origmove;
                                }
                                //console.log('**************','FROM', orig, 'TO', current, 'diff', diff, 'origmove', origmove, 'move', move );
                                container.css({left:  move * current });
                                _orbit.update_idx( current + 3 );
                            }
                        }
                        else {
                            cycle = !!(current <= 1);
                            if ( cycle ) {
                                current = len + (current*-1) +2;
                                container.css({left:  move * current });
                                _orbit.update_idx( current + 3 );
                            }
                        }

                        if ( callback != undefined )
                            callback();
                    }
                }
            );

            container.find('li').removeClass('active');


            // if ( delta > 0 ) {
            //     container.find('li[data-item-n='+current+']').addClass('active');
            // }
            // else {
            //     var ncurrent = current - 2;
            //     if ( ncurrent <= 0 ) ncurrent = len;
            //     container.find('li[data-item-n='+ncurrent+']').addClass('active');
            // }

            container.find('li').each(function() {

                var params = {};
                if ( $(this).hasClass('active') ) {
                   params[ 'opacity' ] = 1;
                   params[ 'transform' ] = 'scale(1)';
                }
                else {
                   //params[ 'opacity' ] = settings.nearbyopacity;
                   //params[ 'transform' ] = 'scale('+settings.nearbyscale+')';
                }

                //$(this).css( params );
                //setTimeout( callback, 650 );

                $(this).animate( params, duration, 'linear', callback );
            });
         }
    
    });



    $('#background-nearby').on('orbit:nearby-resize',function(event, nextoff ) {
        if ( !nextoff ) { nextoff = false; }

        if ($(this).is(':animated')) {
            //console.log('animated');
            return false;
        }

        $(this).css({
            visbility: 'hidden',
            //width: '10%',
        });

        var slideElementWidth = $(settings.nearbyparent).width();
        var slideTotalElemnts = $('li',container).length;
        var slideTotalWidth = slideElementWidth * slideTotalElemnts;

        var slideNextPrevWidth = ( $(window).width() - slideElementWidth ) / 2;

        //console.log( 'slideElementWidth', slideElementWidth, 'slideTotalElemnts', slideTotalElemnts, 'slideTotalWidth', slideTotalWidth );

        $(gallery).css({ overflow: 'visible', width: slideElementWidth, margin: '0 auto' });

        $(container).css({ width: slideTotalWidth, display: 'block' });
        //$(container).css({ border: '1px solid red', width: slideElementWidth, display: 'block', overflow: 'visible' });

        $(container).find('img').css({ width: '100%' });
        $(container).find('li').css({ 'margin': 'auto auto auto auto', position: 'relative', display: 'inline-block', width: slideElementWidth });

        //console.log('height', $('img:first',container).height() );

        var _parent = $(this).parent();
        $(_parent).css({
            height: $('img:first',container).height(),
            display: 'block'
        });

        $('.orbit-prev').css({ left: -$(settings.nearbyparent).offset().left, right: 'auto', width: slideNextPrevWidth, height: $(_parent).height()  });
        $('.orbit-next').css({ right: -$(settings.nearbyparent).offset().left, left: 'auto', width: slideNextPrevWidth, height: $(_parent).height()  });

        _orbit.stop_timer();

        var _top = _parent.offset().top;
        if ( $('#wpadminbar').height() > 0 ) {
            _top = _top - $('#wpadminbar').height();
        }

        $(this).height( $('img:first',container).height() );
        $(this).animate({ 
            width: $(window).width(),
            top: _top,
            display: 'block'
        },{
            duration: 250,
            easing: 'linear',
            complete: function() {
                //console.log( 'complete', $(this).offset().top, _parent.offset().top );

                var backgroundnearby = $(this);
                    backgroundnearby.animate({ opacity: 1, display: 'block' },100,'linear');

                    $(container).parent().find('a.orbit-next').click();

                    _orbit.start_timer();

                    // setTimeout(function() {
                    //     container.trigger('orbit:nearby-click', [ 'next' ] );
                    // },200);
            }
        });
        


    });

    // $( '#background-nearby' ).trigger('orbit:nearby-resize').parent().resize(function() {
    // 	$('#background-nearby').trigger('orbit:nearby-resize');
    // });
    // $( window ).resize(function() {
    // 	$('#background-nearby').trigger('orbit:nearby-resize');
    // });
    // $( 'img', container ).load(function() {
    // 	$('#background-nearby').trigger('orbit:nearby-resize');
    // });


    var lastResizeId = 0;
    $('#background-nearby').on('orbit:nearby-changedom',function() {
        if ($(this).is(':animated')) {
            //console.log('animated');
            return false;
        }
        try {
            clearInterval( lastResizeId );
            //$( this ).css({ opacity: 0.2, width: 1 });
        } catch(e) {}
        lastResizeId = setTimeout(function() {
            $( '#background-nearby' ).trigger('orbit:nearby-resize');
        },1000);
    });


    $( window ).resize(function() {
        $('#background-nearby').trigger('orbit:nearby-changedom');
    });
    $('#background-nearby').trigger('orbit:nearby-changedom');

    // // Fix for IE and Firefox, background resize
    // setInterval(function() {
    //     var height = $('#background-nearby').height();
    //     if( height > 100 && height != 1 ) return true;
    //     console.log('interval works');
    //     //$('#background-nearby').trigger('orbit:nearby-resize');
    //     //$('#background-nearby').trigger('orbit:nearby-changedom');
    //     // $('li img:first',container).each(function() {
    //     //     $('#background-nearby').animate({ height: $(this).height() },'fast','linear');
    //     //     $('#background-nearby').parent().animate({ height: $(this).height() },'fast','linear');
    //     // });
    // },500);

    this.next = function(current, next, callback) {
        container.trigger('orbit:nearby-click', [ 'next', callback ] );
    };

    this.prev = function(current, prev, callback) {
        container.trigger('orbit:nearby-click', [ 'prev', callback ] );
    };

    // var startCurrent = -1;
    // container.css({left: ($(settings.nearbyparent).width() * -1) * startCurrent });
    //_orbit.update_idx( startCurrent + 2 );
    // setTimeout(function() {
    //     container.trigger('orbit:nearby-click', [ 'next' ] );
    // },300);

    setInterval(function() {
        var _parent = $('#background-nearby').parent();
        var old = parseInt( $( '#background-nearby' ).offset().top ) || 0;
        var t = parseInt( $(_parent).offset().top );

        $(_parent).data('offset-top',t);
        if ( old != t ) {
            $('#background-nearby').trigger('orbit:nearby-resize', [ true ] );
        }
    },100);

    //$('#background-nearby').parent().css('min-height',300);


  };

  Foundation.libs = Foundation.libs || {};

  Foundation.libs.orbit = {
    name: 'orbit',

    version: '4.3.1',

    settings: {
      animation: 'slide',
      timer_speed: 10000,
      pause_on_hover: true,
      resume_on_mouseout: false,
      animation_speed: 500,
      stack_on_small: false,
      navigation_arrows: true,
      slide_number: true,
      container_class: 'orbit-container',
      stack_on_small_class: 'orbit-stack-on-small',
      next_class: 'orbit-next',
      prev_class: 'orbit-prev',
      timer_container_class: 'orbit-timer',
      timer_paused_class: 'paused',
      timer_progress_class: 'orbit-progress',
      slides_container_class: 'orbit-slides-container',
      bullets_container_class: 'orbit-bullets',
      bullets_active_class: 'active',
      slide_number_class: 'orbit-slide-number',
      caption_class: 'orbit-caption',
      active_slide_class: 'active',
      orbit_transition_class: 'orbit-transitioning',
      bullets: true,
      timer: true,
      variable_height: false,
      swipe: true,
      before_slide_change: noop,
      after_slide_change: noop
    },

    init: function (scope, method, options) {
      var self = this;
      Foundation.inherit(self, 'data_options');

      if (typeof method === 'object') {
        $.extend(true, self.settings, method);
      }

      if ($(scope).is('[data-orbit]')) {
        var $el = $(scope);
        var opts = self.data_options($el);
        new Orbit($el, $.extend({},self.settings, opts));
      }

      $('[data-orbit]', scope).each(function(idx, el) {
        var $el = $(el);
        var opts = self.data_options($el);
        new Orbit($el, $.extend({},self.settings, opts));
      });
    }
  };

    
}(Foundation.zj, this, this.document));
