<?php
/**
 * cover_book styles
 */
function cover_book_stylesheet() {
    wp_enqueue_style( 'cover_component', get_template_directory_uri() . '/inc/3DBook/css/component.css');
}

add_action( 'wp_enqueue_scripts', 'cover_book_stylesheet' );

/**
 * cover_book scripts
 */
function cover_book_script() {
	wp_enqueue_script('cover_books1', get_template_directory_uri() . '/inc/3DBook/js/books1.js',array('jquery','custom.modernizr'));
}	
	
add_action('wp_enqueue_scripts', 'cover_book_script');