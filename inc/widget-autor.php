<?php

class TTTPromoted_autor_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Promoted Autor','description=Autor des monats.');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
		?>
		<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
			<div class="medium-6 small-9 columns">
		<?php endif; ?>	
			<aside id="promoted-autor" class="widget">
				<div class="widget-container">
					<?php
						$recipe = array(
								'post_type'	 =>	'autor',
								'posts_per_page' => 1,
								'post__in' => get_option("sticky_posts"),
								'post__not_in' => get_option("sticky_posts"),
								'ignore_sticky_posts' => 1,						
							);
						
						$recipe_query = new WP_Query($recipe);
					?>
					<?php if ($recipe_query->have_posts()) : ?>
						<?php while ($recipe_query->have_posts()) : $recipe_query->the_post(); ?>				
							<div class="autor-wrap">
								<div class="autor-thumbnail">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('autor-widget', array('itemprop' => 'image')); ?>
									</a>
								</div>
								<h4 class="widget-title"><?php _e('Au<br>tor', 'callwey'); ?></h4>
							</div>						
								<h3 class="widget-subtitle"><?php the_title(); ?><?php //_e('des monats', 'callwey'); ?></h3>		
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
			</div>
		<?php endif; ?>		    
		<?php
        }

}
register_widget( 'TTTPromoted_autor_widget' );

?>
