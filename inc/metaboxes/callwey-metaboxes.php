<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_clwy_';

	$meta_boxes[] = array(
		'id'         => 'new_link',
		'title'      => 'New link',
		'pages' => array('produkt'), // post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Last Issue link',
				'desc' => 'WITHOUT http://',
				'id'   => $prefix . 'produkt_new_link',
				'type' => 'text',
			),
			array(
				'name' => 'Header text',
				//'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'produkt_header_text',
				'type' => 'text',
			),
			array(
				'name' => 'Magazine Site Link',
				'desc' => 'WITHOUT http://',
				'id'   => $prefix . 'produkt_header_site_link',
				'type' => 'text',
			),
		)
	);
	
	$meta_boxes[] = array(
		'id'         => 'links_link',
		'title'      => 'URL link',
		'pages' => array('links'), // post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'URL Link',
				//'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'url_link',
				'type' => 'text',
			),
		)
	);

	$meta_boxes[] = array(
		'id'         => 'update_text',
		'title'      => 'Update Content',
		'pages' => array('update'), // post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Text',
				//'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'update_text',
				'type' => 'textarea_small',
			),
			array(
				'name' => 'Link url',
				'desc' => 'WITH http://',
				'id'   => $prefix . 'update_link',
				'type' => 'text',
			),
		)
	);
	
	$meta_boxes[] = array(
		'id'         => 'intro_text',
		'title'      => 'Intro',
		'pages' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-template-towidget.php' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Intro text',
				//'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'page_intro',
				'type' => 'textarea',
			),
		)
	);
	
	$meta_boxes[] = array(
		'id'         => 'timeline_entry',
		'title'      => 'Fix timeline position',
		'pages' => array('timeline'), // post type
		'context'    => 'side',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Top Margin',
				'desc' => 'Use numbers only.',
				'id'   => $prefix . 'timeline_top_margin',
				'type' => 'text_small',
			),
		)
	);
	
	$meta_boxes[] = array(
		'id'         => 'widget_texts',
		'title'      => 'Widget Headlines',
		'pages' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-template-towidget.php' ),
		'context'    => 'side',
		'priority'   => 'high',
		'show_names' => false, // Show field names on the left
		'fields'     => array(
			array(
				'name' => '1st',
				'desc' => 'First line',
				'id'   => $prefix . 'first_line',
				'type' => 'text_small',
			),
			array(
				'name' => '2nd',
				'desc' => 'Second line',
				'id'   => $prefix . 'second_line',
				'type' => 'text_small',
			),
		)
	);

	$meta_boxes[] = array(
		'id'         => 'extra_details',
		'title'      => 'Extra info',
		'pages' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-template-presse.php' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Buchvorschau',
				'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'issuu_page',
				'type' => 'text_medium',
			),
			array(
				'name' => 'Form',
				'desc' => 'Paste the Contac Form ID',
				'id'   => $prefix . 'cform_id',
				'type' => 'text_small',
			),
		)
	);

	$meta_boxes[] = array(
		'id'         => 'extra_info',
		'title'      => 'Extra Info',
		'pages' => array('page'), // post type
		'show_on' => array( 'key' => 'page-template', 'value' => 'page-template-buchhandle.php' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Buchvorschau',
				'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'issuu_page_buchhandle',
				'type' => 'text_medium',
			),
			array(
				'name' => 'Unsere Anschrift',
				//'desc' => 'Paste the Contac Form ID',
				'id'   => $prefix . 'buchhandle_address',
				'type' => 'wysiwyg',
				'options' => array(
/*
				    'wpautop' => true, // use wpautop?
				    'media_buttons' => true, // show insert/upload button(s)
				    'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
				    'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
				    'tabindex' => '',
				    'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the <style> tags, can use "scoped".
				    'editor_class' => '', // add extra class(es) to the editor textarea
				    'teeny' => false, // output the minimal editor config used in Press This
				    'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
				    'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
				    'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()	
*/
				),
			),
		)
	);

/*
	$meta_boxes[] = array(
		'id'         => 'agent_details',
		'title'      => 'Agent Details',
		'pages' => array('agent'), // post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Area',
				//'desc' => '',
				'id'   => $prefix . 'agent_area',
				'type' => 'text',
			),
			array(
				'name' => 'Location',
				//'desc' => '',
				'id'   => $prefix . 'agent_location',
				'type' => 'text',
			),
			array(
				'name' => 'Address',
				//'desc' => '',
				'id'   => $prefix . 'agent_address',
				'type' => 'text',
			),
			array(
				'name' => 'Email',
				//'desc' => '',
				'id'   => $prefix . 'agent_email',
				'type' => 'text',
			),
			array(
				'name' => 'Fon',
				//'desc' => '',
				'id'   => $prefix . 'agent_fon',
				'type' => 'text',
			),
			array(
				'name' => 'Fax',
				//'desc' => '',
				'id'   => $prefix . 'agent_fax',
				'type' => 'text',
			),			
		)
	);
*/

	$meta_boxes[] = array(
		'id'         => 'position_details',
		'title'      => 'Position Details',
		'pages' => array('staff'), // post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Position',
				//'desc' => '',
				'id'   => $prefix . 'position',
				'type' => 'text',
			),
			array(
				'name' => 'Email',
				//'desc' => '',
				'id'   => $prefix . 'email',
				'type' => 'text',
			),
			array(
				'name' => 'Fon',
				//'desc' => '',
				'id'   => $prefix . 'fon',
				'type' => 'text',
			),
			array(
				'name' => 'Fax',
				//'desc' => '',
				'id'   => $prefix . 'fax',
				'type' => 'text',
			),			
		)
	);
	
/*
	$meta_boxes[] = array(
		'id'         => 'job_details',
		'title'      => 'Details',
		'pages' => array('jobs'), // post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Upload a PDF',
				//'desc' => '',
				'id'   => $prefix . 'job_pdf',
				'type' => 'file',
				'save_id' => true, // save ID using true
				'allow' => array( 'url' ) // limit to just attachments with array( 'attachment' )
			),
		)
	);
*/
		
	$meta_boxes[] = array(
		'id'         => 'extra_details',
		'title'      => 'Extra info',
		'pages'      => array( 'produkt', 'ebook'), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Product Overview',
				'desc' => 'Use only file ID. ie: URL: issuu.com/callwey/docs/frl_klein?e=0/4859286 FILE ID: 4859286',
				'id'   => $prefix . 'issuu',
				'type' => 'text_medium',
			),
			array(
				'name' => 'Autor Quote',
				'desc' => 'The Author name is the same as the book author.',
				'id'   => $prefix . 'autor_quote',
				'type' => 'textarea',
			),
		)
	);

	$meta_boxes[] = array(
		'id'         => 'ttt_magento_fields',
		'title'      => 'Replace data from Magento',
		'pages'      => array( 'produkt', 'ebook' ), // Post type
		'context'    => 'normal',
		'priority'   => 'default',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name'    => 'Reviews Quantity',
				'desc'    => 'Choose how much review this produkt have.',
				'id'      => $prefix . 'reviews_q',
				'type'    => 'select',
				'options' => array(
					array( 'name' => '1', 'value' => '1', ),
					array( 'name' => '2', 'value' => '2', ),
					array( 'name' => '3', 'value' => '3', ),
					array( 'name' => '4', 'value' => '4', ),
					array( 'name' => '5', 'value' => '5', ),
					array( 'name' => '6', 'value' => '6', ),
					array( 'name' => '7', 'value' => '7', ),
					array( 'name' => '8', 'value' => '8', ),
					array( 'name' => '9', 'value' => '9', ),
					array( 'name' => '10', 'value' => '10', ),
					array( 'name' => '11', 'value' => '11', ),
					array( 'name' => '12', 'value' => '12', ),
					array( 'name' => '13', 'value' => '13', ),
					array( 'name' => '14', 'value' => '14', ),
					array( 'name' => '15', 'value' => '15', ),
					array( 'name' => '16', 'value' => '16', ),
					array( 'name' => '17', 'value' => '17', ),
					array( 'name' => '18', 'value' => '18', ),
					array( 'name' => '19', 'value' => '19', ),
					array( 'name' => '20', 'value' => '20', )
				),
			),		
			array(
				'name' => 'Press Review 1',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_1',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 1',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_1',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_2',
				'type' => 'divider',
			),			
			array(
				'name' => 'Press Review 2',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_2',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 2',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_2',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_3',
				'type' => 'divider',
			),
			array(
				'name' => 'Press Review 3',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_3',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 3',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_3',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_4',
				'type' => 'divider',
			),
			array(
				'name' => 'Press Review 4',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_4',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 4',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_4',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_5',
				'type' => 'divider',
			),
			array(
				'name' => 'Press Review 5',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_5',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 5',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_5',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_6',
				'type' => 'divider',
			),
			array(
				'name' => 'Press Review 6',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_6',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 6',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_6',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_7',
				'type' => 'divider',
			),			
			array(
				'name' => 'Press Review 7',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_7',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 7',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_7',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_8',
				'type' => 'divider',
			),						
			array(
				'name' => 'Press Review 8',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_8',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 8',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_8',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_9',
				'type' => 'divider',
			),						
			array(
				'name' => 'Press Review 9',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_9',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 9',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_9',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_10',
				'type' => 'divider',
			),						
			array(
				'name' => 'Press Review 10',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_10',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 10',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_10',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_11',
				'type' => 'divider',
			),
			array(
				'name' => 'Press Review 11',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_11',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 11',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_11',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_12',
				'type' => 'divider',
			),			
			array(
				'name' => 'Press Review 12',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_12',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 12',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_12',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_13',
				'type' => 'divider',
			),							
			array(
				'name' => 'Press Review 13',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_13',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 13',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_13',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_14',
				'type' => 'divider',
			),									
			array(
				'name' => 'Press Review 14',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_14',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 14',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_14',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_15',
				'type' => 'divider',
			),							
			array(
				'name' => 'Press Review 15',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_15',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 15',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_15',
				'type' => 'text',
			),			
			array(
				'id'   => $prefix . 'divider_16',
				'type' => 'divider',
			),						
			array(
				'name' => 'Press Review 16',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_16',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 16',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_16',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_17',
				'type' => 'divider',
			),						
			array(
				'name' => 'Press Review 17',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_17',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 17',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_17',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_18',
				'type' => 'divider',
			),									
			array(
				'name' => 'Press Review 18',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_18',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 18',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_18',
				'type' => 'text',
			),			
			array(
				'id'   => $prefix . 'divider_19',
				'type' => 'divider',
			),
			array(
				'name' => 'Press Review 19',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_19',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 19',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_19',
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'divider_20',
				'type' => 'divider',
			),						
			array(
				'name' => 'Press Review 20',
				//'desc' => 'If this field is empty will show the data from Magento',
				'id'   => $prefix . 'press_review_20',
				'type' => 'textarea',
			),
			array(
				'name' => 'Press Review cite 20',
				'desc' => 'If this field is empty "Press Review" will show the data from Magento',
				'id'   => $prefix . 'press_review_cite_20',
				'type' => 'text',
			),
		)
	);

	
	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}