<?php
/**
 * icomoon.io font icons
 */
function iconmoon_styles() {
    wp_enqueue_style( 'icons_styles', get_template_directory_uri() . '/css/icons.min.css' );
}

add_action( 'wp_enqueue_scripts', 'iconmoon_styles' );

/**
 * iconmoon.io script for IE
 */
/*
function iconmoon_script() {
	wp_enqueue_script('icons_script', get_template_directory_uri() . '/fonts/lte-ie7.js');
}	
	
add_action('wp_enqueue_scripts', 'iconmoon_script');
*/