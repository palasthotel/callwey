<?php
	
function remove_pages_columns($defaults) {
  unset($defaults['comments']);
  return $defaults;
}
add_filter('manage_pages_columns', 'remove_pages_columns');

/*
* remove login errors
*/
add_filter('login_errors',create_function('$a', "return null;"));

/*
function admin_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_url').'/images/admin-logo.png) !important; height:200px !important; background-position: 0 0; }
    </style>';
}

add_action('login_head', 'admin_login_logo');
*/


function admin_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url('<?php echo of_get_option('logo'); ?>');
			width: 100%;
			background-size: auto;
        }
    </style>
<?php } add_action( 'login_enqueue_scripts', 'admin_login_logo' );



/*
 * Function to force SSL on AJAX connection
 */
function custom_force_ssl_admin($url) {
    global $blog_id;
    if ( $blog_id != 1 ) return $url;
    
    if ( strpos($url,'admin-ajax.php') ) {
        return str_replace('http:','https:',$url);
    }
    return $url;
}

add_filter('admin_url','custom_force_ssl_admin');

/*
* Remove footer admin
*/
function remove_footer_admin () {
  echo '';
}
add_filter('admin_footer_text', 'remove_footer_admin');


/*
* Remove default wordpress dashboard 
*/
 function admin_remove_dashboard_widgets() {
 	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
 	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );	
 	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
 	//remove_meta_box( 'icl_dashboard_widget', 'dashboard', 'side' );
 } 
 
add_action('wp_dashboard_setup', 'admin_remove_dashboard_widgets' );


/*
* remove junk from head
*/
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

/*===================================================================================
 * Add Author Links
 * =================================================================================*/
function add_to_author_profile( $contactmethods ) {
	$contactmethods['rss_url'] = 'RSS URL';
	$contactmethods['google_profile'] = 'Google Profile URL';
	$contactmethods['twitter_profile'] = 'Twitter Profile URL';
	$contactmethods['facebook_profile'] = 'Facebook Profile URL';
	$contactmethods['linkedin_profile'] = 'Linkedin Profile URL';
	return $contactmethods;
}
add_filter( 'user_contactmethods', 'add_to_author_profile', 10, 1);


/*
* Remove admin bar menu and WP logo
*/
function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );


/* NEW USER profile new fields -> widget-author-tagebuch.php & author.php */
add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
<h3><?php _e("TTT Author Tagebuch widget texts", "blank"); ?></h3>

<table class="form-table">
<tr>
<th><label for="headline"><?php _e("Headline"); ?></label></th>
<td>
<input type="text" name="headline" id="headline" value="<?php echo esc_attr( get_the_author_meta( 'headline', $user->ID ) ); ?>" class="regular-text" /><br />
<span class="description"><?php _e("This text shows on top of the widget."); ?></span>
</td>
</tr>
<tr>
<th><label for="underline"><?php _e("Underline"); ?></label></th>
<td>
<input type="text" name="underline" id="underline" value="<?php echo esc_attr( get_the_author_meta( 'underline', $user->ID ) ); ?>" class="regular-text" /><br />
<span class="description"><?php _e("This text shows in the bottom of the widget."); ?></span>
</td>
</tr>
</table>

<h3><?php _e("Author landing page headline", "blank"); ?></h3>

<table class="form-table">
<tr>
<th><label for="single_headline"><?php _e("Headline"); ?></label></th>
<td>
<input type="text" name="single_headline" id="single_headline" value="<?php echo esc_attr( get_the_author_meta( 'single_headline', $user->ID ) ); ?>" class="regular-text" /><br />
<span class="description"><?php _e("This text shows in the headline of the author single page."); ?></span>
</td>
</tr>
</table>

<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

update_user_meta( $user_id, 'headline', $_POST['headline'] );
update_user_meta( $user_id, 'underline', $_POST['underline'] );
update_user_meta( $user_id, 'single_headline', $_POST['single_headline'] );
}

/*
* allways activate secont row of TinyMCE
*/
function add_fontselect_row_3( $mce_buttons ) {
$mce_buttons[] = 'fontselect';
return $mce_buttons;
}

/*
* preserve image quality
*/
//add_filter('jpeg_quality', function($arg){return 100;});


	// Remove default "Add New" menu


function tru_remove_default_new_content_menu() {
    global $wp_admin_bar, $blog_id;
    //$wp_admin_bar->remove_menu('new-post');
	if ( $blog_id == 1 ) {
		$wp_admin_bar->remove_menu('new-magazine');  // I needed this CPT because of the way I wanted to organize taxonomies
	}
	//$wp_admin_bar->remove_menu('new-page');
	//$wp_admin_bar->remove_menu('new-media');
	//$wp_admin_bar->remove_menu('new-books');
	//$wp_admin_bar->remove_menu('new-teachings');
	//$wp_admin_bar->remove_menu('new-our-media');
	//$wp_admin_bar->remove_menu('new-user');
	//$wp_admin_bar->remove_menu('new-tru_main-info');
	//$wp_admin_bar->remove_menu('new-tru_home');
}
add_action( 'wp_before_admin_bar_render', 'tru_remove_default_new_content_menu' );

/*
* Remove menus from dashboard
*/
function remove_menus() {

global $submenu, $blog_id;

	//remove_menu_page( 'edit.php' ); // Posts
	//remove_menu_page( 'upload.php' ); // Media
	//remove_menu_page( 'edit-comments.php' ); // Comments
	//remove_menu_page( 'edit.php?post_type=page' ); // Pages
	//remove_menu_page( 'plugins.php' ); // Plugins
	//remove_menu_page( 'themes.php' ); // Appearance
	//remove_menu_page( 'users.php' ); // Users
	//remove_menu_page( 'tools.php' ); // Tools
	//remove_menu_page(‘options-general.php’); // Settings
	
	if ( $blog_id == 1 ) {
		remove_menu_page( 'edit.php?post_type=magazine' ); // CPT-magazine
	}
	
	//remove_submenu_page ( 'index.php', 'update-core.php' );    //Dashboard->Updates
	remove_submenu_page ( 'themes.php', 'themes.php' ); // Appearance-->Themes
	remove_submenu_page ( 'themes.php', 'customize.php' ); // Appearance-->Customizer
	//remove_submenu_page ( 'themes.php', 'widgets.php' ); // Appearance-->Widgets
	remove_submenu_page ( 'themes.php', 'theme-editor.php' ); // Appearance-->Editor
	remove_submenu_page ( 'plugins.php', 'plugin-editor.php' ); // Plugins-->Editor
	//remove_submenu_page ( 'options-general.php', 'options-general.php' ); // Settings->General
	//remove_submenu_page ( 'options-general.php', 'options-writing.php' ); // Settings->writing
	//remove_submenu_page ( 'options-general.php', 'options-reading.php' ); // Settings->Reading
	//remove_submenu_page ( 'options-general.php', 'options-discussion.php' ); // Settings->Discussion
	//remove_submenu_page ( 'options-general.php', 'options-media.php' ); // Settings->Media
	
}
add_action('admin_menu', 'remove_menus', 102);

/*
* remove default widgets
*/
function unregister_default_wp_widgets() {
  //unregister_widget('WP_Widget_Pages');
  //unregister_widget('WP_Widget_Calendar');
  //unregister_widget('WP_Widget_Archives');
  //unregister_widget('WP_Widget_Meta');
  //unregister_widget('WP_Widget_Search');
  //unregister_widget('WP_Widget_Text');
  //unregister_widget('WP_Widget_Categories');
  //unregister_widget('WP_Widget_Recent_Posts');
  //unregister_widget('WP_Widget_Recent_Comments');
  //unregister_widget('WP_Widget_RSS');
  //unregister_widget('WP_Widget_Tag_Cloud');
  //unregister_widget('WP_Nav_Menu_Widget');
}
add_action('widgets_init', 'unregister_default_wp_widgets', 1);

// Add the posts and pages columns filter. They can both use the same function.
add_filter('manage_posts_columns', 'tcb_add_post_thumbnail_column', 5);
add_filter('manage_pages_columns', 'tcb_add_post_thumbnail_column', 5);

// Add the column
function tcb_add_post_thumbnail_column($cols){
  $cols['tcb_post_thumb'] = __('Featured');
  return $cols;
}

// Hook into the posts an pages column managing. Sharing function callback again.
add_action('manage_posts_custom_column', 'tcb_display_post_thumbnail_column', 5, 2);
add_action('manage_pages_custom_column', 'tcb_display_post_thumbnail_column', 5, 2);

// Grab featured-thumbnail size post thumbnail and display it.
function tcb_display_post_thumbnail_column($col, $id){
  switch($col){
    case 'tcb_post_thumb':
      if( function_exists('the_post_thumbnail') )
        echo the_post_thumbnail( 'thumbnail' );
      else
        echo 'Not supported in theme';
      break;
  }
}

// remove post/pages metaboxes
function remove_extra_meta_boxes() {
//remove_meta_box( 'postcustom' , 'post' , 'normal' ); // custom fields for posts
//remove_meta_box( 'postcustom' , 'page' , 'normal' ); // custom fields for pages
//remove_meta_box( 'postexcerpt' , 'post' , 'normal' ); // post excerpts
//remove_meta_box( 'postexcerpt' , 'page' , 'normal' ); // page excerpts
//remove_meta_box( 'commentsdiv' , 'post' , 'normal' ); // recent comments for posts
//remove_meta_box( 'commentsdiv' , 'page' , 'normal' ); // recent comments for pages
//remove_meta_box( 'tagsdiv-post_tag' , 'post' , 'side' ); // post tags
//remove_meta_box( 'tagsdiv-post_tag' , 'page' , 'side' ); // page tags
//remove_meta_box( 'trackbacksdiv' , 'post' , 'normal' ); // post trackbacks
//remove_meta_box( 'trackbacksdiv' , 'page' , 'normal' ); // page trackbacks
//remove_meta_box( 'commentstatusdiv' , 'post' , 'normal' ); // allow comments for posts
//remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' ); // allow comments for pages
//remove_meta_box('slugdiv','post','normal'); // post slug
//remove_meta_box('slugdiv','page','normal'); // page slug
//remove_meta_box('pageparentdiv','page','side'); // Page Parent
}
add_action( 'admin_menu' , 'remove_extra_meta_boxes' );


// remove deactivate and edit form plugins
/*
function slt_lock_plugins( $actions, $plugin_file, $plugin_data, $context ) {
    // Remove edit link for all
    if ( array_key_exists( 'edit', $actions ) )
        unset( $actions['edit'] );
    // Remove deactivate link for crucial plugins
    if ( array_key_exists( 'deactivate', $actions ) && in_array( $plugin_file, array(
        'sitepress-multilingual-cms/sitepress.php',
        'wpml-string-translation/plugin.php',
        'wpml-media/plugin.php',
        'slimjetpack/slimjetpack.php',
        'tablepress/tablepress.php',
        
    )))
        unset( $actions['deactivate'] );
    return $actions;
}
add_filter( 'plugin_action_links', 'slt_lock_plugins', 10, 4 );
*/


function string_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit) {
  array_pop($words);
  //add a ... at last article when more than limit word count
  echo implode(' ', $words)."..."; } else {
  //otherwise
  echo implode(' ', $words); }
}


function limit_words($string, $word_limit) {

	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character

	$words = explode(' ', $string);

	// this next bit chops the $words array and sticks it back together
	// starting at the first word '0' and ending at the $word_limit
	// the $word_limit which is passed in the function will be the number
	// of words we want to use
	// implode glues the chopped up array back together using a space character

	return implode(' ', array_slice($words, 0, $word_limit));

}

// Replaces the excerpt "more" text by a link
/*
function new_excerpt_more($more) {
	global $post;
	return '<a class="read-more" href="'. get_permalink($post->ID) . '">weiterlesen</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
*/

// Changing excerpt more
function new_excerpt_more($more) {
	global $post;
    $post->excerpt_print = true;
    /* 	var_dump($more); */

    if (get_template_directory() == get_stylesheet_directory()) {
        return '<p><a class="read-more" href="'. get_permalink($post->ID) . '">' . __('weiterlesen','callwey') . '</a></p>';
    }
    else {
        return '<p><a class="read-more" href="'. get_permalink($post->ID) . '">' . __('weiterlesen','callwey-magazine') . '</a></p>';
    }
}
add_filter('excerpt_more', 'new_excerpt_more');

/*
add_filter('the_excerpt', 'new_excerpt_hellip');
function new_excerpt_hellip($text)
{
   //return str_replace('[...]', '&hellip;[+]', $text);
   return str_replace('[...]', '&hellip;<a class="read-more" href="'. get_permalink($post->ID) . '">weiterlesen</a>', $text);
}
*/


/* Add CSS to Visual Editor. */
$callwey_editor_stylesheet = 'css/editor.min.css';
add_editor_style($callwey_editor_stylesheet);

function callwey_editor_styles() {
	global $current_screen;
	switch ($current_screen->post_type) {
		case 'autor':
		add_editor_style('css/autor-editor.min.css');		
		break;
		case 'fotografen':
		add_editor_style('css/autor-editor.min.css');		
		break;
	}
}
add_action( 'admin_head', 'callwey_editor_styles' );

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
	}
add_filter('language_attributes', 'add_opengraph_doctype');

//Lets add Open Graph Meta Info

function insert_fb_in_head() {
	global $post;
	if ( !is_singular()) //if it is not a post or a page
		return;
        echo '<meta property="fb:admins" content="'. get_bloginfo('facebook_id') .'"/>'; //http://findmyfacebookid.com/
        echo '<meta property="og:title" content="' . get_the_title() . '"/>';

	while(have_posts()):the_post();
/* 		$out_excerpt = str_replace(array("\r\n", "\r", "\n"), "", get_the_excerpt()); */
/* 		echo '<meta property="og:description" content="'. apply_filters('the_excerpt_rss', $out_excerpt).'"/>'; */
	endwhile;

        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:url" content="' . get_permalink() . '"/>';
        echo '<meta property="og:site_name" content="'. get_bloginfo('name').'"/>';
	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		$default_image= get_bloginfo('og_image'); //replace this with a default image on your server or an image in your media library
		echo '<meta property="og:image" content="' . $default_image . '"/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "
";
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );


// Schema.org Description metas

function insert_schema_in_head() {
	global $post;
	echo '<meta itemprop="name" content="'. get_bloginfo('name') .'"/>';
	echo '<meta itemprop="description" content="'. get_bloginfo('description') .'"/>';
	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		$default_image= get_bloginfo('og_image'); //replace this with a default image on your server or an image in your media library
		echo '<meta itemprop="image" content="' . $default_image . '"/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '<meta itemprop="image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "
";
}
add_action( 'wp_head', 'insert_schema_in_head', 6 );

function callwey_product_add_taxonomy_filters() {
	global $typenow;
 
	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('kategorie');
 
	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'produkt' ){
 
		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ($terms as $term) { 
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'callwey_product_add_taxonomy_filters' );

///////Filter function//////////

function wp_get_object_terms_exclude_filter($terms, $object_ids, $taxonomies, $args) {

    //if exclude is set and fields is set to all go
    if(isset($args['exclude']) && $args['fields'] == 'all') {

        //loop through terms
        foreach($terms as $key => $term) {

            //loop through exclude terms
            foreach($args['exclude'] as $exclude_term) {

                //if exlude term matches current object term unset
                if($term->term_id == $exclude_term) {
                    unset($terms[$key]);
                }

            }

        }

    }

    //reset keys, because of unset.
    $terms = array_values($terms);

    return $terms;

}

add_filter('wp_get_object_terms', 'wp_get_object_terms_exclude_filter', 10, 4);


/*Include CPTs in frontend search*/
function SearchFilter($query) {
    if ($query->is_search && !is_admin()) {
        $query->set('post_type',array('post','produkt','ebook','rezepte'));
    }
return $query;
}
add_filter('pre_get_posts','SearchFilter');

/*Remove metaboxs from CPTs*/
add_action( 'add_meta_boxes', 'callwey_remove_meta_boxes' );
function callwey_remove_meta_boxes() {
	/* links */
    remove_meta_box( 'slugdiv', 'links', 'normal' );
    remove_meta_box( 'featured_video_plus-box', 'links', 'side' );
    remove_meta_box( 'sharing_meta', 'links', 'advanced' );
    /* Video */
    remove_meta_box( 'slugdiv', 'video', 'normal' );
    remove_meta_box( 'sharing_meta', 'video', 'advanced' );
    /* Staff */
    remove_meta_box( 'slugdiv', 'staff', 'normal' );
    remove_meta_box( 'featured_video_plus-box', 'staff', 'side' );
    remove_meta_box( 'sharing_meta', 'staff', 'advanced' );
    /* Jobs */
    remove_meta_box( 'slugdiv', 'jobs', 'normal' );
    remove_meta_box( 'featured_video_plus-box', 'jobs', 'side' );
    remove_meta_box( 'sharing_meta', 'jobs', 'advanced' );
    /* Autor */
    remove_meta_box( 'featured_video_plus-box', 'autor', 'side' );
    remove_meta_box( 'sharing_meta', 'autor', 'advanced' );
    /* Fotografen */
    remove_meta_box( 'slugdiv', 'fotografen', 'normal' );
    remove_meta_box( 'featured_video_plus-box', 'fotografen', 'side' );
    remove_meta_box( 'sharing_meta', 'fotografen', 'advanced' );
    /* update */
    remove_meta_box( 'slugdiv', 'update', 'normal' );
    remove_meta_box( 'featured_video_plus-box', 'update', 'side' );
    remove_meta_box( 'sharing_meta', 'update', 'advanced' );
    /* agent */
    remove_meta_box( 'slugdiv', 'agent', 'normal' );
    remove_meta_box( 'featured_video_plus-box', 'agent', 'side' );
    remove_meta_box( 'sharing_meta', 'agent', 'advanced' );
    /* rezepte */
    remove_meta_box( 'featured_video_plus-box', 'rezepte', 'side' );
    /* ebook */
    remove_meta_box( 'featured_video_plus-box', 'ebook', 'side' );
    /* produkt */
    remove_meta_box( 'featured_video_plus-box', 'produkt', 'side' );
    /* page */
    remove_meta_box( 'featured_video_plus-box', 'page', 'side' );
    /* post */
    remove_meta_box( 'featured_video_plus-box', 'post', 'side' );
    /* timeline */
    remove_meta_box( 'featured_video_plus-box', 'timeline', 'side' );
}

//Remove TTT-Gallery from specific CPTs
add_filter('ttt-gallery_post_types', 'custom_remove_cpt_gallery' );
function custom_remove_cpt_gallery ( $post_types ) {
   unset($post_types['links']);
   unset($post_types['autor']);
   unset($post_types['fotografen']);   
   unset($post_types['update']);
   unset($post_types['staff']);
   unset($post_types['jobs']);   
   unset($post_types['video']);
   unset($post_types['timeline']); 
   unset($post_types['agent']);
   unset($post_types['produkte']);  
   return $post_types;
};

//Remove TTT-magento from specific CPTs
add_filter('ttt-magentorest_post_types', 'custom_remove_cpt_magento');
function custom_remove_cpt_magento ( $post_types ) {
    if (!is_array($post_types)) return false;

    foreach ($post_types as $key => $value ) {
        if ($key == 'produkt' || $key == 'ebook' || $key == 'magazine' || $key == 'produkte')
            $new_post_types[ $key ] = $value;
    }
    
    return $new_post_types;
}
/*

// Hook our custom function onto the request filter
add_filter(‘pre_get_posts’, ‘change_wp_search_size’);
//To remove default number of posts appearing on search results and category
function change_wp_search_size($query) {
if ( $query->is_search || $query->is_archive) // Make sure it is a search page
$query->query_vars['posts_per_page'] = -1;
// Change -1 to the number of posts you would like to show

return $query; // Return our modified query variables
}
*/

/* ADDING NEW METABOX JSUT FOR CPT-JOBS to select the uploaded PDF files. */

add_action("admin_init", "pdf_init");
add_action('save_post', 'save_pdf_link');
function pdf_init(){
        add_meta_box("my-pdf", "PDF Document", "pdf_link", "jobs", "normal", "low");
        }
function pdf_link(){
        global $post;
        $custom  = get_post_custom($post->ID);
        $link    = $custom["link"][0];
        $count   = 0;
        echo '<div class="link_header">';
        $query_pdf_args = array(
                'post_type' => 'attachment',
                'post_mime_type' =>'application/pdf',
                'post_status' => 'inherit',
                'posts_per_page' => -1,
                );
        $query_pdf = new WP_Query( $query_pdf_args );
        $pdf = array();
        echo '<select name="link">';
        echo '<option class="pdf_select">SELECT pdf FILE</option>';
        foreach ( $query_pdf->posts as $file) {
           if($link == $pdf[]= $file->guid){
              echo '<option value="'.$pdf[]= $file->guid.'" selected="true">'.$pdf[]= $file->guid.'</option>';
                 }else{
              echo '<option value="'.$pdf[]= $file->guid.'">'.$pdf[]= $file->guid.'</option>';
                 }
                $count++;
        }
        echo '</select><br /></div>';
        echo '<p>Selecting a pdf file from the above list to attach to this post.</p>';
        echo '<div class="pdf_count"><span>Files:</span> <b>'.$count.'</b></div>';
}
function save_pdf_link(){
        global $post;
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){ return $post->ID; }
        update_post_meta($post->ID, "link", $_POST["link"]);
}
add_action( 'admin_head', 'pdf_css' );
function pdf_css() {
        echo '<style type="text/css">
        .pdf_select{
                font-weight:bold;
                background:#e5e5e5;
                }
        .pdf_count{
                font-size:9px;
                color:#0066ff;
                text-transform:uppercase;
                background:#f3f3f3;
                border-top:solid 1px #e5e5e5;
                padding:6px 6px 6px 12px;
                margin:0px -6px -8px -6px;
                -moz-border-radius:0px 0px 6px 6px;
                -webkit-border-radius:0px 0px 6px 6px;
                border-radius:0px 0px 6px 6px;
                }
        .pdf_count span{color:#666;}
                </style>';
}
function pdf_file_url(){
        global $post;
        $custom = get_post_custom($wp_query->post->ID);
        echo $custom['link'][0];
}

/* Remove img width & height */

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
/* add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 ); */

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

if (defined('TTT_BLOCKUPDATES') && TTT_BLOCKUPDATES == true) {

    function ttt_fake_check_version($a) {
        global $wp_version;
        return (object) array(
            'last_checked' => time(),
            'version_checked' => $wp_version,
        );
    }
    add_filter('pre_site_transient_update_core',    'ttt_fake_check_version');
    add_filter('pre_site_transient_update_plugins', 'ttt_fake_check_version');
    add_filter('pre_site_transient_update_themes',  'ttt_fake_check_version');

    // //Disable Theme Updates # 3.0+
    // remove_action( 'load-update-core.php', 'wp_update_themes' );
    // add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );
    // wp_clear_scheduled_hook( 'wp_update_themes' );
    // 
    // //Disable Plugin Updates #3.0+
    // remove_action( 'load-update-core.php', 'wp_update_plugins' );
    // add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
    // wp_clear_scheduled_hook( 'wp_update_plugins' );
    // 
    // //Diasable Core Updates # 3.0+
    // add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );
    // wp_clear_scheduled_hook( 'wp_version_check' );
}

add_action( 'init', 'vipx_allow_contenteditable_on_divs' );
function vipx_allow_contenteditable_on_divs() {
    global $allowedposttags;

    $tags = array( 'iframe' );
    $new_attributes = array(
        'src' => true,
        'id' => true,
        'class' => true,
        'href' => true,
        'style' => true,
        'width' => true,
        'height' => true,
        'frameborder' => true
    );

    foreach ( $tags as $tag ) {
        if ( !isset( $allowedposttags[ $tag ] ) )
            $allowedposttags[ $tag ] = $new_attributes;
    }
}

add_filter('tiny_mce_before_init', 'vipx_filter_tiny_mce_before_init'); 
function vipx_filter_tiny_mce_before_init( $options ) { 
    if ( ! isset( $options['extended_valid_elements'] ) ) 
        $options['extended_valid_elements'] = ''; 

    $options['extended_valid_elements'] .= ',iframe[src|id|class|href|style|width|height|frameborder]'; 
    return $options; 
}
