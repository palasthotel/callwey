<?php

class TTTslideshow_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Slidehow','description=Gallery Slideshow.');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
		?>
		<?php if (is_tttdevice('tablet') ): ?>
			<div class="medium-6 columns">
		<?php endif; ?>
			<aside id="slideshow" class="widget">
				<div class="widget-container">
					<h4 class="widget-title"><?php _e('Slideshow', 'callwey'); ?></h4>
					<div class="gallery"><?php echo do_shortcode('[tttgallery template="slideshow-widget" id="1"]'); ?></div>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') ): ?>
			</div>
		<?php endif; ?>
		<?php
        }

}
register_widget( 'TTTslideshow_widget' );

?>
