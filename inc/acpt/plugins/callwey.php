<?php

add_action('init', 'acpt_rezepte_init');
function acpt_rezepte_init() {

  $args_rezepte = array(
  'supports' => array( 'title', 'editor', 'thumbnail'  ),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false ),
  'labels' => array(  
	'name'               => 'Rezepte',
    'singular_name'      => 'Rezept',
    'add_new'            => 'neues hinzufügen',
    'add_new_item'       => 'neues Rezept hinzufügen',
    'edit_item'          => 'Rezept bearbeiten',
    'new_item'           => 'Neues Rezept',
    'all_items'          => 'alle Rezepte',
    'view_item'          => 'Rezept ansehen',
    'search_items'       => 'Rezepte suchen',
    'not_found'          => 'keine Rezepte gefunden',
    'not_found_in_trash' => 'kein Rezept im Trash gefunden',
    'parent_item_colon'  => '',
    'menu_name'          => 'Rezepte'
  ),
  );

  $rezepte = acpt_post_type('rezepte','rezeptes', false,  $args_rezepte )->icon('dine');

/*
  acpt_tax('type', 'types', $rezepte, true, false);
  
  acpt_tax('ingredient', 'ingredients', $rezepte, false, false);  
*/

  $args_produkt = array(
  'supports' => array( 'title', 'editor', 'thumbnail'  ),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false, 'slug' => 'buecher'  ),
  'labels' => array(
	'name'               => 'Bücher',
	'singular_name'      => 'Buch',
	'add_new'            => 'Add New',
	'add_new_item'       => 'neues Buch hinzufügen',
	'edit_item'          => 'Buch bearbeiten',
	'new_item'           => 'Neues Buch',
	'all_items'          => 'alle Bücher',
	'view_item'          => 'Buch ansehen',
	'search_items'       => 'Bücher suchen',
	'not_found'          => 'keine Bücher gefunden',
	'not_found_in_trash' => 'kein Buch im Trash gefunden',
	'parent_item_colon'  => '',
	'menu_name'          => 'Bücher'
  ),
/*   'yarpp_support' => true */

  );

  $produkt = acpt_post_type('produkt','produkts', false,  $args_produkt )->icon('box');



  $args_ebook = array(
  'supports' => array( 'title', 'editor', 'thumbnail'  ),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false, 'slug' => 'ebooks'  ),
  'labels' => array(
	'name'               => 'Ebook',
	'singular_name'      => 'Ebook',
	'add_new'            => 'Add New',
	'add_new_item'       => 'neues Ebook hinzufügen',
	'edit_item'          => 'Ebook bearbeiten',
	'new_item'           => 'Neues Ebook',
	'all_items'          => 'alle Ebook',
	'view_item'          => 'Ebook ansehen',
	'search_items'       => 'Ebook suchen',
	'not_found'          => 'keine Ebook gefunden',
	'not_found_in_trash' => 'kein Ebook im Trash gefunden',
	'parent_item_colon'  => '',
	'menu_name'          => 'Ebook'
  ),
/*   'yarpp_support' => true */

  );

  $ebook = acpt_post_type('ebook','ebooks', false,  $args_ebook )->icon('box');


  
  $settings = array(
      'public' => true,
      'query_var' => true,
      'hierarchical' => false,
      'rewrite' => array(
          'slug' => 'buecherangebot'
      )
    );

  acpt_tax('kategorie', 'katerogies', array($produkt,$ebook), true, false, $settings);

  $args_autor = array(
  'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'  ),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false )  
  );

  $autor = acpt_post_type('autor','autors', false,  $args_autor )->icon('person');

  acpt_tax('typ', 'typ', $autor, true, false);

  $args_photographer = array(
  'supports' => array( 'title', 'editor', 'thumbnail'  ),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false )  
  );

  $photographer = acpt_post_type('fotografen','fotografen', false,  $args_photographer )->icon('person');

  $args_video = array(
  'supports' => array( 'title', 'thumbnail'  ),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false ) ,
  'show_in_menu' => 'edit.php?post_type=produkt'
  );

  $video = acpt_post_type('video','videos', false,  $args_video )->icon('color');

  $args_jobs = array(
  'supports' => array( 'title', 'editor'),
  'hierarchical' => false,
  'rewrite' => array( 'with_front' => false, 'slug' => 'job' )  
  );

  $jobs = acpt_post_type('jobs','jobs', false,  $args_jobs )->icon('card-biz');

  acpt_tax('location', 'location', $jobs, true, false);
  
  $args_staff = array(
  'supports' => array( 'title', 'thumbnail', 'page-attributes'  ),
  'hierarchical' => true,
  'rewrite' => array( 'with_front' => false/* , 'slug' => 'organisation' */ )  
  );

  $staff = acpt_post_type('staff','staff', false,  $args_staff )->icon('card-biz');

  acpt_tax('group', 'group', $staff, true, false);

  $args_update = array(
  'supports' => array( 'title' ),
  'hierarchical' => true,
  'rewrite' => array( 'with_front' => false/* , 'slug' => 'organisation' */ ),
  'labels' => array(
	'name' => 'Pressebox', 
	'singular_name' => 'Pressebox',
	'add_new' => 'Add New',
	'add_new_item' => 'neuen Eintrag erstellen',
	'edit_item' => 'Eintrag editieren',
	'new_item' => 'neuen Eintrag',
	'all_items' => 'alle Einträge',
	'view_item' => 'alle Einträge ansehen',
	'search_items' => 'Einträge suchen',
	'not_found' => 'keine Einträge gefunden',
	'not_found_in_trash' => 'kein Eintrag im Trash gefunden',
	'parent_item_colon' => '', 
	'menu_name' => 'Pressebox'	
  ),   
  );

  $update = acpt_post_type('update','update', false,  $args_update )->icon('mic');  

  $args_links = array(
  'supports' => array( 'title' ),
  'hierarchical' => true,
  'rewrite' => array( 'with_front' => false, 'slug' => 'link' ),
   'labels' => array(
	'name'               => 'Top 5 links',
	'menu_name'          => 'Top 5 links'
  ),    
  );

  $links = acpt_post_type('links','links', false,  $args_links )->icon('thumbs-up');  

  $args_timeline = array(
  'supports' => array( 'title', 'thumbnail', 'editor'  ),
  'hierarchical' => true,
  'rewrite' => array( 'with_front' => false )
  );

  $timeline = acpt_post_type('timeline','timeline', false,  $args_timeline );

  $args_agent = array(
  'supports' => array( 'title', 'editor' ),
  'hierarchical' => true,
  'rewrite' => array( 'with_front' => false ),
  'labels' => array(
	'name' => 'Vertretungen', 
	'singular_name' => 'Vertretungen',
	'add_new' => 'Add New',
	'add_new_item' => 'neuen Vertretungen',
	'edit_item' => 'edit Vertretungen',
	'new_item' => 'neuen Vertretungen',
	'all_items' => 'alle Vertretungen',
	'view_item' => 'alle Vertretungen',
	'search_items' => 'Vertretungen suchen',
	'not_found' => 'keine Vertretungen gefunden',
	'not_found_in_trash' => 'kein Vertretungen im Trash gefunden',
	'parent_item_colon' => '', 
	'menu_name' => 'Vertretungen'	
  ),   
  );

  $agent = acpt_post_type('agent','Agents', false,  $args_agent )->icon('card-biz');

}
