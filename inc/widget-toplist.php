<?php

class TTTPromoted_blog_widget extends WP_Widget {
        public function __construct() {
               // widget actual processes
               parent::WP_Widget(false,'TTT Toplist','description=Promoted content. This widget use CrossOrder .');
        }

        public function form( $instance ) {
               //echo 'include html coding in here';
        }

        public function update( $new_instance, $old_instance ) {
               // processes widget options to be saved
        }

        public function widget( $args, $instance ) {
		global $post;     
        global $blog_id;    
		?>
		<?php if (is_tttdevice('tablet') ): ?>
			<div class="medium-6 columns">
		<?php endif; ?>
			<aside id="toplist" class="widget">
				<div class="widget-container">
					<h4 class="widget-title">
                        <?php if ( $blog_id == 9 ) : ?>
	    				    <?php _e('PRODUKT <br> empfehlungen', 'callwey-familien'); ?>
                        <?php else: ?>
	    				    <?php _e('blog <br> empfehlungen', 'callwey'); ?>
                        <?php endif; ?>
                    </h4>
					<ul data-orbit data-options="animation:push; timer:true; timer_speed:3000; bullets:false; slide_number:false; navigation_arrows:false; timer_container_class:hide; variable_height:true; pause_on_hover:true; resume_on_mouseout:true; animation_speed:500;">
						<?php
							$_first = true;
							$CrossOrder = new CrossOrder();
							$the_query = $CrossOrder->order('posts_toplist');
						?>
						<?php if ($the_query->have_posts()) : ?>
							<?php $count = 1; ?>
							<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
							<?php
								$link_url = get_post_meta( $post->ID, '_clwy_url_link', true );
							?>							
							<li>
								<?php if ($count == 1) : ?>
									<div class="number-wrap">
										<div class="number-back">
											<div class="number post-1">
												<?php if ( 'links' == get_post_type() ): ?>
													<a target="_blank" href="<?php echo $link_url; ?>">
												<?php else: ?>
													<a href="<?php the_permalink(); ?>">
												<?php endif; ?>
														1
													</a>
											</div>
										</div>
									</div>
								<?php elseif ($count == 2) : ?>							
									<div class="number-wrap">
										<div class="number-back">
											<div class="number post-2">
												<?php if ( 'links' == get_post_type() ): ?>
													<a target="_blank" href="<?php echo $link_url; ?>">
												<?php else: ?>
													<a href="<?php the_permalink(); ?>">
												<?php endif; ?>
														2
													</a>
											</div>
										</div>
									</div>
								<?php elseif ($count == 3) : ?>							
									<div class="number-wrap">
										<div class="number-back">
											<div class="number post-3">
												<?php if ( 'links' == get_post_type() ): ?>
													<a target="_blank" href="<?php echo $link_url; ?>">
												<?php else: ?>
													<a href="<?php the_permalink(); ?>">
												<?php endif; ?>
														3
													</a>
											</div>
										</div>
									</div>
								<?php elseif ($count == 4) : ?>							
									<div class="number-wrap">
										<div class="number-back">
											<div class="number post-4">
												<?php if ( 'links' == get_post_type() ): ?>
													<a target="_blank" href="<?php echo $link_url; ?>">
												<?php else: ?>
													<a href="<?php the_permalink(); ?>">
												<?php endif; ?>
														4
													</a>
											</div>
										</div>
									</div>
								<?php elseif ($count == 5) : ?>							
									<div class="number-wrap">
										<div class="number-back">
											<div class="number post-5">
												<?php if ( 'links' == get_post_type() ): ?>
													<a target="_blank" href="<?php echo $link_url; ?>">
												<?php else: ?>
													<a href="<?php the_permalink(); ?>">
												<?php endif; ?>
														5
													</a>
											</div>
										</div>
									</div>
								<?php endif; $count++; ?>
								<h2 class="entry-title">
									<?php if ( 'links' == get_post_type() ): ?>
									<a target="_blank" href="<?php echo $link_url; ?>">
									<?php else: ?>
									<a href="<?php the_permalink(); ?>">
									<?php endif; ?>
										<?php the_title(); ?>
									</a>
								</h2>
							</li>
							<?php $_first = false; endwhile; ?>
						<?php else : ?>
						<?php endif;  wp_reset_postdata(); ?>
						
					</ul>
				</div>
			</aside>
		<?php if (is_tttdevice('tablet') ): ?>
			</div>
		<?php endif; ?>
		<?php
        }

}
register_widget( 'TTTPromoted_blog_widget' );

?>
