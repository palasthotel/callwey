<?php
/**
 * foundation icons
 */
function foundation_icons_styles() {
    wp_enqueue_style( 'foundation_icons_styles', get_template_directory_uri() . '/fonts/foundation-icons/foundation-icons.css' );
}

add_action( 'wp_enqueue_scripts', 'foundation_icons_styles' );