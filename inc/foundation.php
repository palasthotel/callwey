<?php
/**
 * foundation.min.css
 */
function foundation_stylesheet() {
    wp_enqueue_style( 'normalize.min', get_template_directory_uri() . '/inc/foundation/css/normalize.min.css' );
    wp_enqueue_style( 'foundation.min', get_template_directory_uri() . '/inc/foundation/css/foundation.min.css' );
}

add_action( 'wp_enqueue_scripts', 'foundation_stylesheet' );


/**
 * foundation js
 */
function foundation_script() {
	wp_enqueue_script('custom.modernizr',		get_template_directory_uri() . '/inc/foundation/js/vendor/custom.modernizr.js' );	
	wp_enqueue_script('foundation',				get_template_directory_uri() . '/inc/foundation/js/foundation.js', array('jquery') );
	wp_enqueue_script('foundation.alerts',			get_template_directory_uri() . '/inc/foundation/js/foundation.alerts.js', array('foundation') ); //used in header.php fo the alert message
	wp_enqueue_script('foundation.clearing',			get_template_directory_uri() . '/inc/foundation/js/foundation.clearing.js', array('foundation') );	//used for galleries lightbox.
	wp_enqueue_script('foundation.cookie',			get_template_directory_uri() . '/inc/foundation/js/foundation.cookie.js', array('jquery') );			
	wp_enqueue_script('foundation.dropdown',	get_template_directory_uri() . '/inc/foundation/js/foundation.dropdown.js', array('foundation') ); //use for dropdown menus & mobile menu
	wp_enqueue_script('foundation.forms',			get_template_directory_uri() . '/inc/foundation/js/foundation.forms.js', array('foundation') );	
	//wp_enqueue_script('foundation.joyride',			get_template_directory_uri() . '/inc/foundation/js/foundation.joyride.js', array('jquery') );	
	wp_enqueue_script('foundation.magellan',			get_template_directory_uri() . '/inc/foundation/js/foundation.magellan.js', array('foundation') ); //used to fix top-bar menu for desktop mobile width. check header.php	
	wp_enqueue_script('foundation.orbit',			get_template_directory_uri() . '/inc/foundation/js/foundation.orbit.js', array('foundation') ); //all site slider use this script.		
	wp_enqueue_script('foundation.placeholder',			get_template_directory_uri() . '/inc/foundation/js/foundation.placeholder.js', array('foundation') );	
	wp_enqueue_script('foundation.reveal',			get_template_directory_uri() . '/inc/foundation/js/foundation.reveal.js', array('foundation') ); //used for lightbox divs. check partials/content-carrera.php
	wp_enqueue_script('foundation.section',			get_template_directory_uri() . '/inc/foundation/js/foundation.section.js', array('foundation') );//used inside the mobile. check header.php
	wp_enqueue_script('foundation.tooltips',			get_template_directory_uri() . '/inc/foundation/js/foundation.tooltips.js', array('foundation') ); //used in the social infoof CPT docente. check partials/content-docente.php
	wp_enqueue_script('foundation.topbar',			get_template_directory_uri() . '/inc/foundation/js/foundation.topbar.js', array('foundation') ); //used for desktop browser with mobile width. check header.php
	wp_enqueue_script('foundation.interchange',			get_template_directory_uri() . '/inc/foundation/js/foundation.interchange.js', array('foundation') );
}	
	
add_action('wp_enqueue_scripts', 'foundation_script');