<?php
/**
 * The template for displaying Author archive pages.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">
	<header class="archive-header row">
		<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
<!-- 	<h3 class="site-section text-center"><?php _e('Blog', 'callwey'); ?></h3> -->

	<?php if ( have_posts() ) : ?>

		<?php
			/* Queue the first post, that way we know
			 * what author we're dealing with (if that is the case).
			 *
			 * We reset this later so we can run the loop
			 * properly with a call to rewind_posts().
			 */
			the_post();
		?>

			<h3 class="archive-title text-center">
				<?php if ( get_the_author_meta('single_headline')): ?>
					<?php the_author_meta( 'single_headline' ); ?>
				<?php else: ?>
					<?php printf( __( '%s', 'callwey' ), '<span class="vcard">' . get_the_author() . '</span>' ); ?>
				<?php endif; ?>
			</h3>
			<div class="medium-17 medium-centered columns">
				<?php if ( get_the_author_meta( 'description' ) ) : ?>
					<?php get_template_part( 'author-bio' ); ?>
				<?php endif; ?>
			</div>

		<?php
			/* Since we called the_post() above, we need to
			 * rewind the loop back to the beginning that way
			 * we can run the loop properly, in full.
			 */
			rewind_posts();
		?>
	</header>
	
		<div id="primary" class="content-area large-13 medium-13 small-13 columns">
			<main id="main" class="site-main row" role="main">		
	
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'partials/content', 'content' ); ?>
				<?php endwhile; ?>
	
			</main><!-- #main -->
		</div><!-- #primary -->

	<?php else : ?>
		<?php get_template_part( 'content', 'none' ); ?>
	<?php endif; ?>

	<?php get_sidebar(); ?>
	
</div>

<?php get_footer(); ?>