<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _s
 */
?>
<?php
if ( defined( 'WP_INSTALLING' ) and WP_INSTALLING )
{
    locate_template( 'footer-activate.php', TRUE, TRUE );
    return;
}
?>
	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php if ( is_tttdevice('desktop') ): ?>	
			<div class="site-contact large-17 large-centered medium-17 medium-centered columns">
				<div class="row site-contact-top">
					<form class="custom newsletter large-9 medium-12 columns" name="ProfileForm" onsubmit="return CheckInputs(this);" action="http://news.callwey.de/u/register.php" method="get">
						<fieldset class="row">
							<legend class="hide">Newsletter</legend>
							<label class="large-8 medium-9 columns"><?php _e('Newsletter registrieren', 'callwey-magazine'); ?></label>
                            <input type="hidden" name="CID" value="128435595">
                            <input type="hidden" name="SID" value="<? echo $SID; ?>">
							<input type="hidden" name="UID" value="<? echo $UID; ?>">
							<input type="hidden" name="f" value="665">
							<input type="hidden" name="p" value="2">
							<input type="hidden" name="a" value="r">
							<input type="hidden" name="el" value="<? echo $el; ?>">
                            <input type="hidden" name="endlink" value="<?php echo get_bloginfo('url').'/newsletter/'; ?>">
							<input type="hidden" name="llid" value="<? echo $llid; ?>">
							<input type="hidden" name="c" value="<? echo $c; ?>">
							<input type="hidden" name="counted" value="<? echo $counted; ?>">
							<input type="hidden" name="RID" value="<? echo $RID; ?>">
							<input type="hidden" name="mailnow" value="<? echo $mailnow; ?>">
							<div class="large-8 medium-8 columns">
                                <input type="text" class="round" name="inp_3" maxlength="255" value="<? echo $inp_3; ?>" placeholder="<?php _e('E-MAIL-ADRESSE','callwey-magazine'); ?>">
							</div>
							<input type="hidden" name="<?php echo of_get_option('newsletter_inp'); ?>" value="<?php echo of_get_option('newsletter_inp_value'); ?>">
							
						</fieldset>
					</form>
					<? if($alertbox != "") { echo "\r\n<script language=\"javascript\">\r\nalert(\"$alertbox\");</script>"; } ?>

					<div class="large-7 medium-6 small-18 columns">
						<ul class="inline-list social-networks">
							<?php if ( of_get_option('fb_page') ): ?>				
							<li>
								<a href="<?php echo of_get_option('fb_page'); ?>" target="_blank">
									<span aria-hidden="true" data-icon class="icon-facebook"></span>
									<div>facebook</div>
								</a>
							</li>
							<?php endif; ?>
							<?php if ( of_get_option('twitter') ): ?>				
							<li>
								<a href="<?php echo of_get_option('twitter'); ?>" target="_blank">
								<span aria-hidden="true" data-icon class="icon-twitter"></span>
								<div>twitter</div>
							</a>
							</li>
							<?php endif; ?>
						</ul>
					</div>
					<div class="goto-top large-2 medium-2 columns hide-for-medium-down">
						<a href="#" class="row"><?php _e('nach oben','callwey'); ?></a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	
		<?php if ( is_tttdevice('mobile') || is_tttdevice('tablet') ): ?>
			<?php get_template_part( 'footer-tablet-mobile' ); ?>
		<?php endif; ?>
		
		<div class="nav-wrapper site-info">
		<?php if ( is_tttdevice('mobile') ): ?>
			<div class="nav-inner-wrapper">
				<nav id="footer-navigation" class="site-navigation row" role="navigation">
					<section class="top-bar-section">
					<?php wp_nav_menu( array( 
						'theme_location' => 'secondary',
						'menu_class' => 'left',
						'container' => '',
						'container_class' => '',
					) ); ?>
					</section>
				</nav><!-- #site-navigation -->
			</div>
		<?php elseif ( is_tttdevice('tablet') ): ?>		
			<div class="nav-inner-wrapper">
				<nav id="footer-navigation" class="site-navigation top-bar row" role="navigation">
					<section class="top-bar-section">
					<?php wp_nav_menu( array( 
						'theme_location' => 'secondary',
						'menu_class' => 'left',
						'container' => '',
						'container_class' => '',
					) ); ?>
					</section>
				</nav><!-- #site-navigation -->
			</div>
		<?php else: ?>
			<div class="nav-inner-wrapper large-17 large-centered medium-17 medium-centered columns">
				<nav id="footer-navigation" class="site-navigation top-bar row" role="navigation">
					<ul class="title-area hide-for-medium-portrait">
						<!-- Title Area -->
						<li class="name">
							<h1><a href="#">Menu</a></h1>
						</li>
						<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
						<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
					</ul>
					<section class="top-bar-section">
					<?php wp_nav_menu( array( 
						'theme_location' => 'secondary',
						'menu_class' => 'left',
						'container' => '',
						'container_class' => '',
					) ); ?>
					</section>
				</nav><!-- #site-navigation -->
			</div>
		<?php endif; ?>
			<?php do_action( '_s_credits' ); ?>			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
