<?php
/**
Template Name: Featured
 *
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<header class="page-header">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->	
	
	<hr>

	<div id="primary" class="content-area medium-17 medium-centered columns">
		<main id="main" class="site-main row" role="main">		

			<?php while ( have_posts() ) : the_post(); ?>
			
				<?php get_template_part( 'partials/content', 'page-featured' ); ?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php get_sidebar(); ?>

</div>
<?php get_footer(); ?>
