<?php
/**
 * @package _s
 */
?>
<?php
	$ee = get_the_excerpt();

	/* translators: used between list items, there is a space after the comma */
	$category_list = get_the_category_list();

	/* translators: used between list items, there is a space after the comma */
	$tag_list = get_the_tag_list( '', __( ', ', 'callwey' ) );
	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('medium-16 medium-centered columns'); ?>>
	<header class="entry-header">
		<div class="entry-meta">
			<div class="entry-meta-devider medium-18 columns">
				<?php _s_posted_date(); ?>
				<div class="entry-categorie"><?php echo $category_list; ?></div>
			</div>
		</div><!-- .entry-meta -->
		<div class="medium-17 medium-push-2 columns">
		<?php if ( is_tttdevice('mobile')): ?>
			<div class="entry-thumbnail row">
		<?php else: ?>
			<div class="entry-thumbnail text-center">		
		<?php endif; ?>
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('single-landscape'); ?></a>
			</div>
			<h2 class="entry-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="entry-meta">
				<?php _s_posted_author(); ?>
			</div>
		</div>
	</header><!-- .entry-header -->
	<div class="medium-17 medium-push-2 columns entry-content-wrapper">
		<?php if ( !is_tttdevice('mobile') ): ?>
			<div class="entry-content">		
				<?php the_excerpt(); ?>
				<?php if(!$post->excerpt_print) : ?>
					<p><a class="read-more" href="<?php the_permalink(); ?>"><?php _e('weiterlesen','callwey-magazine')?></a></p>
				<?php endif; ?>
		<?php else: ?>
			<div class="entry-content text-right">		
					<p><a class="read-more" href="<?php the_permalink(); ?>"><?php _e('weiterlesen','callwey-magazine')?></a></p>
		<?php endif; ?>
			</div><!-- .entry-content -->
	</div>
	<div class="row"></div>
</article><!-- #post-## -->
