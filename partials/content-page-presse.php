<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _s
 */
	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	<?php if ( has_post_thumbnail() ): ?>
		<div class="medium-4 columns">
			<?php the_post_thumbnail('page'); ?>
		</div>
		<div class="page-content medium-14 columns">
	<?php else: ?>
		<div class="page-content medium-18 columns">	
	<?php endif; ?>
			<?php the_content(); ?>
		</div><!-- .entry-content -->
</article><!-- #post-## -->