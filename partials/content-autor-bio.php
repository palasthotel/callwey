<?php
/**
 * The template for displaying CPT-autor content.
 *
 * @package _s
 */
global $post;
$pageintro = get_post_meta( $post->ID, '_clwy_page_intro', true );	
?>

<div class="author-info">
	<div class="author-avatar medium-4 columns">
		<?php the_post_thumbnail('autor-single'); ?>
	</div><!-- .author-avatar -->
	<div class="author-description medium-14 columns">
		<h2 class="author-title hide"><?php _e( 'Über %s', 'callwey' );?> <?php the_title(); ?></h2>
		<p class="author-bio">
			<?php if (is_page_template('page-template-towidget.php')): ?>
				<?php echo $pageintro; ?>
			<?php else: ?>
				<?php the_excerpt(); ?>
			<?php endif; ?>
		</p>
	</div><!-- .author-description -->
</div><!-- .author-info -->