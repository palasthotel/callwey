<?php
/**
 * @package _s
 */
?>
<?php
	$ee = get_the_excerpt();

	/* translators: used between list items, there is a space after the comma */
	$category_list = get_the_category_list();

	/* translators: used between list items, there is a space after the comma */
	$tag_list = get_the_tag_list( '', __( ', ', 'callwey' ) );
	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('medium-16 medium-centered columns'); ?>>
	<header class="entry-header">
		<div class="entry-meta">
			<div class="entry-meta-devider medium-18 columns">
			<?php if ( 'post' == get_post_type() ): ?>
				<div class="row"><?php _s_posted_date(); ?></div>
				<div class="entry-categorie"><?php echo $category_list; ?></div>
			<?php endif; ?>
			</div>
		</div><!-- .entry-meta -->
		<div class="medium-17 medium-push-2 columns">
			<div class="entry-thumbnail text-center">
				<a href="<?php the_permalink(); ?>">
					<?php if ( 'post' == get_post_type() ): ?>
						<?php the_post_thumbnail('single-landscape'); ?>
					<?php elseif ( 'produkt' == get_post_type() ): ?>
						<?php the_post_thumbnail('single-book'); ?>
					<?php elseif ( 'ebook' == get_post_type() ): ?>
						<?php the_post_thumbnail('single-book'); ?>
					<?php elseif ( 'rezepte' == get_post_type() ): ?>
						<?php the_post_thumbnail('single-landscape'); ?>
					<?php endif; ?>
				</a>
			</div>
			<h2 class="entry-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php if ( 'post' == get_post_type() ): ?>
				<div class="entry-meta">
					<?php _s_posted_author(); ?>		
				</div>
			<?php endif; ?>
		</div>
	</header><!-- .entry-header -->
	<div class="medium-17 medium-push-2 columns entry-content-wrapper">
	
		<div class="entry-content">
			<?php the_excerpt(); ?>
			<?php if(!$post->excerpt_print) : ?>
				<p><a class="read-more" href="<?php the_permalink(); ?>"><?php _e('weiterlesen','callwey-magazine')?></a></p>
			<?php endif; ?>
		</div><!-- .entry-content -->
	</div>
	<div class="row"></div>
</article><!-- #post-## -->
