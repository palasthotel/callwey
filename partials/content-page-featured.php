<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _s
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ): ?>
		<div class="medium-8 columns">
			<?php the_post_thumbnail('page'); ?>
		</div>
		<div class="page-content medium-10 columns">
	<?php else: ?>
		<div class="page-content medium-18 columns">	
	<?php endif; ?>
			<?php the_content(); ?>
		</div><!-- .entry-content -->
</article><!-- #post-## -->
