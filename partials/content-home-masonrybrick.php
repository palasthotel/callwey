<div class="masonry-brick">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( 'post' == get_post_type() ) : ?>
			<?php if ( $_gallery = do_shortcode('[tttgallery template="masonry-orbit"]') ):  ?>		
				<?php echo $_gallery; ?>
            <?php else: ?>
                <a class="entry-thumbnail" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('masonry-home', array('itemprop' => 'image')); ?></a>
			<?php endif; ?>
		<?php elseif ( 'produkt' == get_post_type() || 'ebook' == get_post_type() ) : ?>
			<?php if ( $_gallery = do_shortcode('[tttgallery template="masonry-orbit"]') ):  ?>		
				<?php echo $_gallery; ?>
            <?php else: ?>
                <a class="entry-thumbnail" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('masonry-home', array('itemprop' => 'image')); ?></a>
			<?php endif; ?>
		<?php endif; ?>
		<header class="entry-header">
			<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<a class="goto-entry" href="<?php the_permalink(); ?>">></a>
		</header><!-- .entry-header -->
	</article>
</div>
