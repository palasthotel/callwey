<?php
/**
 * @package _s
 */

global $blog_id;

    /* translators: used between list items, there is a space after the comma */
    $category_list = get_the_category_list();

    /* translators: used between list items, there is a space after the comma */
    $tag_list = get_the_tag_list( '', __( ', ', 'callwey' ) );
    
    // Find connected CPT-autor
    $connected_post = new WP_Query( array(
        'connected_type' => 'book_post',
        'connected_items' => get_queried_object(),
        'nopaging' => true,
    ) );    
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('large-16 large-centered medium-16 medium-centered small-18 columns'); ?>>
        <header class="entry-header row">
            <h1 class="entry-title text-center"><?php the_title(); ?></h1>
            <div class="entry-meta">
                <?php _s_posted_author(); ?>
                <div class="entry-meta-devider medium-13 small-9 columns">
                    <div class="row">
                        <div class="large-2 medium-2 columns"><?php _s_posted_date(); ?></div>
                        <?php if (is_singular('rezepte')): ?>
                        <?php else: ?>
                    		<?php if ( $blog_id == 9 ) : ?>
                    		    <?php if (has_category('profile')): //conditional for links back to the category PROFILE filter link and no /blog link. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog large-16 medium-16 columns hide-for-medium-down" href="<?php bloginfo('url'); ?>/profile"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php elseif (has_category(array('finanzierung','gesundheit','interior-design','material'))): //conditional for links back to the categories under EXPERTEN page. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog large-16 medium-16 columns hide-for-medium-down" href="<?php bloginfo('url'); ?>/experten"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php elseif (has_category(array('wohnen-kueche','bad-wc','innenausbau','haustechnik','dach-fassade','leuchten'))): //conditional for links back to the categories under PRODUKTE page. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog large-16 medium-16 columns hide-for-medium-down" href="<?php bloginfo('url'); ?>/produkte"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php elseif (has_category(array('massivhaeuser','holzhaeuser','kleine-haeuser','kostenguenstig-bauen','energieeffizient-bauen'))): //conditional for links back to the categories under HÄUSER page. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog large-16 medium-16 columns hide-for-medium-down" href="<?php bloginfo('url'); ?>/haeuser"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php else: ?>
                                    <a class="goto-blog large-16 medium-16 columns hide-for-medium-down" href="<?php bloginfo('url'); ?>/blog"><?php _e('< zur übersicht', 'callwey'); ?></a>
                                <?php endif; ?>
                    		<?php else: ?>
                                <a class="goto-blog large-16 medium-16 columns hide-for-medium-down" href="<?php bloginfo('url'); ?>/blog"><?php _e('< zur Blog-übersicht', 'callwey'); ?></a>
                            <?php endif; ?>                                
                        <?php endif; ?>
                    </div>
                </div>
                <div class="entry-categorie small-9 columns">
                    <div class="row">
                        <?php if (is_singular('rezepte')): ?>
                            <ul class="post-categories">
                                <li><a href="<?php bloginfo('url') ?>/rezepte"><?php _e('Rezepte','callwey'); ?></a></li>
                            </ul>
                        <?php else: ?>
                            <?php echo $category_list; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- .entry-meta -->
        </header><!-- .entry-header -->
    <div class="medium-17 medium-push-2 small-18 columns entry-content-wrapper">
<!--
        <div class="entry-thumbnail">
            <a href="<?php //echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>"><?php //the_post_thumbnail('single'); ?></a>
        </div>
-->
        <div class="entry-content fancyarea">

            <div class="row social-buttons">
                <div class="large-12 medium-18 small-18 columns facebook">
                    <?php echo do_shortcode('[facebook_like_button]'); ?>
                </div>
                <div class="large-6 medium-18 small-18 columns"> 
                	<div class="sharing-container">
	                    <?php
	                    if ( function_exists( 'sharing_display' ) ) { 
	                        sharing_register_post_for_share_counts( get_the_ID() );
	                        sharing_display( '', true );
	                    }
	                    ?>
                	</div>
                </div>
            </div>
            <?php the_content(); ?>
        </div><!-- .entry-content -->
        <?php if (is_singular('rezepte')): ?>
        <?php else: ?>
            <footer class="entry-meta">
                <div class="entry-tags"><?php _e('Tags', 'callwey') ?> <?php echo $tag_list; ?></div>
            </footer><!-- .entry-meta -->
        <?php endif; ?>
    </div>
    <?php if (is_singular('rezepte')): ?>
    <?php else: ?>
    <section id="related-products" class="medium-17 medium-push-2 columns">
        <?php if ( $connected_post->have_posts() ) : ?>
        <?php while ( $connected_post->have_posts() ) : $connected_post->the_post(); ?>
            <?php $tttproduct = new TTTMagentorest(); ?>
            
            <div itemscope itemtype="http://schema.org/Book" class="related-product row">
                <div class="product-thumbnail medium-5 columns text-center">
                    <div class="row">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('book-home', array('itemprop' => 'image')); ?></a>
                    <?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="bookmark"]');?>       
                    </div>
                </div><!-- .product-thumbnail -->
                <div class="product-info medium-13 columns">
                    <div class="product-info-inner">
                        <h3 class="product-title" itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <div class="product-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <div class="price-vat">
                                <span class="price" itemprop="price"><?php echo round( $tttproduct->get('price'), 2 ).' €'; ?></span>
                                <small class="vat"><?php echo book_ebook_tax( $tttproduct->get('tax_class_id') ); ?></small>
                                <meta itemprop="priceCurrency" content="EUR" />
                            </div>
                        </div><!-- .product-price -->
                        <div class="shopping-butons medium-10 columns">
                        <div class="row">
                            <?php echo do_shortcode('[tttmagentorest action="add2basket"]');?>
                            <?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="button"]');?>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; wp_reset_postdata(); ?>
    </section>
    <?php endif; ?>
    <div class="row"></div>
</article><!-- #post-## -->
