<div class="item-wrap book-item" itemscope itemtype="http://schema.org/Book">
	<?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="bookmark"]');?>
    <?php
        $tttmagentorest = new TTTMagentorest();
    ?>
		<a class="book-cover-hover" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	<?php if ( is_tttdevice('desktop') ): ?>		
			<div class="book-meta hide-ie">
				<table>
					<tr>
						<td>
							<h4 class="book-autor">
								<span>von</span>
								<span><?php echo $tttmagentorest->get('author'); ?></span>
							</h4>
						</td>
					</tr>
					<tr>
						<td class="book-overview">
							<span><?php _e('blick ins buch', 'callwey'); ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<div class="book-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
								<span class="price" itemprop="price"><?php echo number_format($tttmagentorest->get('price'),2); ?> €</span>		
							</div>
						</td>
					</tr>
				</table>
			</div>	
	<?php endif; ?>
			<div class="book">	
				<div class="book-thumbnail">
					<?php the_post_thumbnail('book-home', array('itemprop' => 'image')); ?>
				</div>
			</div>
		</a>

	
	<header class="book-header">
		<h3 class="book-title" itemprop="name">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_title(); ?>
			</a>
		</h3>
	</header>
	<?php if ( is_tttdevice('tablet') || is_tttdevice('mobile') ): ?>
		<footer class="book-footer">
			<h4 class="book-autor">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo $tttmagentorest->get('author'); ?></a>
			</h4>		
			<div class="book-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="price" itemprop="price"><?php echo number_format($tttmagentorest->get('price'),2); ?> €</a>
			</div>
		</footer>
	<?php endif; ?>
	<link itemprop="bookFormat" href="http://schema.org/Paperback">
</div>
