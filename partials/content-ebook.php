<?php
/**
 * @package _s
 */
?>
<?php 
	$tttproduct = new TTTMagentorest(); 
	$cc = get_the_content();
	
	$issuu = get_post_meta( $post->ID, '_clwy_issuu', true );
	$autor_quote = get_post_meta( $post->ID, '_clwy_autor_quote', true );
	
	$totalreviews = get_post_meta( $post->ID, '_clwy_reviews_q', true );	

	for ( $i = 1; $i <= 20; $i++ ) {
        $press_review[$i] = get_post_meta( $post->ID, '_clwy_press_review_'.$i, true );
        $press_review_cite[$i] = get_post_meta( $post->ID, '_clwy_press_review_cite_'.$i, true );
    }
					


	// Find connected CPT-autor
	$connected_autor = new WP_Query( array(
		'connected_type' => 'ebook_autor',
		'connected_items' => get_queried_object(),
		'nopaging' => true,
		'tax_query' => array(
			array(
				'taxonomy' => 'typ',
				'field' => 'id',
				'terms' => 21
			)
		)
	) );

	// Find connected CPT-autor
	$connected_coautor = new WP_Query( array(
		'connected_type' => 'ebook_autor',
		'connected_items' => get_queried_object(),
		'nopaging' => true,
		'tax_query' => array(
			array(
				'taxonomy' => 'typ',
				'field' => 'id',
				'terms' => 22
			)
		)
	) );


	// Find connected CPT-autor
	$connected_fotografen = new WP_Query( array(
	  'connected_type' => 'ebook_fotografen',
	  'connected_items' => get_queried_object(),
	  'nopaging' => true,
	) );

	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Book">

	<header class="product-header">
		<div class="product-meta">		
			<div class="product-categorie text-center">
            <?php
            $obj = get_post_type_object( 'ebook' );
            echo '<a href="'.get_bloginfo('home').'/'.$obj->rewrite['slug'].'">'.$obj->label.'</a>';
            ?>
			<?php
				$book_kat = wp_get_object_terms(
	                $post->ID, 
	                array(
	                    'kategorie', 
	                    'post_tag'
	                ), 
	                array(
	                    'fields' => 'all', 
	                    'exclude' => array(19,4,9)
	                )
	            );
				if(!empty($book_kat)){
				  if(!is_wp_error( $book_kat )){
				    foreach($book_kat as $term){
				      echo '<a href="'.get_term_link($term->slug, 'kategorie').'?filter='.$obj->rewrite['slug'].'">'.$term->name.'</a>'; 
				    }
				  }
				}				
			?>
			</div>
		</div><!-- .product-categorie -->
		<h1 class="product-title text-center" itemprop="name"><?php the_title(); ?></h1>
		<div class="product-meta">
			<?php if ( $connected_autor->have_posts() ) : ?>
				<?php while ( $connected_autor->have_posts() ) : $connected_autor->the_post(); ?>
					<div class="product-author text-center">
						<span><?php _e('von','callwey') ?></span>
						<span class="author vcard">
							<a class="url fn n" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" itemprop="author"><?php the_title(); ?></a>
						</span>
					</div>
				<?php endwhile; ?>
			<?php else: ?>
				<?php if ( $tttproduct->get('author') == '' ) : ?>			
					<?php _s_posted_author(); ?>
				<?php else: ?>
					<div class="product-author text-center">
						<span><?php _e('von','callwey') ?></span>					
						<span class="author vcard"><?php echo $tttproduct->get('author'); ?></span>
					</div>
				<?php endif; ?>
			<?php endif; wp_reset_postdata(); ?>
		</div><!-- .product-meta -->
	</header><!-- .product-header -->

	<div id="product-info" class="product-info">
		<?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="bookmark"]');?>
		
		<?php if ( is_tttdevice('mobile') ): ?>

			<div class="medium-7 medium-push-1 columns">
				<div class="product-thumbnail">
					<?php the_post_thumbnail('single-book', array('itemprop' => 'image')); ?>
				</div><!-- .product-thumbnail -->
				
				<div class="product-description">
					<?php if($cc != '') : ?>
						<?php the_content(); ?>
					<?php else: ?>
						<h2><?php echo $tttproduct->get('subtitle'); ?></h2>
						<p><?php echo $tttproduct->get('description'); ?></p>
					<?php endif; ?>
								<?php //echo $tttproduct->get('press_review'); ?>
					<ul id="press-reviews" data-orbit data-options="animation:push; timer:false; bullets:false; slide_number:false; navigation_arrows:<?php if ($totalreviews == 1): ?>false<?php else: ?>true<?php endif; ?>; timer_container_class:hide;variable_height:true;">
						<?php for ($i = 1; $i <= 20; $i++): ?>
	                        <?php if ( get_post_meta( $post->ID, '_clwy_press_review_cite_'.$i, true ) ): ?>
	                            <li>
	                                <blockquote class="text-center">
	                                    <span>„</span><?php echo $press_review[$i]; ?><span>“</span>
	                                    <cite><?php echo $press_review_cite[$i]; ?></cite>
	                                </blockquote>
	                            </li>
	                        <?php else: ?>
	                            <?php break; ?>
	                        <?php endif; ?>         
	                    <?php endfor; ?>
					</ul>
				</div><!-- .product-description -->

				<div class="product-price row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					<div class="medium-12 columns price-vat">
						<span class="price" itemprop="price"><?php echo round( $tttproduct->get('price'), 2 ).'€'; ?></span>
	                    <small class="vat"><?php echo book_ebook_tax( $tttproduct->get('tax_class_id') ); ?></small>
						<meta itemprop="priceCurrency" content="EUR" />
					</div>
					<div class="medium-6 columns">
	                    <?php
	                    $p2p_Query = p2p_type( 'product_product' )->get_connected( get_the_ID() );
	                    ?>
	                    <?php if ( $p2p_Query->have_posts() ): ?>
	                        <?php while( $p2p_Query->have_posts() ): $p2p_Query->the_post(); ?>
	                            <a class="ebook-icon text-center right" href="<?php echo get_permalink(); ?>">
                                    <div class="icon-book"></div>
                                    <span><?php _e('Zum gedruckten Buch','callwey'); ?></span>
	                            </a>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
	                    <?php wp_reset_postdata(); ?>
					</div>
				</div><!-- .product-price -->
	
				<p class="product-details fancyarea">
					<?php _e('Erscheinungsdatum', 'callwey'); ?>: <?php echo custom_dateformat( $tttproduct->get('erscheinungsdatum'), 'd.m.Y' ); ?><meta itemprop="datePublished" content="<?php echo $tttproduct->get('erscheinungsdatum'); ?>"><br>
					<?php _e('Ausstattung', 'callwey'); ?>: <?php echo $tttproduct->get('ausstattung_label'); ?><br>
					<?php _e('Seitenanzahl', 'callwey'); ?>: <span itemprop="numberOfPages"><?php echo $tttproduct->get('page_numbers'); ?></span><br>
					<?php _e('ISBN', 'callwey'); ?>: <span itemprop="isbn"><?php echo $tttproduct->get('isbn_number'); ?></span>
				</p><!-- .product-details -->
	            <?php echo do_shortcode('[tttmagentorest action="add2basket"]');?>
	            <?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="button"]');?>
			</div>
					
		<?php else: /* END MOBILE. START DESKTOP & TABLET */ ?>
		
			<div class="medium-7 medium-push-1 columns">
				<div class="product-thumbnail">
					<?php the_post_thumbnail('single-book', array('itemprop' => 'image')); ?>
				</div><!-- .product-thumbnail -->
				<div class="product-price row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					<div class="medium-12 columns price-vat">
						<span class="price" itemprop="price"><?php echo round( $tttproduct->get('price'), 2 ).'€'; ?></span>
	                    <small class="vat"><?php echo book_ebook_tax( $tttproduct->get('tax_class_id') ); ?></small>
						<meta itemprop="priceCurrency" content="EUR" />
					</div>
					<div class="medium-6 columns">
	                    <?php
	                    $p2p_Query = p2p_type( 'product_product' )->get_connected( get_the_ID() );
	                    ?>
	                    <?php if ( $p2p_Query->have_posts() ): ?>
	                        <?php while( $p2p_Query->have_posts() ): $p2p_Query->the_post(); ?>
	                            <a class="ebook-icon text-center right" href="<?php echo get_permalink(); ?>">
                                    <div class="icon-book"></div>
                                    <span><?php _e('Zum gedruckten Buch','callwey'); ?></span>
	                            </a>
	                        <?php endwhile; ?>
	                    <?php endif; ?>
	                    <?php wp_reset_postdata(); ?>
					</div>
				</div><!-- .product-price -->
	
				<p class="product-details fancyarea">
					<?php _e('Erscheinungsdatum', 'callwey'); ?>: <?php echo custom_dateformat( $tttproduct->get('erscheinungsdatum'), 'd.m.Y' ); ?><meta itemprop="datePublished" content="<?php echo $tttproduct->get('erscheinungsdatum'); ?>"><br>
					<?php _e('Ausstattung', 'callwey'); ?>: <?php echo $tttproduct->get('ausstattung_label'); ?><br>
					<?php _e('Seitenanzahl', 'callwey'); ?>: <span itemprop="numberOfPages"><?php echo $tttproduct->get('page_numbers'); ?></span><br>
					<?php _e('ISBN', 'callwey'); ?>: <span itemprop="isbn"><?php echo $tttproduct->get('isbn_number'); ?></span>
				</p><!-- .product-details -->
	            <?php echo do_shortcode('[tttmagentorest action="add2basket"]');?>
	            <?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="button"]');?>
			</div>
			<div class="medium-9 columns">	
				<div class="product-description">
					<?php if($cc != '') : ?>
						<?php the_content(); ?>
					<?php else: ?>
						<h2><?php echo $tttproduct->get('subtitle'); ?></h2>
						<p><?php echo $tttproduct->get('description'); ?></p>
					<?php endif; ?>
								<?php //echo $tttproduct->get('press_review'); ?>
					<ul id="press-reviews" data-orbit data-options="animation:push; timer:false; bullets:false; slide_number:false; navigation_arrows:<?php if ($totalreviews == 1): ?>false<?php else: ?>true<?php endif; ?>; timer_container_class:hide;variable_height:true;">
						<?php for ($i = 1; $i <= 20; $i++): ?>
	                        <?php if ( get_post_meta( $post->ID, '_clwy_press_review_cite_'.$i, true ) ): ?>
	                            <li>
	                                <blockquote class="text-center">
	                                    <span>„</span><?php echo $press_review[$i]; ?><span>“</span>
	                                    <cite><?php echo $press_review_cite[$i]; ?></cite>
	                                </blockquote>
	                            </li>
	                        <?php else: ?>
	                            <?php break; ?>
	                        <?php endif; ?>         
	                    <?php endfor; ?>
					</ul>
				</div><!-- .product-description -->
			</div>
			
		<?php endif; /* END DESKTOP & TABLET  */ ?>
		
	</div><!-- .product-info -->
	<div class="row"></div>	
	<?php if ( get_post_meta( $post->ID, '_clwy_issuu', true ) ): ?>
		<section id="product-overview" class="text-center">
		<?php if ( is_tttdevice('desktop') ): ?>
			<div data-configid="<?php echo $issuu; ?>" style="width: 100%; height: 490px;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
		<?php else: ?>
			<div data-configid="<?php echo $issuu; ?>" style="width: auto; height: auto;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
		<?php endif; ?>
		<div class="row"></div>
		</section>
	<?php endif; ?>
	<section id="product-gallery" class="row fancyarea">
		<?php if( $_gallery = do_shortcode('[tttgallery template="produkt"]') ): ?>
			<?php echo $_gallery; ?>
		<?php endif; ?>
		<div class="medium-18 columns">
			<?php if ( $connected_coautor->have_posts() ) : ?>
				<ul id="book-coauthors" class="medium-block-grid-4">
				<?php while ( $connected_coautor->have_posts() ) : $connected_coautor->the_post(); ?>
					<li class="text-center">
						<?php the_post_thumbnail('book-autor-fotog'); ?>
						<h4 class="coauthor-name"><?php the_title(); ?></h4>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	<div class="row"></div>
	</section>
	<footer class="product-meta row">	
		<div id="book-author" class="medium-16 medium-centered columns">	
		<?php if ( $connected_autor->have_posts() ) : ?>
			<?php while ( $connected_autor->have_posts() ) : $connected_autor->the_post(); ?>
				<div class="medium-5 columns"><?php the_post_thumbnail('book-autor-fotog'); ?></div>
				<div class="medium-13 columns">
					<strong class="text-center"><?php _e('Autor', 'callwey'); ?></strong>
					<span class="author vcard text-center"><a class="url fn n" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" itemprop="author"><?php the_title(); ?></a></span>
					<div class="author-description"><?php the_content(); ?></div>
				</div>
			<?php endwhile; ?>
		<?php else: ?>
			<?php if ( $tttproduct->get('author') == '' ) : ?>			
				<?php _s_posted_author(); ?>
			<?php else: ?>
				<div class="medium-13 columns right">
					<strong class="text-center" itemprop="author"><?php _e('Autor', 'callwey'); ?></strong>
					<span class="author vcard text-center"><?php echo $tttproduct->get('author'); ?></span>
				</div>
			<?php endif; ?>			
		<?php endif; wp_reset_postdata(); ?>
		<div class="row"></div>
		</div>
		<div id="book-photographer" class="medium-16 medium-centered columns">		
		<?php if ( $connected_fotografen->have_posts() ) : ?>
			<?php while ( $connected_fotografen->have_posts() ) : $connected_fotografen->the_post(); ?>
				<div class="medium-5 columns"><?php the_post_thumbnail('book-autor-fotog'); ?></div>
				<div class="medium-13 columns">
					<strong class="text-center"><?php _e('Fotograf', 'callwey'); ?></strong>
					<span class="photographer vcard text-center" itemprop="author"><?php the_title(); ?></span>
					<div class="photographer-description"><?php the_content(); ?></div>
				</div>
			<?php endwhile; ?>
		<?php endif; wp_reset_postdata(); ?>		
		</div>
	<div class="row"></div>		
	</footer><!-- .product-meta -->	
		
	<?php if ( is_tttdevice('desktop') ): ?>
		<div id="product-info-footer" class="product-info medium-16 medium-centered columns">
			<?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="bookmark"]');?>	
			<div class="product-thumbnail medium-3 columns">
				<?php the_post_thumbnail('small-book', array('itemprop' => 'image')); ?>
			</div><!-- .product-thumbnail -->
			<div class="product-price medium-5 columns" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<div class="price-vat row">
					<span class="price" itemprop="price"><?php echo round( $tttproduct->get('price'), 2 ).'€'; ?></span>
	                <small class="vat"><?php echo book_ebook_tax( $tttproduct->get('tax_class_id') ); ?></small>
					<meta itemprop="priceCurrency" content="EUR" />
				</div>
			</div><!-- .product-price -->
			<div class="medium-7 columns text-center">
	            <?php echo do_shortcode('[tttmagentorest action="add2basket"]');?>
	            <?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="button"]');?>
	            <a class="view-wishlist" href="<?php bloginfo ('url'); ?>/wunschliste"><?php _e('WUNSCHLISTE ANSEHEN >','callwey'); ?></a>
			</div>
			<div class="medium-3 columns">
                <?php
	            $p2p_Query = p2p_type( 'product_product' )->get_connected( get_the_ID() );
	            ?>
	            <?php if ( $p2p_Query->have_posts() ): ?>
	                <?php while( $p2p_Query->have_posts() ): $p2p_Query->the_post(); ?>
                    <a class="ebook-icon text-center right" href="<?php the_permalink(); ?>">
                        <div class="icon-book"></div>
                        <span><?php _e('Zum gedruckten Buch','callwey'); ?></span>
                    </a>
	                <?php endwhile; ?>
	            <?php endif; ?>
			</div>
			<div class="row"></div>
		</div>
	<?php endif; ?>
	
	<div class="row"></div>
</article><!-- #post-## -->
