<?php
/**
 * The template for displaying Archive CTP-produkt TAX-kategorie.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
<div class="row">

	<?php if ( have_posts() ) : ?>

	<header class="archive-header">
		<h4 class="site-section-title text-center">
			<?php _e('Callwey', 'callwey'); ?>
		</h4>
		<h1 class="archive-title text-center">
            Bücher
            <script type="text/javascript">
                var _cwm = _cwm || [];
                _cwm.push(['menu','#menu-item-4']);
            </script>

			<?php //$term_title = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term_title->name; ?>
		</h1>
	</header><!-- .archive-header -->

	<section id="primary" class="content-area medium-17 medium-centered columns">
		<main id="main" class="site-main" role="main">
			<?php //while ( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'partials/content', 'content' ); ?>

			<?php //endwhile; ?>
	            <div id="KategorieMasonryContainer">
	            <div class="row">
                    <?php
                    
                    	$terms = get_terms( 'kategorie', array(
                    		'orderby'    => 'term_order',
                    		'hide_empty' => 0,
                    		'exclude'    => array(5,19)	
                    	) );
                    	$count = 1;	
                    	foreach( (array) $terms as $term){
                    		$meta = get_option('kategorie_thumbnail_size');
                    		if (empty($meta)) $meta = array();
                    		if (!is_array($meta)) $meta = (array) $meta;
                    		$meta = isset($meta[$term->term_id]) ? $meta[$term->term_id] : array();
                    		$images = $meta['kategorie_image'];
                    		$image_id = false;
                    		if (is_array($images)) {
                    			foreach ($images as $att) {
                    				$image_id = $att;
                    			}
                    		}
                            
                            switch ($meta['select_thumb_size']) {
                                case 'kategorie-landscape':
                                    $_css_class = 'large-11 medium-11 small-18';
                                    break;
                                case 'kategorie-portrait':
                                    $_css_class = 'large-7 medium-7 small-18';
                                    break;
                                case 'kategorie-half':
                                    $_css_class = 'large-9 medium-9 small-18';
                                    break;
                                case 'kategorie-quarter':
									if ( is_tttdevice('tablet') ) {
										$meta['select_thumb_size'] = 'kategorie-half';
	                                    $_css_class = 'medium-9';										
									} else {
	                                    $_css_class = 'medium-45 small-18';
                                    }
                                    break;
                                case 'kategorie-square':
									if ( is_tttdevice('tablet') ) {
										$meta['select_thumb_size'] = 'kategorie-half';
	                                    $_css_class = 'medium-9';										
									} else {
	                                    $_css_class = 'medium-45 small-18';
                                    }
                                    break;
                            }
							if ( !is_tttdevice('mobile') ):
	                    		echo '<div class="KategorieMasonry-brick columns '.$_css_class.'">';
                    		else:
	                    		echo '<div class="KategorieMasonry-brick columns small-18">';
                    		endif;
                                echo '<div class="kategorie-term kategorie-'.sprintf(__('%s', 'callwey'), $term->name).'">';
                                echo '<a class="kategorie-link" href="' . esc_url( get_term_link( $term, $term->taxonomy ) ) . '?filter=buecher">';                                
                                    echo '<div class="kategorie-container"><div class="kategorie-table"><div class="kategorie-cell"><h4 class="kategorie-title">';
                                    echo sprintf(__('%s', 'callwey'), $term->name);
                                    echo '</h4></div></div></div>';
							if ( !is_tttdevice('mobile') ):
                                    echo wp_get_attachment_image( $image_id, $meta['select_thumb_size'] );
							else:
                                    echo wp_get_attachment_image( $image_id, 'kategorie-landscape' );
							endif;
                                echo '</a>';
                                echo '</div>';
							echo '</div>';
							if ( is_tttdevice('desktop') ):
	                    		if ($count == 6):
		                    		echo '<div id="adspace" class="large-18 medium-18 small-18 columns">';
		                    		echo ajax_adrotate_group(4);
		                    		echo '</div>';
	                    		endif; $count++;
                    		endif;
                    	}
                    ?>
	            </div>
	            </div>	
		</main><!-- #main -->
	</section><!-- #primary -->			

	<?php else : ?>

		<?php get_template_part( 'no-results', 'archive' ); ?>

	<?php endif; ?>

</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>
