<?php
/**
 * The template for displaying the subsite CPT-magazine last posts (covers)
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('CALLWEY', 'callwey'); ?></h4>
	
	<header class="page-header">
		<h1 class="page-title text-center"><?php _e('zeitschriften', 'callwey'); ?></h1>
	</header><!-- .entry-header -->	
	
	<div id="primary" class="medium-17 medium-centered columns">	
		<main id="main" class="site-main" role="main">
			<?php
				$magazines_sites = wp_get_sites();
				array_shift($magazines_sites);
				
			?>
			<?php if ( count($magazines_sites) > 0 ) : ?>
				<ul id="magazine-list" class="medium-block-grid-3">
<?php
                foreach ($magazines_sites as $magazine):
                	if ($magazine['blog_id']==8) continue;
					if ($magazine['blog_id']==9) continue;



                    global $switched;
                    switch_to_blog( $magazine['blog_id'] );

                    $blog_details = get_blog_details($magazine['blog_id']);
                    $sitelink = $blog_details->siteurl;
                    $blogname = $blog_details->blogname;
                    $newtext = get_bloginfo('description');

    
                    $magazine_cover = array(
                        'post_type' => 'magazine',
                        'orderby' => 'date',
                        'posts_per_page' => 1,
                        // 'year' => date("Y"),
                        // 'tax_query' => array(
                        //     array(
                        //         'taxonomy' => 'year',
                        //         'terms' => '2012',
                        //         'field' => 'name',
                        //         'include_children' => false,
                        //         'operator' => 'IN'
                        //     )
                        // )
                    );
    
                    $magazine_cover_query = new WP_Query($magazine_cover);
                    // echo '<pre>';
                    // echo var_export($magazine_cover_query->query);
                    // echo "\n";
                    // echo var_export($magazine_cover_query->request);
                    // echo '</pre>';
                    $magazine_cover_query->the_post()
?>
				
					<li>
						<div class="magazine">
							<h4 class="magazine-categorie text-center">
								<a class="last-magazine-cover text-center" target="_blank" href="<?php echo $sitelink; ?>">									
									<?php echo $newtext; ?>
								</a>
							</h4>
							<div class="magazine-thumbnail">
								<div class="magazine-cell">
									<a class="last-magazine-cover text-center" target="_blank" href="<?php echo preg_replace('/([0-9]{4}\/[0-9]{2}\/[0-9]{2})/','zeitschriften',get_permalink()); ?>">
										<?php the_post_thumbnail('magazine-archive'); ?>
									</a>
								</div>
							</div>
							<h2 class="magazine-title text-center">
								<a target="_blank" href="<?php echo $sitelink; ?>">
									<?php echo $blogname ?>
								</a>
							</h2>
							<div class="magazine-description">
								<?php //the_content(); ?>
								<p><?php echo of_get_option('subsite_description'); ?></p>
							</div>
							<a class="magazine-url" target="_blank" href="<?php echo $sitelink; ?>">
								<?php echo str_replace('http://','',$sitelink) ; ?>
							</a>
						</div>
					</li>
				<?php 
				restore_current_blog();
				endforeach;
				?>
				</ul>
				
			<?php endif; wp_reset_postdata(); wp_reset_postdata(); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>
