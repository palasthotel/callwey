<?php
/**
 * The template for displaying only for wunschliste page.
 *
 * This is the template that displays ttt-magento whislist widget.
 *
 * @package _s
 */

get_header(); ?>

<div id="wishlist-page" class="row">

	<h4 class="site-section-title text-center"><?php _e('CALLWEY', 'callwey'); ?></h4>
	
	<header class="page-header">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->	
	
	<div id="primary" class="medium-17 medium-centered columns">
		<hr>	
		<main id="main" class="site-main medium-15 medium-centered columns" role="main">
			<section id="my-wishlist" class="row">
				<h5 class="section-title"><?php _e('MEINE WUNSCHLiSTE', 'callwey'); ?></h5>
		        <?php the_widget('TTTMagentorest_wishlist_widget'); ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

</div>
<?php get_footer(); ?>
