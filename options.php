<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_framework_theme'),
		'two' => __('Two', 'options_framework_theme'),
		'three' => __('Three', 'options_framework_theme'),
		'four' => __('Four', 'options_framework_theme'),
		'five' => __('Five', 'options_framework_theme')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_framework_theme'),
		'two' => __('Pancake', 'options_framework_theme'),
		'three' => __('Omelette', 'options_framework_theme'),
		'four' => __('Crepe', 'options_framework_theme'),
		'five' => __('Waffle', 'options_framework_theme')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );
		
	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}
	
	// Pull all the produkt into an array
	$options_produkt = array();
	$options_produkt_obj = get_posts('post_type=produkt&post_status=publish');
	$options_produkt[''] = 'Select a book:';
	foreach ($options_produkt_obj as $book) {
		$options_produkt[$book->ID] = $book->post_title;
	}
	
	// Pull all the update into an array
	$options_update = array();
	$options_update_obj = get_posts('post_type=update&post_status=publish');
	$options_update[''] = 'Select a book:';
	foreach ($options_update_obj as $update) {
		$options_update[$update->ID] = $update->post_title;
	}
	
	// Pull all the TAX-kategorie into an array
	$options_kategories = array();
	$options_kategories_obj = get_terms('kategorie','hide_empty=0');
	foreach ($options_kategories_obj as $kategories) {
		$options_kategories[$kategories->term_id] = $kategories->name;
	}

	// TAX-Kategorie $options_kategorie orderby
	$options_kategories_orderby = array(
		'' => __('Default', 'options_framework_theme'),
		'id' => __('ID', 'options_framework_theme'),
		'count' => __('Quantity', 'options_framework_theme'),
		'ame' => __('Name', 'options_framework_theme'),
		'slug' => __('Slug', 'options_framework_theme'),
		'term_order' => __('Drag&Drop', 'options_framework_theme')
	);

	// TAX-Kategorie $options_kategorie order
	$options_kategories_order = array(
		'' => __('Default', 'options_framework_theme'),
		'ASC' => __('Ascendent', 'options_framework_theme'),
		'DESC' => __('Descendent', 'options_framework_theme'),
	);

	// Adrotate group types selector
	$options_adrotate = array(
		'1' => __('Random', 'options_framework_theme'),
		'2' => __('External', 'options_framework_theme'),
		'3' => __('Internal', 'options_framework_theme'),
		'4' => __('Deactivate', 'options_framework_theme'),		
	);

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

		/* Applied in front-page.php shows selected TAX-kategorie term name. */
		$options[] = array(
			'name' => __('Home Settings', 'options_framework_theme'),
			'type' => 'heading');
	
		$options[] = array(
		'name' => __('Promoted Book Categories', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'promoted_kategories_title',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Choose Terms', 'options_framework_theme'),
		'desc' => __('Multicheck allowed', 'options_framework_theme'),
		'id' => 'promoted_kategories',
		//'std' => $options_kategorie,
		'type'    => 'multicheck',
		'options' => $options_kategories
		);
		
		$options[] = array(
        'name' => __('Select order by', 'options_framework_theme'),
        //'desc' => __('Small Select Box.', 'options_framework_theme'),
        'id' => 'promoted_kategories_orderby',
        'type' => 'select',
        'class' => 'mini', //mini, tiny, small
        'options' => $options_kategories_orderby
        );		
		
		$options[] = array(
        'name' => __('Select order', 'options_framework_theme'),
        //'desc' => __('Small Select Box.', 'options_framework_theme'),
        'id' => 'promoted_kategories_order',
        'type' => 'select',
        'class' => 'mini', //mini, tiny, small
        'options' => $options_kategories_order
        );		

/*
		$options[] = array(
		'name' => __('Promoted Kategories Text', 'options_framework_theme'),
		'desc' => __('Display in 2nd position between selected terms. ', 'options_framework_theme'),
		'id' => 'promoted_kategories_text',
		'type' => 'textarea'
		);
*/

        $wp_editor_settings = array(
                'wpautop' => false, // Default
                'textarea_rows' => 10,
                'tinymce' => array( 'plugins' => 'wordpress' )
        );
        
        $options[] = array(
		'name' => __('Promoted Kategories Text', 'options_framework_theme'),
        'id' => 'promoted_kategories_text',
        'type' => 'editor',
        'settings' => $wp_editor_settings
        );
                
		$options[] = array(
        'name' => __('Promoted Kategories Text position', 'options_framework_theme'),
        //'desc' => __('Small Select Box.', 'options_framework_theme'),
        'id' => 'promoted_kategories_text_count',
        'class' => 'mini',
        'type' => 'text'
        );		
		

		/* Used in related-site-content.php */
		$options[] = array(
			'name' => __('Footer boxes', 'options_framework_theme'),
			'type' => 'heading');
	
		/*
$options[] = array(
		'name' => __('Book', 'options_framework_theme'),
		'desc' => __('Choose a book', 'options_framework_theme'),
		'id' => 'promoted_book',
		'type'    => 'select',
		'options' => $options_produkt
		);
		
		$options[] = array(
		'name' => __('Update', 'options_framework_theme'),
		'desc' => __('Choose one', 'options_framework_theme'),
		'id' => 'promoted_update',
		'type'    => 'select',
		'options' => $options_update
		);	
*/	

		$options[] = array(
		'name' => __('Box #1', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_1',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_1_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_1_url',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Top text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_1_uptext',
		'class' => 'mini',		
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Bottom text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_1_bottext',
		'class' => 'mini',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('------------------------------------------------------------------------------------------------------------------------------------', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'separator',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Box #2', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_2',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_2_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_2_url',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Top text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_2_uptext',
		'class' => 'mini',		
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Bottom text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_2_bottext',
		'class' => 'mini',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('------------------------------------------------------------------------------------------------------------------------------------', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'separator',
		'type' => 'info'
		);
		
		$options[] = array(
		'name' => __('Box #3', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_3',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_3_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_3_url',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Top text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_3_uptext',
		'class' => 'mini',		
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Bottom text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_3_bottext',
		'class' => 'mini',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('------------------------------------------------------------------------------------------------------------------------------------', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'separator',
		'type' => 'info'
		);
		
		$options[] = array(
		'name' => __('Box #4', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_4',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_4_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_4_url',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Top text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_4_uptext',
		'class' => 'mini',		
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Bottom text', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_4_bottext',
		'class' => 'mini',
		'type' => 'text'
		);

		/* Used in custom-related-content.php */
		$options[] = array(
			'name' => __('Organization footer', 'options_framework_theme'),
			'type' => 'heading');
			
		$options[] = array(
		'name' => __('Box #1', 'options_framework_theme'),
		'id' => 'box_page_1',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_1_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_page_1_url',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('------------------------------------------------------------------------------------------------------------------------------------', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'separator',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Box #2', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_2',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_2_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_page_2_url',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('------------------------------------------------------------------------------------------------------------------------------------', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'separator',
		'type' => 'info'
		);
		
		$options[] = array(
		'name' => __('Box #3', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_3',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_3_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_page_3_url',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('------------------------------------------------------------------------------------------------------------------------------------', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'separator',
		'type' => 'info'
		);
		
		$options[] = array(
		'name' => __('Box #4', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_4',
		'type' => 'info'
		);

		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'box_page_4_image',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Link', 'options_framework_theme'),
		'desc' => __('Include http://', 'options_framework_theme'),
		'id' => 'box_page_4_url',
		'type' => 'text'
		);				
		
		/* Site personalizacion */
		$options[] = array(
			'name' => __('Brand Settings', 'options_framework_theme'),
			'type' => 'heading');
	
		$options[] = array(
		'name' => __('Logo Home', 'options_framework_theme'),
		'desc' => __('Only for Home!. The image must have the exact size.', 'options_framework_theme'),
		'id' => 'logo',
		'type' => 'upload'
		);
						
		$options[] = array(
		'name' => __('Favicon ICO', 'options_framework_theme'),
		'desc' => __('Use http://www.favicon.cc/ to create .ico file.', 'options_framework_theme'),
		'id' => 'favicon_ico',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Favicon PNG', 'options_framework_theme'),
		'desc' => __('Can be 16x16px or 32x32px PNG file.', 'options_framework_theme'),
		'id' => 'favicon_png',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('iPhone & iPad Icon', 'options_framework_theme'),
		'desc' => __('72x72 PNG image. Or use http://www.pic2icon.com/site_icon_for_iphone_ipad_generator.php', 'options_framework_theme'),
		'id' => 'touch_icon_iphone',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('iPhone 4', 'options_framework_theme'),
		'desc' => __('114x114 PNG image. Or use http://www.pic2icon.com/site_icon_for_iphone_ipad_generator.php', 'options_framework_theme'),
		'id' => 'touch_icon_iphone4',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('iPad 3', 'options_framework_theme'),
		'desc' => __('144x144 PNG image. Or use http://www.pic2icon.com/site_icon_for_iphone_ipad_generator.php', 'options_framework_theme'),
		'id' => 'touch_icon_ipad3',
		'type' => 'upload'
		);

		$options[] = array(
		'name' => __('Facebook Open Graph Image', 'options_framework_theme'),
		//'desc' => __('144x144 PNG image. Or use http://www.pic2icon.com/site_icon_for_iphone_ipad_generator.php', 'options_framework_theme'),
		'id' => 'og_image',
		'type' => 'upload'
		);

		
		/* Social networks links */
		$options[] = array(
			'name' => __('Social Networks', 'options_framework_theme'),
			'type' => 'heading');
	
		$options[] = array(
		'name' => __('Facebook Page', 'options_framework_theme'),
		'desc' => __('Paste full url with http://.', 'options_framework_theme'),
		'id' => 'fb_page',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('Twitter Profile', 'options_framework_theme'),
		'desc' => __('Paste full url with http://.', 'options_framework_theme'),
		'id' => 'twitter',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Youtube Profile', 'options_framework_theme'),
		'desc' => __('Paste full url with http://.', 'options_framework_theme'),
		'id' => 'youtube',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Google+ Profile', 'options_framework_theme'),
		'desc' => __('Paste full url with http://.', 'options_framework_theme'),
		'id' => 'google_plus',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Pinterest', 'options_framework_theme'),
		'desc' => __('Paste full url with http://.', 'options_framework_theme'),
		'id' => 'pinterest',
		'type' => 'text'
		);


		/* Contact details - Used in contact template */
/*
		$options[] = array(
			'name' => __('Contact Details', 'options_framework_theme'),
			'type' => 'heading');
	
		$options[] = array(
		'name' => __('Address', 'options_framework_theme'),
		'desc' => __('i.e: Very long Av. N12, Calle Orell 4, 5ºA', 'options_framework_theme'),
		'id' => 'address',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Postal Code', 'options_framework_theme'),
		'desc' => __('i.e: 007340', 'options_framework_theme'),
		'id' => 'postal_code',
		'class' => 'mini',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Location', 'options_framework_theme'),
		'desc' => __('i.e: Caprica City', 'options_framework_theme'),
		'id' => 'location',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('State/Province', 'options_framework_theme'),
		'desc' => __('i.e: Caprica', 'options_framework_theme'),
		'id' => 'state',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Phone', 'options_framework_theme'),
		'desc' => __('i.e: (+593) 2 800000', 'options_framework_theme'),
		'id' => 'phone',
		'class' => 'mini',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Fax', 'options_framework_theme'),
		'desc' => __('i.e: (+593) 2 800000', 'options_framework_theme'),
		'id' => 'fax',
		'class' => 'mini',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Email', 'options_framework_theme'),
		'desc' => __('i.e: info[at]domain[dot]com', 'options_framework_theme'),
		'id' => 'email',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Google Maps', 'options_framework_theme'),
		'desc' => __('Paste full url.', 'options_framework_theme'),
		'id' => 'google_maps',
		'type' => 'text'
		);
		
		$options[] = array(
		'name' => __('Longitud', 'options_framework_theme'),
		'desc' => __('search here: http://itouchmap.com/latlong.html', 'options_framework_theme'),
		'id' => 'longitude',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Latitud', 'options_framework_theme'),
		'desc' => __('search here: http://itouchmap.com/latlong.html', 'options_framework_theme'),
		'id' => 'latitude',
		'type' => 'text'
		);
*/
		

		/* GEOMetas details - Used in contact template */
		$options[] = array(
			'name' => __('GEO Metas', 'options_framework_theme'),
			'type' => 'heading');
	
		$options[] = array(
		'name' => __('Region', 'options_framework_theme'),
		'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'geo_region',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Place name', 'options_framework_theme'),
		'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'geo_placename',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('Position', 'options_framework_theme'),
		'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'geo_position',
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('ICBM', 'options_framework_theme'),
		'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'geo_icbm',
		'type' => 'text'
		);
		
		/* Magento product taxes */
		$options[] = array(
			'name' => __('Taxes', 'options_framework_theme'),
			'type' => 'heading');
	
		$options[] = array(
		'name' => __('Book', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'book_tax',
		'class' => 'mini',		
		'type' => 'text'
		);
		$options[] = array(
		'name' => __('eBook', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'ebook_tax',
		'class' => 'mini',		
		'type' => 'text'
		);

		/* Adrotate banners group options */
		$options[] = array(
			'name' => __('Adrotate Groups', 'options_framework_theme'),
			'type' => 'heading');

		$options[] = array(
        'name' => __('Select order', 'options_framework_theme'),
        'desc' => __('Choose an Ad Group to show in blog', 'options_framework_theme'),
        'id' => 'ajax_adrotate_group',
        'type' => 'select',
        //'class' => 'mini', //mini, tiny, small
        'options' => $options_adrotate
        );

		/* Media Subsite Widget Content */
		$options[] = array(
			'name' => __('Subsites Media Widget', 'options_framework_theme'),
			'type' => 'heading');
			
		$options[] = array(
		'name' => __('Contact 1', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'media_contact_info_1',
		'type' => 'info'
		);
			
		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		'desc' => __('IMPORTANT! max width 350px', 'options_framework_theme'),
		'id' => 'media_contact_image_1',
		'type' => 'upload'
		);

		$options[] = array(
        'name' => __('Description', 'options_framework_theme'),
        //'desc' => __('Choose an Ad Group to show in blog', 'options_framework_theme'),
        'id' => 'media_contact_text_1',
        'type' => 'editor',
        'settings' => $wp_editor_settings
        );

		$options[] = array(
		'name' => __('Contact 2', 'options_framework_theme'),
		//'desc' => __('Find it here http://www.geo-tag.de/generator/en.html', 'options_framework_theme'),
		'id' => 'media_contact_info_2',
		'type' => 'info'
		);
			
		$options[] = array(
		'name' => __('Image', 'options_framework_theme'),
		'desc' => __('IMPORTANT! max width 350px', 'options_framework_theme'),
		'id' => 'media_contact_image_2',
		'type' => 'upload'
		);

		$options[] = array(
        'name' => __('Description', 'options_framework_theme'),
        //'desc' => __('Choose an Ad Group to show in blog', 'options_framework_theme'),
        'id' => 'media_contact_text_2',
        'type' => 'editor',
        'settings' => $wp_editor_settings
        );

        $wp_editor_settings_2 = array(
                'wpautop' => false, // Default
                'textarea_rows' => 50,
                'tinymce' => array( 'plugins' => 'wordpress' )
        );

		$options[] = array(
        'name' => __('Representation', 'options_framework_theme'),
        //'desc' => __('Choose an Ad Group to show in blog', 'options_framework_theme'),
        'id' => 'media_contact_text_3',
        'type' => 'editor',
        'settings' => $wp_editor_settings_2
        );
        
        
        
		/* Newsletter */
		$options[] = array(
			'name' => __('Newsletter', 'options_framework_theme'),
			'type' => 'heading');

		$options[] = array(
		'name' => __('INP name', 'options_framework_theme'),
		'desc' => __('ie: inp_1611', 'options_framework_theme'),
		'id' => 'newsletter_inp',
		'class' => 'mini',		
		'type' => 'text'
		);

		$options[] = array(
		'name' => __('INP name', 'options_framework_theme'),
		'desc' => __('ie: 1', 'options_framework_theme'),
		'id' => 'newsletter_inp_value',
		'class' => 'mini',		
		'type' => 'text'
		);
		



	return $options;
}
