<?php
/**
 * The Template for displaying all single CPT-produkt.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<div id="primary" class="content-area medium-17 medium-centered columns">
		<main id="main" class="site-main" role="main">		

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'partials/content', 'produkt' ); ?>
			
			<div class="medium-16 medium-centered columns">
				<div class="social-buttons row">
					<div class="medium-5 medium-centered columns"><?php if ( function_exists( 'sharing_display' ) ) {
					    sharing_display( '', true );
					}?></div>
				</div>
			</div>
					
					<?php
						// If comments are open or we have at least one comment, load up the comment template
						/*
if ( comments_open() || '0' != get_comments_number() )
						comments_template();
*/
					?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


</div>
<div class="row">
	<section id="related-posts" class="medium-17 medium-centered columns">
			<?php if ( is_tttdevice('mobile') ): ?>
				<?php if(function_exists('related_posts')){
		                wp_reset_query();				
						related_posts(array(
						'post_type' => array('produkt'),
						'show_pass_post' => true,
						'past_only' => false,
						'threshold' => 1,
						'template' => 'yarpp-template-produkt.php',
						'limit' => 4,
						'order' => 'score DESC'
					), $post->ID , true);
				} ?>
			<?php else: ?>
				<?php if(function_exists('related_posts')){
		                wp_reset_query();
						related_posts(array(
						'post_type' => array('produkt'),
						'show_pass_post' => true,
						'past_only' => false,
						'threshold' => 1,
						'template' => 'yarpp-template-produkt.php',
						'limit' => 3,
						'order' => 'score DESC'
					), $post->ID , true);
				} ?>
                <script type="text/javascript">
                var _cwm = _cwm || [];
                _cwm.push(['menu','#menu-item-4']);
                </script>

			<?php endif; ?>
		<hr>
	</section>
</div>
<?php if ( !is_tttdevice('mobile') ): ?>
	<?php $args = array(
	/*
		'show_option_all'    => '',
		'order'              => 'ASC',
	*/	
		'orderby'			=> 'term_order',
		'style'				=> 'list',
		'hide_empty'		=> 0,
	    // 'child_of'           => 3,
		'exclude'            => array(5,19),
	    // 'exclude_tree'       => '',
		// 'include'            => '',
		'hierarchical'       => 0,
		'number'			=> 5,
		'title_li'			=> '',
		'show_option_none'	=> __('No categories'),
		'taxonomy'			=> 'kategorie',
	
	); 
    ?>
	<div class="row">
		<div class="medium-17 medium-centered columns">
			<ul id="product-categories" class="medium-block-grid-5 text-center">
				<?php wp_list_categories( $args ); ?>
			</ul>
		</div>
	</div>
<?php endif; ?>
<?php get_footer(); ?>
