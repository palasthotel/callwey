<?php global $post;
$tttproduct = new TTTMagentorest();  ?>
<?php
	// Find connected CPT-autor
	$connected_autor = new WP_Query( array(
		'connected_type' => 'book_autor',
		'connected_items' => get_queried_object(),
		'nopaging' => true,
		'tax_query' => array(
			array(
				'taxonomy' => 'typ',
				'field' => 'id',
				'terms' => 21
			)
		)
	) );
?>
<div id="BookMasonryContainer">
	<?php foreach( $ttt_gallery->medias as $ttt_media ): ?>

		<?php if ( $ttt_media['type'] == 'video' ): ?>
		<div class="BookMasonry-brick">
	        <div id="book-video" class="gallery-object">
	            <figure>	        
	            <?php
	                $post = get_post( $ttt_media['id'] );
	                setup_postdata( $post );
	                echo the_post_video('book-gallery-portrait');
	                wp_reset_postdata();
	            ?>
	            </figure>
	        </div>
        </div>
		<?php elseif ( $ttt_media['type'] == 'produkt' || $ttt_media['type'] == 'ebook' ): ?>
		<div class="BookMasonry-brick">
			<div id="book-author-quote" class="gallery-object">
	            <?php
	                $post = get_post( $ttt_media['id'] );
	                setup_postdata( $post );
					$autor_quote = get_post_meta( $post->ID, '_clwy_autor_quote', true ); ?>				
				<blockquote class="text-center">
					<cite>
						<?php if ( $connected_autor->have_posts() ) : ?>
							<?php while ( $connected_autor->have_posts() ) : $connected_autor->the_post(); ?>
								<?php the_title(); ?>
							<?php endwhile; ?>
						<?php else: ?>
							<?php echo $tttproduct->get('author'); ?>
						<?php endif; wp_reset_postdata(); ?>
					</cite>			
					<?php echo $autor_quote; ?>
				</blockquote>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
        <?php else: ?>
		<div class="BookMasonry-brick">        
			<div class="gallery-object">
	            <?php
	                $_size = get_post_meta( $ttt_media['id'], 'produtk_format', true );
	                if ($_size != 'book-gallery-portrait' && $_size != 'book-gallery-landscape' )
	                    $_size = 'book-gallery-landscape';
	            ?>
	            <?php $_attachement_full = wp_get_attachment_image_src( $ttt_media['id'], ttt_device_imagesizes_alias('fullscreen') ); ?>
	            <?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], $_size ); ?>
	            <figure>
					<a href="<?php echo $_attachement_full[0]; ?>" rel="gallery-<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>" data-author="<?php echo htmlentities(get_post_meta( $ttt_media['id'], 'photo_author', true ),ENT_COMPAT,'UTF-8'); ?>">
			             <?php echo ttt_gallery_unveil_tag(sprintf('<img class="book-portrait-image" src="%s" alt="%s" width="584" height="800">',$_attachement[0], $ttt_media['title'] )); ?>
					</a>
					<figcaption>
						<?php echo $ttt_media['caption']; ?>
					</figcaption>
				</figure>
			</div>
		</div>
        <?php endif; ?>

	<?php endforeach; ?>
</div>

