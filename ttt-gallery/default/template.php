<?php foreach( $ttt_gallery->medias as $ttt_media ): ?>
    <?php
    switch( mb_strtolower($callweysize) ) {
        case 'big':     $_size = 'single'; break;
        case 'medium':  $_size = 'book-gallery-portrait'; break;
        default:        $_size = 'book-autor-fotog';
    }
    $_size = get_post_meta( $ttt_media['id'], 'post_format', true );
    if ( $_size == 'single-landscape' ) $_size = 'single-landscape';
    elseif ( $_size == 'single-portrait' ) $_size = 'single-portrait';
    elseif ( $_size == 'single-portrait-double' ) $_size = 'single-portrait-double';        
    else $_size = 'single-landscape';
    ?>
	<?php $_link = wp_get_attachment_image_src( $ttt_media['id'], ttt_device_imagesizes_alias('fullscreen') ); ?>
	<?php if ( $_size == 'single-landscape' ): ?>
		<?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], $_size ); ?>	
		<figure>
		    <a class="image-thumbnail" href="<?php echo $_link[0]; ?>" rel="gallery-<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>" data-author="<?php echo htmlentities(get_post_meta( $ttt_media['id'], 'photo_author', true ),ENT_COMPAT,'UTF-8'); ?>">
		    	<?php echo ttt_gallery_unveil_tag( sprintf('<img class="gallery-object" src="%s" alt="%s" >',$_attachement[0],$ttt_media['title']) ); ?>
		    </a>
		    <figcaption class="text-center">
				<?php echo $ttt_media['caption']; ?>
				<?php //echo $ttt_media['description']; ?>
		    </figcaption>
		</figure>
	<?php elseif ( $_size == 'single-portrait' ): ?>
		<?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], $_size ); ?>	
		<figure class="text-center">
		    <a class="image-thumbnail" href="<?php echo $_link[0]; ?>" rel="gallery-<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>" data-author="<?php echo htmlentities(get_post_meta( $ttt_media['id'], 'photo_author', true ),ENT_COMPAT,'UTF-8'); ?>">
		    	<?php echo ttt_gallery_unveil_tag( sprintf('<img class="gallery-object" src="%s" alt="%s" >',$_attachement[0],$ttt_media['title']) ); ?>
			    <!-- <p class="title"><?php echo $ttt_media['title']; ?></p> -->
		    </a>
		    <figcaption class="text-center">
				<?php echo $ttt_media['caption']; ?>
		    </figcaption>
		</figure>
	<?php elseif ( $_size == 'single-portrait-double' ): ?>
		<?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], 'single-portrait' ); ?>	
		<figure class="medium-9 columns portrait-double">
		    <a class="image-thumbnail" href="<?php echo $_link[0]; ?>" rel="gallery-<?php the_ID(); ?>" title="<?php the_title_attribute(); ?>" data-author="<?php echo htmlentities(get_post_meta( $ttt_media['id'], 'photo_author', true ),ENT_COMPAT,'UTF-8'); ?>">
		    	<?php echo ttt_gallery_unveil_tag( sprintf('<img class="gallery-object" src="%s" alt="%s" >',$_attachement[0],$ttt_media['title']) ); ?>
			    <!-- <p class="title"><?php echo $ttt_media['title']; ?></p> -->
		    </a>
		    <figcaption class="text-center">
				<?php echo $ttt_media['caption']; ?>
		    </figcaption>
		</figure>
	<?php endif; ?>	
<?php endforeach; ?>
