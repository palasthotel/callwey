<?php

wp_enqueue_style( 'fancybox',            get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/jquery.fancybox.css'                           );
wp_enqueue_style( 'fancybox-buttons',    get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/helpers/jquery.fancybox-buttons.css'           );
wp_enqueue_style( 'fancybox-thumbs',     get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/helpers/jquery.fancybox-thumbs.css'            );

wp_enqueue_script( 'mousewheel',         get_template_directory_uri() . '/ttt-gallery/default/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',         array( 'jquery' ) );

wp_enqueue_script( 'fancybox',           get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/jquery.fancybox.js',                   array( 'jquery','mousewheel' ) );
//wp_enqueue_script( 'fancybox-run',       get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/jquery.fancybox-run.js' ,              array( 'fancybox' ) );
wp_enqueue_script( 'fancybox-buttons',   get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/helpers/jquery.fancybox-buttons.js',   array( 'fancybox' ) );
wp_enqueue_script( 'fancybox-thumbs',    get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/helpers/jquery.fancybox-thumbs.js',    array( 'fancybox' ) );
wp_enqueue_script( 'fancybox-media',     get_template_directory_uri() . '/ttt-gallery/default/fancybox/source/helpers/jquery.fancybox-media.js',     array( 'fancybox' ) );

?>
