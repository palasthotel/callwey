<ul data-orbit data-options="animation:fade; timer_speed:10000; bullets:false; slide_number:false; navigation_arrows:false; timer_container_class:hide;">
	<?php foreach( $ttt_gallery->medias as $ttt_media ): ?>
	<li>
		<?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], 'slideshow-widget' ); ?>
        <?php echo ttt_gallery_unveil_tag(sprintf( '<img src="%s" alt="%s" width="273" height="170">', $_attachement[0], $ttt_media['title'] )); ?>
	</li>
	<?php endforeach; ?>
</ul>
<h2 class="gallery-title"><?php echo $ttt_gallery->description; ?></h2>
