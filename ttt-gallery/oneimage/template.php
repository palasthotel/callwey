<?php foreach( $ttt_gallery->medias as $ttt_media ): ?>
    <?php
    switch( mb_strtolower($callweysize) ) {
        case 'landscape':     $_size = 'single'; break;
        case 'portrait':  $_size = 'book-gallery-portrait'; break;
        default:        $_size = 'single';
    }
    //var_dump($_size);
    ?>
	<?php $_link = wp_get_attachment_image_src( $ttt_media['id'], ttt_device_imagesizes_alias('fullscreen') ); ?>
	<?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], $_size ); ?>
    <a data-fancybox href="<?php echo $_link[0]; ?>">
        <?php echo ttt_gallery_unveil_tag(sprintf('<img class="gallery-object" src="%s" alt="%s">',$_attachement[0], $ttt_media['title'])) ?>
    </a>
<?php endforeach; ?>
