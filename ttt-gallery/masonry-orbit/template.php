    <?php
        if ( ( $_count = count($ttt_gallery->medias) ) > 0 ) {
            $_case = (int) rand(0,$_count);
        }
        else {
            $_case = 0;
        }
        
        //echo count($ttt_gallery->medias).'*'.$_case;

        $ttt_media = $ttt_gallery->medias[ $_case - 1 ];
        if (!$ttt_media)
            $ttt_media = array_shift( $ttt_gallery->medias );
    ?>
    <?php $_attachement = wp_get_attachment_image_src( $ttt_media['id'], 'masonry-home' ); ?>
    <a class="entry-thumbnail" href="<?php the_permalink(); ?>">
        <?php echo ttt_gallery_unveil_tag( sprintf('<img src="%s" alt="%s" width="234" height="auto" itemprop="image" class="unveil-pendding">', $_attachement[0], $ttt_media['title']) ); ?>
    </a>
