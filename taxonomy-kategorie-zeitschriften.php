<?php
/**
 * The template for displaying only for TAX-kategorie -> zeitschrift CPT-produkt.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('CALLWEY', 'callwey'); ?></h4>
	
	<header class="page-header">
		<h1 class="page-title text-center"><?php _e('zeitschriften', 'callwey') ?></h1>
	</header><!-- .entry-header -->	
	
	<div id="primary" class="medium-17 medium-centered columns">	
		<main id="main" class="site-main" role="main">
				<?php
					$magazines = array(
							'post_type'	 =>	'produkt',
							'posts_per_page' => -1,
							'orderby' => 'date',
							'order' => 'ASC',
							'tax_query' => array (
						    	array (
						    		'taxonomy' => 'kategorie',
						    		'field' => 'id',
						    		'terms' => 5
						    	)
						    )						
						);
					
					$magazines_query = new WP_Query($magazines);
				?>
					<?php if ($magazines_query->have_posts()) : ?>
						<ul id="magazine-list" class="medium-block-grid-3">
						<?php while ($magazines_query->have_posts()) : $magazines_query->the_post();?>
						<?php $newlink = get_post_meta( $post->ID, '_clwy_produkt_new_link', true ); ?>
						<?php $sitelink = get_post_meta( $post->ID, '_clwy_produkt_header_site_link', true ); ?>
						<?php $newtext = get_post_meta( $post->ID, '_clwy_produkt_header_text', true ); ?>
							<li>
								<div class="magazine">
									<h4 class="magazine-categorie text-center">
										<a class="last-magazine-cover text-center" target="_blank" href="http://<?php echo $sitelink; ?>">									
											<?php echo $newtext; ?>
										</a>
									</h4>
									<div class="magazine-thumbnail">
										<div class="magazine-cell">
											<a class="last-magazine-cover text-center" target="_blank" href="http://<?php echo $newlink; ?>">
												<?php the_post_thumbnail('single-book'); ?>
											</a>
										</div>
									</div>
									<h2 class="magazine-title text-center">
										<a target="_blank" href="http://<?php echo $sitelink; ?>">
											<?php the_title(); ?>
										</a>
									</h2>
									<div class="magazine-description">
										<?php the_content(); ?>
									</div>
									<a class="magazine-url" target="_blank" href="http://<?php echo $sitelink; ?>">
										<?php echo $sitelink; ?>
									</a>
								</div>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; wp_reset_postdata();?>
		</main><!-- #main -->
	</div><!-- #primary -->

</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>
