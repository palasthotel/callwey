<?php
/**
Template Name: Presse
 *
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<header class="page-header">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->	

	<div id="primary" class="content-area">

		<?php while ( have_posts() ) : the_post(); ?>
			<main id="main" class="site-main medium-17 medium-centered columns" role="main">
			<hr>
				<?php get_template_part( 'partials/content', 'page-presse' ); ?>
			</main><!-- #main -->
		<?php if ( is_tttdevice('desktop') ): ?>						
			<div class="medium-13 columns">
				<div id="page-extra-content" class="content-area large-17 large-uncentered large-push-1 medium-17 medium-centered small-18 small-centered columns">
		<?php else: ?>
			<div class="small-17 small-centered columns">
				<div id="page-extra-content">
		<?php endif; ?>
					<div class="row">
						<?php $issuu = get_post_meta( $post->ID, '_clwy_issuu_page', true ); ?>
						<?php $cformid = get_post_meta( $post->ID, '_clwy_cform_id', true ); ?>
						<?php if ( get_post_meta( $post->ID, '_clwy_issuu_page', true ) ): ?>
							<section id="product-overview">
								<h4 class="page-section-title text-center">
								 	 <?php //_e('Buchvorschau ','callwey'); ?> <?php //echo date("Y",time()); ?>
									 <?php _e('Buchvorschau ','callwey'); ?> 2015 <!-- input of the Year manually  -->
								</h4>
								<?php if ( is_tttdevice('desktop') ): ?>
									<div data-configid="<?php echo $issuu; ?>" style="width: 100%; height: 490px;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
								<div class="row"></div>
								<?php else: ?>
									<div data-configid="<?php echo $issuu; ?>" style="width: auto; height: auto;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
								<?php endif; ?>
							</section>
						<?php endif; ?>
							<section id="data-forms">
								<h4 class="page-section-title text-center">
									<?php _e('Daten anforderung', 'callwey'); ?>
								</h4>
								<?php echo do_shortcode('[contact-form-7 id="'.$cformid.'" title="Daten anforderung"]'); ?>
							</section>
					</div>
				</div>
			</div>
		<?php endwhile; // end of the loop. ?>
			<?php get_sidebar('presse'); ?>

	</div><!-- #primary -->
</div>

<?php get_footer(); ?>
