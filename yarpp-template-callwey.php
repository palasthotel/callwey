<?php
/*
YARPP Template: Callwey
Description: Requires a theme which supports post thumbnails in a random
Author: lonchbox (Pancho Pérez)
*/ ?>
<?php if ( is_tttdevice('desktop') ): ?>
	<hr>
	<div class="medium-4 columns">
		<h3 class="section-title">
			<?php _e('Was ihnen<br>vielleicht<br>auch<br>gefällt', 'callwey') ?>
		</h3>
		<h4 class="section-subtitle"><?php _e('mehr davon<br>anzeigen', 'callwey'); ?></h4>
	</div>
	<div class="medium-14 columns">
		<div class="row">
			<ul class="medium-block-grid-3 related-posts">
<?php else: ?>
	<div class="medium-18 columns">
		<div class="row">
			<h3 class="section-title text-center hide-for-small">
				<?php _e('Was ihnen vielleicht auch gefällt', 'callwey') ?>
			</h3>
			<h3 class="section-title text-left show-for-small">
				<?php _e('Was ihnen vielleicht auch gefällt', 'callwey') ?>
			</h3>
			<h4 class="section-subtitle hide-for-small"><?php _e('mehr davon anzeigen', 'callwey'); ?></h4>
			<hr>
			<ul class="medium-block-grid-3 related-posts">
<?php endif; ?>
			<?php if (have_posts()):?>
				<?php while (have_posts()) : the_post(); ?>
					<?php if (has_post_thumbnail()):?>
					<li>
						<a class="entry-thumbnail text-center" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail('yarpp-produkt-thumb'); ?>
						</a>
						<h2 class="entry-title text-center"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					</li>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else: ?>
			<?php endif; wp_reset_query();?>
			</ul>
		</div>
	</div>