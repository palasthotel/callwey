<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<header class="page-header large-15 large-centered medium-15 medium-centered columns">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	<div class="large-17 large-centered medium-17 medium-centered columns"><hr></div>	
	<div class="large-13 medium-18 columns">
		<div id="primary" class="content-area large-17 large-uncentered large-push-1 medium-17 medium-centered small-18 small-centered columns">
			<main id="main" class="site-main row" role="main">		
	
				<?php while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part( 'partials/content', 'page' ); ?>
	
				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
	<?php get_sidebar('page'); ?>

</div>

<?php get_footer(); ?>
