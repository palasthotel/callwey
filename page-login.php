<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

<div id="login-page" class="row">

	<h4 class="site-section-title text-center"><?php _e('WILLKOMMEN BEI CALLWEY', 'callwey'); ?></h4>
	
	<h3 class="site-section text-center"><?php the_title(); ?></h3>
	
	<p class="site-section-text text-center medium-8 medium-centered columns"><?php _e('Um Callwey Produkte zu kaufen benötigen wir ihre Anmeldung.<br>Der Wunschzettel ist auch ohne Anmeldung nutzbar.
', 'callwey') ?></p>

	<div id="primary" class="content-area medium-17 medium-centered columns">
		<main id="main" class="site-main login-page row" role="main">	
            
			<?php //while ( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'content', 'page' ); ?>
			
                <?php //the_content(); ?>

			<?php //endwhile; // end of the loop. ?>
			
            <div id="login-form" class="medium-6 columns">
            	<h3 class="text-center"><?php _e('Kunden-Login', 'callwey'); ?></h3>
            	<hr>
            	<?php the_widget('TTTMagentorest_login_widget'); ?>
            </div>
			<div id="register-page" class="medium-6 columns">
            	<h3 class="text-center"><?php _e('Neukunde', 'callwey'); ?></h3>
            	<hr>
            	<p class="login-text"><?php _e('Sie haben keinen Account bei Callwey.de? Sie können Ihren nächsten Einkauf schneller und einfacher gestalten, indem Sie sich bei uns regestrieren.', 'callwey'); ?></p>
            	<a href="<?php bloginfo( 'url' ); ?>/registrieren" class="button round button-noshadow"><?php _e('Zur registrierung', 'callwey'); ?></a>
			</div>
			<div id="home-page" class="medium-6 columns">
            	<h3 class="text-center"><?php _e('Als Gast einkaufen', 'callwey'); ?></h3>
            	<hr>
            	<p class="login-text"><?php _e('Sie können jederzeit auch als Gast Artikel in unserem Shop bestellen.', 'callwey'); ?></p>
            	<a href="<?php bloginfo( 'home' ); ?>" class="button round button-noshadow"><?php _e('zum Shop', 'callwey'); ?></a>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

</div>
<?php get_footer(); ?>
