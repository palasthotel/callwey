<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */
global $blog_id;

get_header(); ?>
<div class="row">
		<h4 class="site-section-title text-center">
			<?php if ( $blog_id == 9 ) : ?>
				&nbsp;
			<?php else: ?>
				<?php _e('Callwey', 'callwey'); ?>
			<?php endif; ?>
		</h4>

	
		<?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h1 class="archive-title text-center">
					<?php if ( is_post_type_archive( 'rezepte') ) :
							_e( 'Rezepte', 'callwey' ); 
					?>
					<?php else: ?>
						<?php if ( $blog_id == 9 ) : ?>
						<?php else: ?>
							<?php _e('Blog', 'callwey'); ?> 
						<?php endif; ?>
						<?php
							if ( is_category() ) :
								single_cat_title();
			
							elseif ( is_tag() ) :
								single_tag_title();
			
							elseif ( is_author() ) :
								/* Queue the first post, that way we know
								 * what author we're dealing with (if that is the case).
								*/
								the_post();
								printf( __( 'Author: %s', 'callwey' ), '<span class="vcard">' . get_the_author() . '</span>' );
								/* Since we called the_post() above, we need to
								 * rewind the loop back to the beginning that way
								 * we can run the loop properly, in full.
								 */
								rewind_posts();
			
							elseif ( is_day() ) :
								printf( __( 'Day: %s', 'callwey' ), '<span>' . get_the_date() . '</span>' );
			
							elseif ( is_month() ) :
								printf( __( 'Month: %s', 'callwey' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
			
							elseif ( is_year() ) :
								printf( __( 'Year: %s', 'callwey' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
			
							elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
								_e( 'Asides', 'callwey' );
			
							elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
								_e( 'Images', 'callwey');
			
							elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
								_e( 'Videos', 'callwey' );
			
							elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
								_e( 'Quotes', 'callwey' );
			
							elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
								_e( 'Links', 'callwey' );
															
							elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
								_e( 'Links', 'callwey' );
			
							else :
								_e( 'Archives', 'callwey' );
			
							endif;
						?>
					<?php endif; ?>
				</h1>
				<?php
					// Show an optional term description.
					/*
		$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
		*/
				?>
			</header><!-- .page-header -->	

	<?php if ( is_tttdevice('desktop') ): ?>
	<section id="primary" class="content-area large-13 medium-13 small-18 columns">
		<main id="main" class="site-main row" role="main">
	<?php else: ?>
	<section id="primary" class="content-area medium-18 columns">
		<main id="main" class="site-main row" role="main">
	<?php endif; ?>
	
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'partials/content', 'content' ); ?>
			<?php endwhile; ?>

			<?php //_s_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</main><!-- #main -->


		<?php if ( !is_tttdevice('mobile') ): ?>
			<div class="medium-16 medium-centered small-18 columns">
				<div class="medium-17 medium-push-2 small-18 small-push-0 columns">
		<?php else: ?>
			<div class="small-18 columns">
				<div class="row">
		<?php endif; ?>
					<div class="load-more">
                        <?php
                        $_ttt_loadmore_category = get_the_category();

                        ?>
						<?php if ( is_post_type_archive( 'rezepte') ) : ?>
							<a href="#" data-tttloadmore-do="archiverezepte" data-tttloadmore-to="#main">
						<?php else: ?>
							<a href="#" data-tttloadmore-do="archiveposts" data-tttloadmore-to="#main" data-tttloadmore-args="category:<?php echo $_ttt_loadmore_category[0]->slug; ?>;">
						<?php endif; ?>
								<div class="down"><?php _e('Weitere anzeigen', 'callwey') ?></div>
							</a>
					</div>
				</div>
			</div>
			
	</section><!-- #primary -->

	<?php get_sidebar(); ?>
	
</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>
