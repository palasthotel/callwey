<div class="medium-17 medium-centered columns">
	<h3 class="footer-section-title text-left"><?php _e('mehr von callwey','callwey'); ?></h3>		
	<div id="footer-widget-mobile" class="row">
		<aside id="followus-mobile" class="mobile-widget medium-6 small-18 columns">
			<h4 class="mobile-widget-title"><?php _e('Follow Us','callwey'); ?></h4>
			<div class="medium-17 medium-centered small-17 small-centered">
				<ul class="inline-list social-networks">
					<?php if ( of_get_option('fb_page') ): ?>				
					<li>
						<a href="<?php echo of_get_option('fb_page'); ?>" target="_blank">
							<span aria-hidden="true" data-icon class="icon-facebook"></span>
							<div>Like</div>
						</a>
					</li>
					<?php endif; ?>
					<?php if ( of_get_option('twitter') ): ?>				
					<li>
						<a href="<?php echo of_get_option('twitter'); ?>" target="_blank">
							<span aria-hidden="true" data-icon class="icon-twitter"></span>
							<div>follow</div>
						</a>
					</li>
					<?php endif; ?>
				</ul>
			</div>
		</aside>
		<aside id="newsletter-mobile" class="mobile-widget medium-6 small-18 columns">
			<h4 class="mobile-widget-title"><?php _e('Newsletter','callwey'); ?></h4>
			<div class="medium-17 medium-centered small-17 small-centered">					
				<form class="custom newsletter large-9 medium-12 columns" name="ProfileForm" onsubmit="return CheckInputs(this);" action="http://news.callwey.de/u/register.php" method="get">
					<fieldset>
						<legend class="hide">Newsletter</legend>
						<label><?php _e('Abonnieren', 'callwey'); ?></label>
                            <input type="hidden" name="CID" value="128435595">
                            <input type="hidden" name="SID" value="<? echo $SID; ?>">
							<input type="hidden" name="UID" value="<? echo $UID; ?>">
							<input type="hidden" name="f" value="665">
							<input type="hidden" name="p" value="2">
							<input type="hidden" name="a" value="r">
							<input type="hidden" name="el" value="<? echo $el; ?>">
                            <input type="hidden" name="endlink" value="<?php echo get_bloginfo('url').'/newsletter/'; ?>">
							<input type="hidden" name="llid" value="<? echo $llid; ?>">
							<input type="hidden" name="c" value="<? echo $c; ?>">
							<input type="hidden" name="counted" value="<? echo $counted; ?>">
							<input type="hidden" name="RID" value="<? echo $RID; ?>">
							<input type="hidden" name="mailnow" value="<? echo $mailnow; ?>">
						<div class="medium-18 small-18 columns">
							<input type="text" class="round text-center" placeholder="<?php _e('IHRE E-MAIL-ADRESSE','callwey'); ?>" name="inp_3" value="<? echo $inp_3; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'IHRE E-MAIL-ADRESSE'">
						</div>
						
							<input type="hidden" name="<?php echo of_get_option('newsletter_inp'); ?>" value="<?php echo of_get_option('newsletter_inp_value'); ?>">
						<div class="medium-18 small-18 columns">
							<button name="newsletter-submit" type="submit" class="button button-noshadow"><?php _e('Senden','callwey'); ?></button>
						</div>
					</fieldset>
				</form>
			</div>
		</aside>
		<aside id="blog-mobile" class="mobile-widget medium-6 small-18 columns">
			<h4 class="mobile-widget-title"><?php _e('CALLWEY BLOG','callwey'); ?></h4>
			<div class="medium-17 medium-centered small-17 small-centered">
				<p><?php _e('TÄGlich neues rund ums haus','calley'); ?></p>
				<a href="<?php bloginfo('url') ?>/blog" class="button button-noshadow"><?php _e('ZUM BLOG','callwey'); ?></a>
			</div>
		</aside>
	</div>
</div>
<div class="row"></div>