<?php
/**
 * The template for displaying Author bios.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<div class="author-info">
	<div class="author-avatar medium-4 columns">
		<?php //echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentythirteen_author_bio_avatar_size', 74 ) ); ?>
		<?php echo get_simple_local_avatar(get_the_author_meta( 'user_email' ), apply_filters( '_s_author_bio_avatar_size', 182 ) ); ?>
	</div><!-- .author-avatar -->
	<div class="author-description medium-14 columns">
		<h2 class="author-title hide"><?php printf( __( 'Über %s', 'callwey' ), get_the_author() ); ?></h2>
		<p class="author-bio">
			<?php the_author_meta( 'description' ); ?>
		</p>
	</div><!-- .author-description -->
</div><!-- .author-info -->