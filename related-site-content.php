<?php if ( is_main_site() ): ?>
	<div class="row">
		<section id="related-site-content" class="medium-17 medium-centered columns">
		<?php if ( is_tttdevice('desktop') ): ?>
			<div class="large-4 medium-4 small-18 columns">
				<h3 class="section-title">
					<?php _e('ENTDECKEN<br>SIE<br>WEITERE<br>INHALTE', 'callwey') ?>
				</h3>
			</div>
			<div class="large-14 large-uncentered medium-14 medium-uncentered small-15 small-centered columns">
				<div class="row">
				<ul class="large-block-grid-4 medium-block-grid-4 small-block-grid-2 related-content text-center">
		<?php elseif ( is_tttdevice('tablet') ): ?>
			<div class="medium-18 columns">
				<div class="row">
				<ul class="medium-block-grid-4 related-content text-center">
		<?php else: ?>
			<div class="small-18 columns">
				<div class="row">
				<ul class="small-block-grid-2 related-content text-center">
		<?php endif; ?>
	                <?php foreach( array(1,2,3,4) as $box_id ): ?>
					<li>
						<aside>						
							<div class="content-image">
								<a href="<?php echo of_get_option('box_'.$box_id.'_url'); ?>">
									<?php //the_post_thumbnail('autor-widget'); ?>
									<?php $thumb_id = prefix_get_attachment_id_from_url( of_get_option('box_'.$box_id.'_image') ); ?>
									<?php $attachement = wp_get_attachment_image_src($thumb_id, 'autor-widget'); ?>
	                                <img src="<?php echo $attachement[0]; ?>">
								</a>
							</div>
							<header>
								<h4 class="content-title">
									<a href="<?php echo of_get_option('box_'.$box_id.'_url'); ?>">
										<?php echo of_get_option('box_'.$box_id.'_uptext'); ?>
									</a>
									<span></span>
								</h4>
								<h3 class="content-subtitle">
									<a href="<?php echo of_get_option('box_'.$box_id.'_url'); ?>">
										<?php echo of_get_option('box_'.$box_id.'_bottext'); ?>	
									</a>
								</h3>
							</header>
						</aside>
					</li>
	                <?php endforeach; ?>
				</ul>
				</div>
			</div>
			<div class="row"></div>
		</section>
	</div>
<?php endif; ?>