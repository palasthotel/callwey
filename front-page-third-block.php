	<div class="third-block row">
		<section id="magazines" class="large-17 large-centered medium-17 medium-centered small-16 small-centered columns">
			<div class="row">
				<?php
					$magazines_sites = wp_get_sites();
					array_shift($magazines_sites);
					
				?>
				<?php if ( is_tttdevice('desktop') ): ?>			
					<div class="large-5 medium-5 columns">
						<h2 class="section-title"><?php _e('Weitere<br>fachinformatioen<br>aus dem<br>führenden<br>verlag für<br>architektur', 'callwey'); ?></h2>
						<h3 class="section-subtitle"><?php _e('Zeitschriften', 'callwey'); ?></h3>
					</div>
					<?php
/*
						$magazines = array(
								'post_type'	 =>	'produkt',
								'posts_per_page' => -1,
								'orderby' => 'date',
								'order' => 'ASC',
								'tax_query' => array (
							    	array (
							    		'taxonomy' => 'kategorie',
							    		'field' => 'id',
							    		'terms' => 5,
							    	)
							    )						
							);
						
						$magazines_query = new WP_Query($magazines);
*/
					?>

			<?php if ( count($magazines_sites) > 0 ) : ?>			
					<div class="large-13 medium-13 columns">
						<?php //if ($magazines_query->have_posts()) : ?>
						<div class="frame" id="basic">
							<ul class="clearfix">
							<?php //while ($magazines_query->have_posts()) : $magazines_query->the_post(); ?>
							<?php //$newlink = get_post_meta( $post->ID, '_clwy_produkt_new_link', true ); ?>
							<?php
								foreach ($magazines_sites as $magazine) : 
			                	if ($magazine['blog_id']==8) continue;
								if ($magazine['blog_id']==9) continue;


			                    global $switched;
			                    switch_to_blog( $magazine['blog_id'] );
								
								$blog_details = get_blog_details($magazine['blog_id']);
								/* switch_to_blog( $magazine['blog_id'] ); */
								/* $newlink = get_post_meta( $post->ID, '_clwy_produkt_new_link', true ); */
								$sitelink = $blog_details->siteurl;
								$blogname = $blog_details->blogname;
								$newtext = get_bloginfo('description');
								
								/* wp_reset_postdata(); */
								
								$magazine_cover = array(
				                            'post_type' => 'magazine',
											'orderby' => 'date',
											'posts_per_page' => 1,
											// 'year' => date("Y"),
/*
				                            'tax_query' => array(
				                                    array(
				                                            'taxonomy' => 'year',
				                                            'field' => 'name',
				                                            'terms' => date("Y")
				                                    )
				                            )
*/
											);
										
								$magazines_query = new WP_Query($magazine_cover);
								$magazines_query->the_post()
							?>
								<li>
									<div class="magazine">
										<div class="magaince-thumb-table">
											<a target="_blank" class="magazine-thumbnail" href="<?php echo preg_replace('/([0-9]{4}\/[0-9]{2}\/[0-9]{2})/','zeitschriften',get_permalink()); ?>">
												<?php the_post_thumbnail('magazine-archive'); ?>
											</a>
										</div>
										<h3 class="magazine-title"><a target="_blank" href="<?php echo preg_replace('/([0-9]{4}\/[0-9]{2}\/[0-9]{2})/','zeitschriften',get_permalink()); ?>"><?php echo $blogname; ?></a></h3>
									</div>
								</li>
							<?php //endwhile; ?>
							<?php 
							restore_current_blog();
							endforeach;
							?>
							</ul>				
							<?php endif; wp_reset_postdata(); wp_reset_postdata(); ?>
						</div>
						<?php //endif; wp_reset_postdata(); ?>
						<div class="scrollbar">
							<div class="handle"><div class="mousearea"></div></div>
						</div>						
					</div>
				<?php elseif ( is_tttdevice('tablet') ): ?>
					<div class="medium-16 medium-centered columns">
						<h2 class="section-title text-center"><?php _e('Weitere fachinformatioen aus dem führenden verlag für architektur', 'callwey'); ?></h2>
						<h3 class="section-subtitle text-center"><?php _e('Zeitschriften', 'callwey'); ?></h3>
					</div>
					<?php
/*
						$magazines = array(
								'post_type'	 =>	'produkt',
								'posts_per_page' => -1,
								'orderby' => 'date',
								'order' => 'ASC',
								'tax_query' => array (
							    	array (
							    		'taxonomy' => 'kategorie',
							    		'field' => 'id',
							    		'terms' => 5,
							    	)
							    )						
							);
						
						$magazines_query = new WP_Query($magazines);
*/
					?>
					<div id="basic-tablet" class="medium-16 medium-centered columns">
						<?php //if ($magazines_query->have_posts()) : ?>
						<?php if ( count($magazines_sites) > 0 ) : ?>			
						<ul class="medium-block-grid-3">
							<?php //while ($magazines_query->have_posts()) : $magazines_query->the_post(); ?>
							<?php //$newlink = get_post_meta( $post->ID, '_clwy_produkt_new_link', true ); ?>
							<?php
								foreach ($magazines_sites as $magazine) : 
								
			                    global $switched;
			                    switch_to_blog( $magazine['blog_id'] );								
								
								$blog_details = get_blog_details($magazine['blog_id']);
								/* switch_to_blog( $magazine['blog_id'] ); */
								/* $newlink = get_post_meta( $post->ID, '_clwy_produkt_new_link', true ); */
								$sitelink = $blog_details->siteurl;
								$blogname = $blog_details->blogname;
								$newtext = get_bloginfo('description');
								
								wp_reset_postdata();
								
								$magazine_cover = array(
				                            'post_type' => 'magazine',
											'orderby' => 'date',
											'posts_per_page' => 1,
											// 'year' => date("Y"),
/*
				                            'tax_query' => array(
				                                    array(
				                                            'taxonomy' => 'year',
				                                            'field' => 'name',
				                                            'terms' => date("Y")
				                                    )
				                            )
*/
											);
										
								$magazines_query = new WP_Query($magazine_cover);
								$magazines_query->the_post()
							?>
							<?php //while ($magazines_query->have_posts()) : $magazines_query->the_post(); ?>
							<?php //$newlink = get_post_meta( $post->ID, '_clwy_produkt_new_link', true ); ?>
								<li>
									<div class="magazine">
										<a target="_blank" class="magazine-thumbnail" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('magazin-home'); ?></a>
										<h3 class="magazine-title"><a target="_blank" href="<?php the_permalink(); ?>"><?php echo $blogname; ?></a></h3>
									</div>
								</li>
							<?php //endwhile; ?>
							<?php 
							restore_current_blog();
							endforeach;
							?>
						</ul>
						<?php endif; wp_reset_postdata(); wp_reset_postdata(); ?>
					</div>
				<?php endif; ?>				
			</div>
		</section>
	</div><!-- .third-block.row -->