<?php
/**
 * The template for displaying only for wunschliste page.
 *
 * This is the template that displays ttt-magento whislist widget.
 *
 * @package _s
 */

get_header(); ?>

<div id="basket-page" class="row">

	<h4 class="site-section-title text-center"><?php _e('IHR', 'callwey'); ?></h4>
	
	<header class="page-header">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->	
	
	<div id="primary" class="medium-17 medium-centered columns">
		<hr>	
		<main id="main" class="" role="main">
			<section id="my-basket">
				<h5 class="section-title"><?php _e('WARENKORB', 'callwey'); ?></h5>
		        <?php the_widget('TTTMagentorest_basket_widget'); ?>
			</section>
			<section id="basket-legal">
				<h6 class="section-title text-center"><?php _e('können wir ihnen behilflich sein?', 'callwey') ?></h6>
				<?php wp_nav_menu( array( 
					'theme_location' => 'basket',
					'menu_class' => 'inline-list medium-8 small-15 small-centered columns',
					'container' => '',
					'container_class' => '',
				) ); ?>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

</div>
<?php get_footer(); ?>
