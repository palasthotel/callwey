<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _s
 */

global $blod_id;

get_header(); ?>

<div class="row">

    <h4 class="site-section-title text-center">
		<?php if ( $blog_id == 9 ) : ?>
			&nbsp;
		<?php else: ?>
	    	<a href="<?php bloginfo('url') ?>/blog"><?php _e('Callwey Blog', 'callwey'); ?></a>
	    <?php endif; ?>
    </h4>

    <?php if ( is_tttdevice('tablet') ): ?>
        <div id="primary" class="content-area medium-18 columns">
    <?php else: ?>
        <div id="primary" class="content-area large-13 medium-13 small-18 columns">
    <?php endif; ?>
    
        <main id="main" class="site-main row" role="main">        

        <?php while ( have_posts() ) : the_post(); ?>
            <?php if (is_singular('rezepte')): ?>
                <?php get_template_part( 'partials/content', 'single' ); ?>
                <div class="large-16 large-centered medium-16 medium-centered small-18 columns">
                    <div class="medium-17 medium-push-2 columns">
                        <?php //_s_post_nav(); ?>                    
                        
                        <div class="medium-16 medium-centered columns">
							<?php if ( function_exists( 'sharing_display' ) ) {
							    sharing_display( '', true );
							}?>
                        </div>
                    
                    </div>
                </div>
            <?php else: ?>
                <?php get_template_part( 'partials/content', 'single' ); ?>
            
            <div class="large-16 large-centered medium-16 medium-centered small-18 columns">
                <div class="medium-17 medium-push-2 columns">
                    <?php _s_post_nav(); ?>                    

                    <!--
                    <div class="medium-15 medium-centered columns">
						<?php if ( function_exists( 'sharing_display' ) ) {
						    //sharing_display( '', true );
                        }?>
                        <li class="share-comments">
                            <a rel="nofollow" class="share-comments sd-button share-icon no-text" href="#">
                                <div aria-hidden="true" class="icon-facebook social-icon"></div><span></span>
                            </a>
                        </li>
                    </div>
                    -->

                    <?php 
                    if (
                        is_callable(array('Facebook_Comments','comments_enabled_for_post_type'))
                        &&
                        Facebook_Comments::comments_enabled_for_post_type( get_the_ID() )
                    ):
                    ?>
					<div class="row social-comments">
		                <?php
		                    // If comments are open or we have at least one comment, load up the comment template
		                    if ( comments_open() || '0' != get_comments_number() )
		                    comments_template();
		                ?>
                    </div>
                    <?php endif; ?>

                    <br> 
            		<?php if ( $blog_id == 9 ) : ?>
                    		    <?php if (has_category('profile')): //conditional for links back to the category PROFILE filter link and no /blog link. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog footer-link" href="<?php bloginfo('url'); ?>/profile"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php elseif (has_category(array('finanzierung','gesundheit','interior-design','material'))): //conditional for links back to the categories under EXPERTEN page. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog footer-link" href="<?php bloginfo('url'); ?>/experten"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php elseif (has_category(array('wohnen-kueche','bad-wc','innenausbau','haustechnik','dach-fassade','leuchten'))): //conditional for links back to the categories under PRODUKTE page. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog footer-link" href="<?php bloginfo('url'); ?>/produkte"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php elseif (has_category(array('massivhaeuser','holzhaeuser','kleine-haeuser','kostenguenstig-bauen','energieeffizient-bauen'))): //conditional for links back to the categories under HÄUSER page. ONLY for callwey-familien childtheme ?>
                                    <a class="goto-blog footer-link" href="<?php bloginfo('url'); ?>/haeuser"><?php _e('< zur übersicht', 'callwey'); ?></a>
                    		    <?php else: ?>
                                    <a class="goto-blog footer-link" href="<?php bloginfo('url'); ?>/blog"><?php _e('< zur übersicht', 'callwey'); ?></a>
                                <?php endif; ?>
            		<?php else: ?>
                        <a class="goto-blog footer-link" href="<?php bloginfo('url') ?>/blog"><?php _e('< zur Blog-übersicht', 'callwey'); ?></a>
                    <?php endif; ?>
                </div>
            </div>    
            <?php endif; ?>        

        <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->
    </div><!-- #primary -->

    <?php get_sidebar(); ?>
</div>
<div class="row">
    <section id="related-posts" class="medium-17 medium-centered columns">
        <?php if (is_singular('rezepte')): ?>
            <?php if(function_exists('related_posts')){
                wp_reset_query();
                related_posts(array(
                    'post_type' => 'rezepte',
                    'show_pass_post' => true,
                    'past_only' => false,
                    'threshold' => 1,
                    'template' => 'yarpp-template-callwey.php',
                    'limit' => 3,
                    'order' => 'score DESC'
                ), get_the_ID() , true);
            } ?>
        <?php else: ?>
            <?php if(function_exists('related_posts')){
                wp_reset_query();
                related_posts(array(
                    'post_type' => 'post',
                    'show_pass_post' => true,
                    'past_only' => false,
                    'threshold' => 1,
                    'template' => 'yarpp-template-callwey.php',
                    'limit' => 3,
                    'order' => 'score DESC'
                ), get_the_ID() , true);
            } ?>
        <?php endif; ?>    
        <hr>
    </section>
</div>
<?php if ( $blog_id == 9 ) : ?>
<?php else: ?>
    <?php get_template_part( 'related-site-content' ); ?>
<?php endif; ?>
<?php get_footer(); ?>
