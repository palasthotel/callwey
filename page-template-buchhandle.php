<?php
/**
Template Name: Buchhandelsservice
 *
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<header class="page-header large-17 large-centered medium-15 medium-centered columns">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->
	
	<div class="large-17 large-centered medium-17 medium-centered columns"><hr></div>	
	
	<div class="large-13 medium-18 columns">
		<div id="primary" class="content-area large-17 large-uncentered large-push-1 medium-17 medium-centered small-18 small-centered columns">
			<main id="main" class="site-main row" role="main">		
	
				<?php while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part( 'partials/content', 'page' ); ?>

					<?php $issuu = get_post_meta( $post->ID, '_clwy_issuu_page_buchhandle', true ); ?>
					<?php if ( get_post_meta( $post->ID, '_clwy_issuu_page_buchhandle', true ) ): ?>
						<section id="product-overview">
							<h4 class="page-section-title text-center">
								<?php //_e('Buchvorschau','callwey'); ?> <?php //echo date("Y",time()); ?>
								<?php _e('Buchvorschau','callwey'); ?> 2015    <!-- input Year manually  -->
							</h4>
							<div data-configid="<?php echo $issuu; ?>" style="width: 838px; height: 490px;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
						</section>
					<?php endif; ?>
			
				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->
			
		</div><!-- #primary -->
	</div>

	<?php get_sidebar('buchhandel'); ?>
</div>
<div class="row">
	<div class="large-17 large-centered medium-17 medium-centered columns">	
		<?php while ( have_posts() ) : the_post(); ?>	
			<?php //$our_address = get_post_meta( $post->ID, '_clwy_buchhandle_address', true ); ?>
			<?php if ( get_post_meta( $post->ID, '_clwy_buchhandle_address', true ) ): ?>
				<section id="company-address">
					<div class="row"><h4 class="page-section-title text-center">
						<?php _e('UNSERE ANSCHRIFT','callwey'); ?>
					</h4></div>
					<div class="address">
						<?php echo wpautop( get_post_meta( get_the_ID(), '_clwy_buchhandle_address', true ) ); ?>
					</div>
					<div class="row"></div>
				</section>
			<?php endif; ?>
		<?php endwhile; // end of the loop. ?>			
		
		<section id="staff-contact">
			<?php                    
	    	$terms = get_terms( 'group', array(
	    		'hide_empty' => 0,		
	    		'include' => array(139), //IMPORTANT: check the group "Ihre ansprechpartner im verlag" taxonomy id, STAGE (137) have a different one than LIVE (139).
	    	) );
	    	foreach( (array) $terms as $term) : ?>

				<div class="row"><h4 class="page-section-title text-center"><?php _e('IHRE ANSPRECHPARTNER IM VERLAG','callwey'); //echo sprintf(__('%s', 'callwey'), $term->name); ?></h4></div>
				<?php
					$staff = array(
						'post_type'	 =>	'staff',
						'posts_per_page' => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'group',
								'field' => 'slug',
								'terms' => $term
							)
						)
					);					
					$staff_query = new WP_Query($staff);
				?>
				<?php if ($staff_query->have_posts()) : ?>
					<ul class="large-block-grid-2 medium-block-grid-2 members-area">
					<?php while ($staff_query->have_posts()) : $staff_query->the_post(); ?>
						<?php
							$position = get_post_meta( $post->ID, '_clwy_position', true );
							$email = get_post_meta( $post->ID, '_clwy_email', true );
							$fon = get_post_meta( $post->ID, '_clwy_fon', true );
							$fax = get_post_meta( $post->ID, '_clwy_fax', true );
							$cc = get_the_content();	
						?>
						<li>
							<div class="large-10 medium-10 columns member-avatar row"><?php the_post_thumbnail('staff'); ?></div>
							<div class="large-8 medium-8 columns member-details">
								<div class="member-details-table">
									<div class="member-details-cell">
										<ul class="vcard">
											<li class="fn"><?php the_title(); ?></li>
										<?php if ( get_post_meta( $post->ID, '_clwy_position', true ) ): ?>
											<li class="position"><?php echo $position; ?></li>
										<?php endif; ?>
										<?php if ( get_post_meta( $post->ID, '_clwy_email', true ) ): ?>
											<li class="email"><?php echo $email; ?></li>
										<?php endif; ?>
										<?php if ( get_post_meta( $post->ID, '_clwy_fon', true ) ): ?>
											<li class="tel"><?php echo $fon; ?></li>
										<?php endif; ?>
										<?php if ( get_post_meta( $post->ID, '_clwy_fax', true ) ): ?>
											<li class="fax"><?php echo $fax; ?></li>
										<?php endif; ?>
										</ul>
									</div>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; wp_reset_query(); ?>
							
	    	<?php endforeach; ?>
		</section>

		<section id="agents-contact">

				<div class="row"><h4 class="page-section-title text-center"><?php _e('VERTRETUNGEN','callwey'); ?></h4></div>
				<?php
					$staff = array(
						'post_type'	 =>	'agent',
						'posts_per_page' => -1,
					);					
					$staff_query = new WP_Query($staff);
				?>
				<?php if ($staff_query->have_posts()) : ?>
					<ul class="large-block-grid-3 medium-block-grid-3">
					<?php while ($staff_query->have_posts()) : $staff_query->the_post(); ?>
						<li>
							<div class="agent-description">
								<h3 class="agent-location"><?php the_title(); ?></h3>
								<?php the_content(); ?>
							</div>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; wp_reset_query(); ?>

		</section>

	</div>
</div>			


<?php get_footer(); ?>

