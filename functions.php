<?php
/**
 * _s functions and definitions
 *
 * @package _s
 */

/**
 * Active or disable ttt-optimizer for old browsers
 */
if ( !defined('TTT_OPTIMIZER_ENABLED') ) {
    if( !preg_match('/(?i)msie [5-9]/',$_SERVER['HTTP_USER_AGENT']) ) {
        define('TTT_OPTIMIZER_ENABLED',true);
    }
    else {
        define('TTT_OPTIMIZER_ENABLED',false);
    }
}

if ( !defined('TTT_OPTIMIZER_ASYNC') )
    define('TTT_OPTIMIZER_ASYNC',false);


/**
 * Force SSL for all pages at IE9
 */

function ie9_force_ssl($force_ssl, $post_id = 0, $url = '') {
    if( preg_match('/(?i)msie [5-9]/',$_SERVER['HTTP_USER_AGENT']) ) {
        $force_ssl = true;
        if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
            $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            header("Location: $redirect");
        }
    }

    return $force_ssl;
}
add_filter('force_ssl', 'ie9_force_ssl', 91, 3);


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( '_s_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _s_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on _s, use a find and replace
	 * to change 'callwey' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'callwey', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
    locate_template( '/inc/images-sizes.php',true );
	locate_template( '/ttt-gallery/default/styles.php', true );	

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Header Menu', 'callwey' ),
		'secondary' => __( 'Footer Menu', 'callwey' ),
		'basket' => __( 'Basket Footer Menu', 'callwey' ),		
	) );

	/**
	 * Enable support for Post Formats
	 */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	/*
add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
*/
}
endif; // _s_setup
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function _s_widgets_init() {
/*
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'callwey' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
*/
	require(locate_template('/inc/sidebars.php'));
}
add_action( 'widgets_init', '_s_widgets_init' );

	
/**
 * Enqueue scripts and styles
 */
function _s_scripts() {
	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	//wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	//wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( '_s-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}
}
add_action( 'wp_enqueue_scripts', '_s_scripts' );


/**
 * subscription form : newsletter
 */
function newsletter_script() {
	wp_enqueue_script('newsletter-subscription', get_stylesheet_directory_uri() . '/js/subscription-form.js','', '0.1');
}
	
add_action('wp_enqueue_scripts', 'newsletter_script');


//JETPACK removing defoult position sharing buttons
//http://wordpress.org/support/topic/share-buttons-position-above-other-plugins?replies=5
function jptweak_remove_share() {
	remove_filter( 'the_content', 'sharing_display',19 );
	remove_filter( 'the_excerpt', 'sharing_display',19 );
}
add_action( 'loop_end', 'jptweak_remove_share' );
/* To insert the sharing buttons is necessary to sue this path <?php echo sharing_display(); ?> */

// Remove Jetpack CSS styles
function remove_jetpack_styles(){
	//wp_deregister_style('AtD_style'); # After the Deadline
	//wp_deregister_style('jetpack-carousel'); # Carousel
	//wp_deregister_style('grunion.css'); # Grunion contact form
	//wp_deregister_style('the-neverending-homepage'); # Infinite Scroll
	//wp_deregister_style('infinity-twentyten'); # Infinite Scroll - Twentyten Theme
	//wp_deregister_style('infinity-twentyeleven'); # Infinite Scroll - Twentyeleven Theme
	//wp_deregister_style('infinity-twentytwelve'); # Infinite Scroll - Twentytwelve Theme
	//wp_deregister_style('noticons'); # Notes
	//wp_deregister_style('post-by-email'); # Post by Email
	//wp_deregister_style('publicize'); # Publicize
	wp_deregister_style('sharedaddy'); # Sharedaddy
	wp_deregister_style('sharing'); # Sharedaddy Sharing
	//wp_deregister_style('stats_reports_css'); # Stats
	//wp_deregister_style('jetpack-widgets'); # Widgets
}
add_action('wp_print_styles', 'remove_jetpack_styles');


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * TTT Gallery Alias
 */
require get_template_directory() . '/inc/ttt-gallery-alias.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
//require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Gallery slider for tablet and mobile
 */

function load_swipebox() {
    if ( !is_admin() &&  ( is_tttdevice('mobile') || is_tttdevice('tablet') ))
        require get_template_directory() . '/inc/swipebox.php';
}
add_action('init','load_swipebox');

/*
*Making jQuery Google API
*/
function modify_jquery() {
	if (!is_admin()) {
        wp_deregister_script('jquery');
        if( preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT']) )
            wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-1.10.2.min.js', false, '1.10.2');
        else
            wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-2.0.3.min.js', false, '2.0.3');

	}
}
add_action('init', 'modify_jquery');

/**
 * Implement WP dashboard & backend hacks.
 */
locate_template( '/inc/hacks.php',true );
//require( get_template_directory() . '/inc/hacks.php' );

/**
 * Implement foundation
 */
require( get_template_directory() . '/inc/foundation.php' );

/**
 * jquery.xdomainrequest.min.js https://github.com/MoonScript/jQuery-ajaxTransport-XDomainRequest
 */
function jquery_xdomainrequest() {
    wp_enqueue_script('jquery_xdomainrequest', get_template_directory_uri() . '/js/jquery.xdomainrequest.min.js', array('jquery') );
}	
	
add_action('wp_enqueue_scripts', 'jquery_xdomainrequest');

/**
 * masonry.js https://github.com/desandro/masonry
 */
function masonry_script() {
	//if (is_front_page() || is_single() || is_singular()) {
		//wp_enqueue_script('masonry', get_template_directory_uri() . '/js/masonry.pkg.min.js');
		wp_enqueue_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.js');
	//}
}	
	
add_action('wp_enqueue_scripts', 'masonry_script');

/**
 * jquery.ba-resize.js https://github.com/cowboy/jquery-resize
 */
function ba_resize_script() {
	if (is_front_page()) {
		wp_enqueue_script('ba_resize', get_template_directory_uri() . '/js/jquery.ba-resize.js');
	}
}	
	
add_action('wp_enqueue_scripts', 'ba_resize_script');

/**
 * jquery.transform2d.js https://github.com/louisremi/jquery.transform.js
 */
function transform2d_script() {
	if (is_front_page()) {
		wp_enqueue_script('transform2d', get_template_directory_uri() . '/js/jquery.transform2d.js');
	}
}	
	
add_action('wp_enqueue_scripts', 'transform2d_script');

/**
 * Sly Horizontal Slider http://darsa.in/sly
 */
function sly_script() {
	if (is_front_page()) {
		wp_enqueue_script('easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js');
		wp_enqueue_script('sly', get_template_directory_uri() . '/js/sly.min.js');
	}
}	
	
add_action('wp_enqueue_scripts', 'sly_script');

/**
 * Hyphenate text jquery plugin
 * https://github.com/bramstein/hypher/blob/master/dist/jquery.hypher.js
 * https://github.com/bramstein/hypher/blob/master/examples/jquery/en-us.js
 */
function hypher_script() {
	wp_enqueue_script('hypher', get_template_directory_uri() . '/js/jquery.hypher.js');
	wp_enqueue_script('hypher_lang', get_template_directory_uri() . '/js/jquery.hypher.de.js');
}	
	
add_action('wp_enqueue_scripts', 'hypher_script');

/**
 * Implement foundation
 */
//require( get_template_directory() . '/inc/3DBook.php' );

/**
 * app.js Personalization JS for the site
 */
function foundation_app_script() {
	wp_enqueue_script('app', get_template_directory_uri() . '/js/app.js',array('jquery','foundation.interchange','foundation'), '1.2');
}	
	
add_action('wp_enqueue_scripts', 'foundation_app_script');

/**
 * Implement icomoon.io icons
 */
require( get_template_directory() . '/inc/icomoon-icons.php' );

/**
 * Foundation icons
 */
require( get_template_directory() . '/inc/foundation-icons.php' );

/**
 * Editor custom shortcodes
 */
require( get_template_directory() . '/inc/shortcodes.php' );

/**
 * Custom Widgets
 */
require( get_template_directory() . '/inc/widget-social.php' );
require( get_template_directory() . '/inc/widget-rezepte.php' );
require( get_template_directory() . '/inc/widget-presse.php' );
require( get_template_directory() . '/inc/widget-toplist.php' );
require( get_template_directory() . '/inc/widget-autor.php' );
require( get_template_directory() . '/inc/widget-slideshow.php' );
require( get_template_directory() . '/inc/widget-author-tagebuch.php' );
require( get_template_directory() . '/inc/widget-towidget.php' );
require( get_template_directory() . '/inc/widget-pressebox.php' );

/**
 * 
 .css
 */
function style_min_stylesheet() {
    wp_enqueue_style( 'style.min', get_template_directory_uri() . '/css/style.min.css', '3.3.8' );
	/*
if ( is_tttdevice('mobile') ) {
	    wp_enqueue_style( 'mobile.min', get_template_directory_uri() . '/css/mobile.min.css' );
	}
*/
    //wp_enqueue_style( 'extras', get_template_directory_uri() . '/css/extras.css' );
    
    wp_enqueue_style( 'adrotate.min', get_template_directory_uri() . '/css/adrotate.min.css', '1.4.3' );    

    wp_enqueue_style( 'ie.min', get_template_directory_uri() . '/css/ies.min.css' );    
}

add_action( 'wp_enqueue_scripts', 'style_min_stylesheet' );

/**
 * Implement advanced custom post type framework
 */
/**
 * Custom Post type register framework
 */
locate_template('/inc/acpt/init.php', true);

/**
 * Initialize the metabox class
 */
add_action( 'init', 'be_initialize_cmb_meta_boxes', 9999 );
function be_initialize_cmb_meta_boxes() {
	if ( !class_exists( 'cmb_Meta_Box' ) ) {
		require_once( 'inc/metaboxes/callwey-metaboxes.php' );
		require_once( 'inc/metaboxes/init.php' );

	}
}

/* 
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */
 
if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/options/' );
	require_once dirname( __FILE__ ) . '/inc/options/options-framework.php';
}

function custom_ttt_magentorest_customhost( $host ) {
    if ( get_bloginfo('url') == 'http://www.callwey.de' || get_bloginfo('url') == 'https://www.callwey.de' )
        return $host;
    else
        return preg_replace( '/www\.callwey\-shop\.de/', 'callwey-shop.monok.org', $host );
}
//add_filter('ttt-magentorest-customhost','custom_ttt_magentorest_customhost');

/* 
 * Active the relation between produkt <-> autor
*/
function book_autor() {
	p2p_register_connection_type( array(
		'name' => 'book_autor',
		'from' => 'autor',
		'to' => 'produkt',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'Produkt',
		),
		'to_labels' => array(
			'column_title' => 'Autor',
		),
		'sortable' => 'any'
	) );
}
add_action( 'p2p_init', 'book_autor' );

/* 
 * Active the relation between ebook <-> autor
*/
function ebook_autor() {
	p2p_register_connection_type( array(
		'name' => 'ebook_autor',
		'from' => 'autor',
		'to' => 'ebook',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'ebook',
		),
		'to_labels' => array(
			'column_title' => 'Autor',
		),
		'sortable' => 'any'
	) );
}
add_action( 'p2p_init', 'ebook_autor' );

/* 
 * Active the relation between produkt <-> fotografen
*/
function book_fotografen() {
	p2p_register_connection_type( array(
		'name' => 'book_fotografen',
		'from' => 'fotografen',
		'to' => 'produkt',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'Produkt',
		),
		'to_labels' => array(
			'column_title' => 'Fotografen',
		),
	) );
}
add_action( 'p2p_init', 'book_fotografen' );

/* 
 * Active the relation between ebook <-> fotografen
*/
function ebook_fotografen() {
	p2p_register_connection_type( array(
		'name' => 'ebook_fotografen',
		'from' => 'fotografen',
		'to' => 'ebook',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'ebook',
		),
		'to_labels' => array(
			'column_title' => 'Fotografen',
		),
	) );
}
add_action( 'p2p_init', 'ebook_fotografen' );

/* 
 * Active the relation between produkt <-> video
*/
/*
function book_video() {
	p2p_register_connection_type( array(
		'name' => 'book_video',
		'from' => 'video',
		'to' => 'produkt',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'Produkt',
		),
		'to_labels' => array(
			'column_title' => 'Vimeo Video',
		),
	) );
}
add_action( 'p2p_init', 'book_video' );
*/

/* 
 * Active the relation between produkt <-> post
*/
function book_post() {
	p2p_register_connection_type( array(
		'name' => 'book_post',
		'from' => 'post',
		'to' => 'produkt',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'Produkt',
		),
		'to_labels' => array(
			'column_title' => 'Post',
		),
	) );
}
add_action( 'p2p_init', 'book_post' );

/* 
 * Active the relation between ebook <-> post
*/
function ebook_post() {
	p2p_register_connection_type( array(
		'name' => 'ebook_post',
		'from' => 'post',
		'to' => 'ebook',
		'admin_column' => 'any',
		'from_labels' => array(
			'column_title' => 'ebook',
		),
		'to_labels' => array(
			'column_title' => 'Post',
		),
	) );
}
add_action( 'p2p_init', 'ebook_post' );

/* 
 * Active the relation between Produkt <-> Produkt
*/
function product_product() {
	p2p_register_connection_type( array(
		'name' => 'product_product',
		'from' => 'produkt',
		'to' => 'ebook',
		'reciprocal' => true,
		'title' => 'eBook <-> Book'
	) );
}
add_action( 'p2p_init', 'product_product' );

//specific js to load press reviews

function add_js_press_review() {
    wp_enqueue_script( 'add_js_press_review', get_template_directory_uri() . '/js/add_js_press_review.js', array( 'jquery' ), '0.1' );
}
add_filter('admin_head', 'add_js_press_review');


//loadmore wp_query for bestseller book in home. front-page.php


function loadmore_homebooks( $page ){
    
    $products = array(
        'post_type'	 =>	array('produkt','ebook'),
        'offset' => 7,
        'posts_per_page' => 16,
        'orderby' => 'date',
        'order' => 'DESC',
        'paged' => $page,
        'tax_query' => array (
        	array (
        		'taxonomy' => 'kategorie',
        		'field' => 'id',
        		'terms' => 19
        	)
        )						
    );
    $products_query = new WP_Query($products);

        
/*
	$_first = true;
	$CrossOrder = new CrossOrder();
	$products_query = $CrossOrder->order('bestseller_home_loadmore');	
*/
    ?>
    <?php if ($products_query->have_posts()) : ?>
    	<?php while ($products_query->have_posts()) : $products_query->the_post(); ?>
    		<li>
				<?php get_template_part( 'partials/content', 'home-bestseller' ); ?>
    		</li>
    	<?php endwhile; ?>
    <?php endif; wp_reset_postdata();?>

    <script type="text/javascript">
    (function($) {  $('body').trigger('tttmagentorest:add2wishlist');  })(jQuery);
    </script>
    <?


}
add_action('ttt_loadmore_homebooks','loadmore_homebooks');


//loadmore wp_query for news-feed masonry gallery in home. front-page.php

function loadmore_homenewsfeed( $page ){

		$masonry_produkt_query = new WP_Query(
			array( 
				'post_type' => array('produkt'),
                'post_status' => array('publish'),
				'posts_per_page' => 1,
				'orderby' => 'rand',
                'ignore_sticky_posts' => 1,
				'tax_query' => array(
					'relation' => 'AND',									
					array(
						'taxonomy' => 'kategorie',
						'field' => 'id',
						'terms' => array(5,19),
						'operator' => 'NOT IN'
					)
				)									
			)
		);

		$masonry_ebook_query = new WP_Query(
			array( 
				'post_type' => array('ebook'),
                'post_status' => array('publish'),
				'posts_per_page' => 1,
				'orderby' => 'rand',
                'ignore_sticky_posts' => 1,
				'tax_query' => array(
					'relation' => 'AND',									
					array(
						'taxonomy' => 'kategorie',
						'field' => 'id',
						'terms' => array(5,19),
						'operator' => 'NOT IN'
					)
				)									
			)
		);

		$masonry_posts_query = new WP_Query(
		    array( 
                'post_type' => 'post',
                'posts_per_page' => 4,
                'orderby' => 'rand',
                'post_status' => array('publish'),
                'ignore_sticky_posts' => 1,
			)
		);
							
		$masonrytresult = new WP_Query();
		
		// start putting the contents in the new object
		$masonrytresult->posts = array_merge( $masonry_posts_query->posts, $masonry_produkt_query->posts, $masonry_ebook_query->posts );
        shuffle( $masonrytresult->posts );
		
		// here you might wanna apply some sort of sorting on $result->posts
		
		// we also need to set post count correctly so as to enable the looping
		$masonrytresult->post_count = count( $masonrytresult->posts );
		
	?>
	<?php if ($masonrytresult->have_posts()) : ?>
		<?php while ($masonrytresult->have_posts()) : $masonrytresult->the_post(); ?>
			<?php get_template_part( 'partials/content', 'home-masonrybrick' ); ?>
		<?php endwhile; ?>
	<?php endif; wp_reset_postdata();?>
	
    <?

}
add_action('ttt_loadmore_homenewsfeed','loadmore_homenewsfeed');


//loadmore wp_query for index.php blog posts. 

function loadmore_blogposts( $page ){
    
    $blogposts = array(
        'post_type'	 =>	'post',
        //'offset' => 3,
        //'posts_per_page' => get_option('posts_per_page',true),
        'post_status' => 'publish',
        'order' => 'DESC',
        'orderby' => 'date',
        'paged' => $page,
        'ignore_sticky_posts' => 1
    );
    
    $blogposts_query = new WP_Query($blogposts);
	
    ?>
    <?php if ($blogposts_query->have_posts()) : ?>
    	<?php while ($blogposts_query->have_posts()) : $blogposts_query->the_post(); ?>
				<?php get_template_part( 'partials/content', 'content' ); ?>
                <?php wp_reset_postdata();?>
    	<?php endwhile; ?>
    <?php endif;?>

    <?php //rewind_posts(); ?>
    <?php //wp_reset_query(); ?>


    <?

}
add_action('ttt_loadmore_blogposts','loadmore_blogposts');


//loadmore wp_query for archive.php posts.

function loadmore_archiveposts( $page, $args = false ){

    $archiveposts = array(
        'post_type'	 =>	'post',
        //'offset' => 3,
        // 'posts_per_page' => get_option('posts_per_page',true),
        'post_status' => 'publish',
        'order' => 'DESC',
        'orderby' => 'date',
        'paged' => $page,
        'ignore_sticky_posts' => 1,
        'category_name' => $args['category'],
    );
    
    $archiveposts_query = new WP_Query($archiveposts);
	
    ?>
    <?php if ($archiveposts_query->have_posts()) : ?>
    	<?php while ($archiveposts_query->have_posts()) : $archiveposts_query->the_post(); ?>
				<?php get_template_part( 'partials/content', 'content' ); ?>
                <?php wp_reset_postdata();?>
    	<?php endwhile; ?>
    <?php endif;?>

    <?php //rewind_posts(); ?>
    <?php //wp_reset_query(); ?>


    <?

}
add_action('ttt_loadmore_archiveposts','loadmore_archiveposts', 1, 2);

//loadmore wp_query for archive.php rezepte.

function loadmore_archiverezepte( $page, $args = false ){

    $archiverezepte = array(
        'post_type'	 =>	'rezepte',
        //'offset' => 3,
        //'posts_per_page' => 3,
        'post_status' => 'publish',
        'order' => 'DESC',
        'orderby' => 'date',
        'paged' => $page,
        'ignore_sticky_posts' => 1,
    );
    
    $archiverezepte_query = new WP_Query($archiverezepte);
	
    ?>
    <?php if ($archiverezepte_query->have_posts()) : ?>
    	<?php while ($archiverezepte_query->have_posts()) : $archiverezepte_query->the_post(); ?>
				<?php get_template_part( 'partials/content', 'content' ); ?>
                <?php wp_reset_postdata();?>
    	<?php endwhile; ?>
    <?php endif;?>

    <?php //rewind_posts(); ?>
    <?php //wp_reset_query(); ?>


    <?

}
add_action('ttt_loadmore_archiverezepte','loadmore_archiverezepte', 1, 2);


//loadmore wp_query for search.php blog posts. 

function loadmore_searchresult( $page ){
    
    $searchresult = array(
        'post_type'	 =>	array('post','produkt','ebook','rezepte'),
        //'offset' => 3,
        //'posts_per_page' => get_option('posts_per_page',true),
        'paged' => $page       
    );
    
    $searchresult_query = new WP_Query($searchresult);
	
    ?>
    <?php if ($searchresult_query->have_posts()) : ?>
    	<?php while ($searchresult_query->have_posts()) : $searchresult_query->the_post(); ?>
				<?php get_template_part( 'partials/content', 'search' ); ?>
    	<?php endwhile; ?>
    <?php endif; wp_reset_postdata();?>

    <?

}
add_action('ttt_loadmore_blogposts','loadmore_blogposts');


//loadmore wp_query for taxonomy-kategory.php CPT-produkt posts. 

function loadmore_taxonomyproducts( $page, $args = false ){
    
    if (get_current_blog_id() != 1) {
        switch_to_blog(1);
        global $wp_taxonomies;
        if(!taxonomy_exists('kategorie'))
            $wp_taxonomies['kategorie'] = 'delete';

        if (!is_numeric($args['term_id'])) {
            $args['term_id'] = 23;
        }
    }


    $offset = 0;
    if ( $page == 2 ) $offset = 7;

    $taxonomyproducts = array(
		'post_type'	 =>	$args['post_type'],
        'paged' => $page,
        'offset' => $offset,
		'posts_per_page' => 8,
		'post_status' => 'publish',
	    'tax_query' => array (
            'relation' => 'AND',
	    	array (
	    		'taxonomy' => 'kategorie',
	    		'field' => 'id',
	    		'terms' => array($args['term_id'])
	    	),
	        array (
	        	'taxonomy' => 'kategorie',
	        	'field' => 'id',
	        	'terms' => array( 19, 5 ),
	        	'operator' => 'NOT IN'
	        )
	    )
    );
    
    $taxonomyproducts_query = new WP_Query($taxonomyproducts);
	
    ?>
    <?php if ($taxonomyproducts_query->have_posts()) : ?>
    	<?php while ($taxonomyproducts_query->have_posts()) : $taxonomyproducts_query->the_post(); ?>
                <li>
				<?php get_template_part( 'partials/content', 'home-bestseller' ); ?>
                <?php wp_reset_postdata();?>
                </li>
    	<?php endwhile; ?>
    <?php endif;?>

    <?php //rewind_posts(); ?>
    <?php //wp_reset_query(); ?>


    <?

}

add_action('ttt_loadmore_taxonomyproducts','loadmore_taxonomyproducts',1, 2);

function custom_dateformat( $date, $format ) {
    $parsed = date_parse_from_format( 'Y-m-d h:i:s', $date );
    $new = mktime(
        $parsed['hour'], 
        $parsed['minute'], 
        $parsed['second'], 
        $parsed['month'], 
        $parsed['day'], 
        $parsed['year']
    );
    return date( $format, $new );
    
}

/*Meta fields for image and thumbnail size for TAX-kategorie*/
require( get_template_directory() . '/inc/kategorie-metas.php' );

/*Ebook & Book tax variable*/
function book_ebook_tax( $tax_class_id = false ) {
    if ( $tax_class_id == 2 ) {
        return 'inkl. '.of_get_option('book_tax').'% MwSt.';
    }
    elseif ( $tax_class_id == 4 ) {
        return 'inkl. '.of_get_option('ebook_tax').'% MwSt.';
    }
}
function book_ebook_tax_float( $tax_class_id = false ) {
    if ( $tax_class_id == 2 ) {
        return (float) of_get_option('book_tax');
    }
    elseif ( $tax_class_id == 4 ) {
        return (float) of_get_option('ebook_tax');
    }
}

function prefix_get_attachment_id_from_url( $attachment_url = '' ) {

    global $wpdb;
    $attachment_id = false;

    // If there is no url, return.
    if ( '' == $attachment_url )
        return;

    // Get the upload directory paths
    $upload_dir_paths = wp_upload_dir();
    $ssl = str_replace('http://','https://',$upload_dir_paths['baseurl']);
        
    // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
    if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) || false !== strpos($attachment_url,$ssl) ) {

        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
        $attachment_url = str_replace( $ssl . '/', '', $attachment_url );

        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

    }

    return $attachment_id;
}

/*Add class to every cform <form>*/
add_filter( 'wpcf7_form_class_attr', 'foundation_form_class_attr' );

function foundation_form_class_attr( $class ) {
	$class .= ' custom';
	return $class;
}

if (class_exists('Unveil_Images'))
    $unveil = new Unveil_Images();
else
    $unveil = false;


function ttt_gallery_unveil_tag( $string ) {
    if ( $unveil ==  false ) return $string;
    if ( isset($_REQUEST['action']) ) return $string;
    
    return $unveil->add_dummy_image( $string );
}


function ttt_device_imagesizes_alias( $size ) {
    $alias = array(
        'fullscreen' => array( 'mobile' => 'fullscreen-mobile', 'tablet' => 'fullscreen' )
    );

    if ( !isset($alias[$size]) ) return $size;

    if ( is_tttdevice('desktop') ) return $size;
    if ( is_tttdevice('mobile') ) return $size[$size]['mobile'];
    if ( is_tttdevice('tablet') ) return $size[$size]['tablet'];
}


function custom_sharing_js_dialog( $name, $params, $opts) {
    ?>
    <script type="text/javascript">
        var _sharing_dialog = _sharing_dialog || [];
        _sharing_dialog.push([ '<?php echo $name; ?>','click',function(event) { event.preventDefault(); window.open( jQuery(this).attr( 'href' ), 'wpcom<?php echo $name; ?>', '<?php echo $opts; ?>' ); } ]);
    </script>
    <?php
}
add_filter('sharing_js_dialog','custom_sharing_js_dialog');

function custom_create_post_type_magazine() {
    // NEED TO ADD THE CHILD TAXONOMY 'YEAR' TO USE IT IN THE WP_QUERY
    $args_magazines = array(
        'supports' => array( 'title', 'thumbnail', 'editor' ),
        'hierarchical' => false,
        'rewrite' => array( 'with_front' => false )
        //'rewrite' => array( 'with_front' => false, 'slug' => 'magazine-int' )  
    );
    $magazines = acpt_post_type('magazine','magazines', false,  $args_magazines )->icon('pulse');
    acpt_tax('year', 'years', $magazines, true, false);
}
// add_action('init','custom_create_post_type_magazine');


/**
 * Remove from slug the "/blog/" before the taxonomy post_type produkt and ebook
 */
function callwey_rewrite_rules( $rules ) {

    foreach($rules as $key => $value) {
        if (strpos($key,'blog/buecherangebot/') !== FALSE) {
            $newkey = str_replace('blog/','',$key);
            $rules[ $newkey ]  = $value;
        }

    }

    return $rules;
}
add_filter( 'rewrite_rules_array','callwey_rewrite_rules' );

function callwey_remove_blog_from_product_link( $permalink, $post, $leavename ){

    if (strpos($permalink, '/buecherangebot/') > 0)
        return str_replace('/blog/', '/', $permalink);

    return $permalink;
}
add_filter( 'term_link', 'callwey_remove_blog_from_product_link', 1, 3 );
