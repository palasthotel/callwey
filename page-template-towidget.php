<?php
/**
Template Name: To Widget
 *
 * @package _s
 */

get_header(); ?>

<div class="row">
	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<header class="page-header row">
		<h1 class="page-title text-center"><?php the_title(); ?></h1>
		<div class="medium-17 medium-centered columns">
				<?php get_template_part( 'partials/content', 'autor-bio' ); ?>
		</div>		
	</header><!-- .entry-header -->
	
	<div class="large-17 large-centered medium-17 medium-centered columns"><hr></div>	
	
	<div class="large-13 medium-18 columns">
		<div id="primary" class="content-area large-17 large-uncentered large-push-1 medium-17 medium-centered small-18 small-centered columns">
			<main id="main" class="site-main row" role="main">		
	
				<?php while ( have_posts() ) : the_post(); ?>
				
					<?php get_template_part( 'partials/content', 'page' ); ?>
	
				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>


	<?php get_sidebar(); ?>
	
</div>
<div class="row">
	<section id="related-posts" class="medium-17 medium-centered columns">
		<hr>
			<?php if(function_exists('related_posts')){
	                wp_reset_query();			
					related_posts(array(
				  'post_type' => 'post',
				  'show_pass_post' => true,
				  'past_only' => false,
				  'threshold' => 1,
				  'template' => 'yarpp-template-callwey.php',
				  'limit' => 3,
				  'order' => 'score DESC'
				), $post->ID , true);
			} ?>		
		<hr>
	</section>
</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>