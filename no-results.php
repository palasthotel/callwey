<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */
?>

<section class="no-results not-found">
		<h2 class="section-title medium-17 medium-centered columns text-center"><?php _e( 'In Kürze finden Sie hier Bücher zu diesem Thema', 'callwey' ); ?></h2>
</section><!-- .no-results -->
