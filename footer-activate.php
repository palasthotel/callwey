<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _s
 */
?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="nav-wrapper site-info">
		<?php if ( is_tttdevice('mobile') ): ?>
			<div class="nav-inner-wrapper">
				<nav id="footer-navigation" class="site-navigation row" role="navigation">
					<section class="top-bar-section">
					<?php wp_nav_menu( array( 
						'theme_location' => 'secondary',
						'menu_class' => 'left',
						'container' => '',
						'container_class' => '',
					) ); ?>
					</section>
				</nav><!-- #site-navigation -->
			</div>
		<?php elseif ( is_tttdevice('tablet') ): ?>		
			<div class="nav-inner-wrapper">
				<nav id="footer-navigation" class="site-navigation top-bar row" role="navigation">
					<section class="top-bar-section">
					<?php wp_nav_menu( array( 
						'theme_location' => 'secondary',
						'menu_class' => 'left',
						'container' => '',
						'container_class' => '',
					) ); ?>
					</section>
				</nav><!-- #site-navigation -->
			</div>
		<?php else: ?>
			<div class="nav-inner-wrapper large-17 large-centered medium-17 medium-centered columns">
				<nav id="footer-navigation" class="site-navigation top-bar row" role="navigation">
					<section class="top-bar-section">
					<?php wp_nav_menu( array( 
						'theme_location' => 'secondary',
						'menu_class' => 'left',
						'container' => '',
						'container_class' => '',
					) ); ?>
					</section>
				</nav><!-- #site-navigation -->
			</div>
		<?php endif; ?>
			<?php do_action( '_s_credits' ); ?>			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>