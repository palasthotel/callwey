	<div class="first-block row">
	<?php if ( is_tttdevice('tablet') ): ?>	
		<div id="primary" class="content-area medium-18 columns">
			<main id="main" class="site-main row" role="main">
			
				<section id="bestseller" class="medium-15 medium-centered columns">

				<h2 class="section-title text-center row"><?php _e('Bestseller und Neuheiten','callwey'); ?></h2>
				<h3 class="section-subtitle text-center"><?php _e('bücher', 'callwey'); ?></h3>
				
	<?php else: ?>
		<div id="primary" class="content-area large-13 large-uncetered medium-13 medium-uncentered small-16 small-centered columns">
			<main id="main" class="site-main row" role="main">
			
				<section id="bestseller" class="large-16 large-centered medium-16 medium-centered small-18 columns">
	<?php endif; ?>


				
	                <style>
	                #homeproducts .hidden {
	                    display: none;
	
	                }
	                </style>
					<?php
	/*
						$products = array(
								'post_type'	 =>	'produkt',
								'posts_per_page' => 7,
								'orderby' => 'date',
								'order' => 'DESC',
								'tax_query' => array (
							    	array (
							    		'taxonomy' => 'kategorie',
							    		'field' => 'id',
							    		'terms' => 19
							    	)
							    )						
							);
						
						$products_query = new WP_Query($products);
	*/
					?>
					<?php
						$_count = 0;
						$CrossOrder = new CrossOrder();
						$products_query = $CrossOrder->order('bestseller_home');
					?>
					<div class="row">
						<?php if ($products_query->have_posts()) : ?>
							<ul id="homeproducts" class="large-block-grid-4 medium-block-grid-3 small-block-grid-2">
								<?php if ( is_tttdevice('desktop') ): ?>						
									<li>
										<h2 class="section-title text-left"><?php _e('Bestseller und Neuheiten','callwey'); ?></h2>
										<h3 class="section-subtitle"><?php _e('bücher', 'callwey'); ?></h3>
									</li>
								<?php endif; ?>
							<?php while ($products_query->have_posts()) : $products_query->the_post(); $_count++; ?>
								<?php if ( is_tttdevice('desktop') ): ?>
									<li class="<?php echo ( $_count <= 7 ? '':'hidden' ); ?>">
								<?php elseif ( is_tttdevice('tablet') ): ?>
									<li class="<?php echo ( $_count <= 6 ? '':'hidden' ); ?>"> 								<?php endif; ?>
										<?php get_template_part( 'partials/content', 'home-bestseller' ); ?>
									</li>
							<?php endwhile; ?>
							</ul>
							<div class="load-more">
	                            <!-- <a href="#" data-tttloadmore-do="homebooks" data-tttloadmore-to="#homeproducts">-->
	                            <a href="#">
	                                <div class="down"><?php _e('Weitere Besteller anzeigen', 'callwey') ?></div>
	                                <div class="up"><?php _e('Weitere Bestseller wieder ausblenden', 'callwey') ?></div>
	                            </a>
	                        </div>
						<?php endif; wp_reset_postdata();?>
					</div>
				</section>

			<?php if ( is_tttdevice('tablet') ): ?>
				<div id="secondary" class="widget-area medium-16 medium-centered columns" role="complementary">
					<?php do_action( 'before_sidebar' ); ?>
					<div class="medium-18 columns">
						<?php if ( ! dynamic_sidebar( 'sidebar-tablet' ) ) : ?>
						<?php endif; // end sidebar widget area ?>
					</div>
					<div class="row"></div>
				</div>
			<?php endif; ?>
			
			<?php if ( is_tttdevice('desktop') ): ?>				
				<div id="adspace" class="large-16 large-centered medium-16 medium-centered small-18 columns">
					<div class="row">
						<?php echo ajax_adrotate_group(1); ?>
					</div>
				</div><!-- #adspace -->
			<?php elseif ( is_tttdevice('tablet') ): ?>
				<div id="adspace" class="medium-14 medium-centered columns">
					<div class="row">
						<?php echo ajax_adrotate_group(1); ?>
					</div>
				</div><!-- #adspace -->
			<?php endif; ?>
				
				<section id="promoted-product-category" class="large-16 large-centered medium-16 medium-centered small-18 columns">
					<div class="row">
						<?php if ( is_tttdevice('desktop') ): ?>
							<div class="medium-5 columns"><h2 class="section-title text-left"><?php _e('Wichtige buch- kategorien','callwey'); ?></h2></div>
						<?php else: ?>
							<div class="medium-17 medium-centered columns"><h2 class="section-title text-center row"><?php _e('Wichtige buch- kategorien','callwey'); ?></h2></div>
						<?php endif; ?>
	                    <?php
							$promo_kategories = of_get_option('promoted_kategories');
							$promo_kategories_orderby = of_get_option('promoted_kategories_orderby');
							$promo_kategories_order = of_get_option('promoted_kategories_order');
							$promo_kategories_text_count = of_get_option('promoted_kategories_text_count');
							$homeParam;
							foreach($promo_kategories as $key => $value){
								if($value == 1){
									$homeParam[] = $key;
								}
							}                    
	                    	$terms = get_terms( 'kategorie', array(
	                    		'orderby'    => $promo_kategories_orderby, /*id, count, name, slug, none, term_order */
								'order'         => $promo_kategories_order,                    		
	                    		'hide_empty' => 0,
	                    		'include'    => $homeParam	
	                    	) );
	                    	$count = 1;	
	                    	foreach( (array) $terms as $term){
	/*
	                    		$meta = get_option('kategorie_thumbnail_size');
	                    		if (empty($meta)) $meta = array();
	                    		if (!is_array($meta)) $meta = (array) $meta;
	                    		$meta = isset($meta[$term->term_id]) ? $meta[$term->term_id] : array();
	                    		$images = $meta['kategorie_image'];
	                    		$image_id = false;
	                    		if (is_array($images)) {
	                    			foreach ($images as $att) {
	                    				$image_id = $att;
	                    			}
	                    		}
	*/
								$taxperm = get_term_link( $term, $term->taxonomy );
								if (!is_wp_error($taxperm)) {
									echo '<h4 class="kategorie-name"><a class="kategorie-link" href="' . esc_url($taxperm) . '?filter=buecher">';
									echo sprintf(__('%s', 'callwey'), $term->name);
									echo '</a></h4>';
								}
								
	                    		if ($count == $promo_kategories_text_count):
									if ( is_tttdevice('desktop') || is_tttdevice('tablet') ):		                    		
			                    		echo of_get_option('promoted_kategories_text');
			                    	endif;
	                    		endif; $count++;
	                    	}
	                    ?>
						<a class="view-all hide-for-medium-down" href="<?php bloginfo('url'); ?>/buecher"><?php _e('Zur<br>Übersicht', 'callwey'); ?></a>
					</div>
				</section>
				
			</main>
		</div><!-- #primary -->

	<?php if ( is_tttdevice('desktop') ): ?>
		<div id="secondary" class="widget-area large-4 large-pull-1 medium-4 medium-pull-1 hide-for-medium-down columns" role="complementary">
			<?php do_action( 'before_sidebar' ); ?>
			<?php if ( ! dynamic_sidebar( 'sidebar-general' ) ) : ?>
			<?php endif; // end sidebar widget area ?>			
			<?php if ( ! dynamic_sidebar( 'sidebar-home-right' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		</div><!-- #secondary -->
	<?php endif; ?>

	</div><!-- .first-block.row -->
