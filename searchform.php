<?php
/**
 * The template for displaying search forms in _s
 *
 * @package _s
 */
?>
<form role="search" method="get" class="search-form custom" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<span class="screen-reader-text"><?php _ex( 'Bücher- und zeitschriften', 'label', 'callwey' ); ?></span>
	<?php if ( !is_tttdevice('mobile') ): ?>
		<span aria-hidden="true" class="icon-search"></span>
	<?php endif; ?>
	<input type="search" class="search-field round" placeholder="<?php echo esc_attr_x( 'SUCHE', 'placeholder', 'callwey' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'SUCHE', 'label', 'callwey' );?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Suche'">

	<input type="submit" class="search-submit hide" value="<?php echo esc_attr_x( 'SUCHE', 'submit button', 'callwey' ); ?>">
</form>
