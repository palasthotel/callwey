
var paramsMasonry = {};
var _cwm = _cwm || [];
var _sharing_dialog = _sharing_dialog || [];
var menuInterval = 0;
var menuIntervalChange = false;

$(document).ready(function() {

    /* Click to comment */
    $('a.comment-bottom').on('click',function(event) {
        event.preventDefault();
        var vtop = $('#comments').offset().top;
        $("html, body").animate({
            scrollTop: vtop
        },500);
    });
	
	/* WORDPRESS NAV-BAR SUPPORT ------------- */
	/* Adds support for the top-bar with flyouts in WordPress */	
	
	$('.top-bar-section li').has('ul').addClass("has-dropdown");
	$('.top-bar-section li .page-submenu').addClass("dropdown");

	/* WORDPRESS add active categorie to last item of the breadcrumbs ------------- */

	//$('.breadcrumbs li:last-child').addClass("current");	

	/* Cform7 plugin foudnation classes ------------- */

	//$('.form-submit input#submit').addClass("button small");	

	//$('.wpcf7-form').addClass("custom");	
	
	/* WORDPRESS add custom styles for categories widget ------------- */

	$('.widget_categories ul').addClass("side-nav");	
	
	/* JETPACK insert font social icons into sharebuttons */
	$('.sharedaddy .sd-block .sd-content ul').addClass("inline-list");	
	$('.sharedaddy .share-facebook a span').before('<div aria-hidden="true" class="icon-facebook social-icon"></div>');
	$('.sharedaddy .share-twitter a span').before('<div aria-hidden="true" class="icon-twitter social-icon"></div>');	
	$('.sharedaddy .share-pinterest a span').before('<div class="fi-social-pinterest social-icon fi-icon"></div>');	
	$('.sharedaddy .share-email a span').before('<div class="fi-mail social-icon fi-icon"></div>');		
	$('.sharedaddy ul.medium-block-grid-3 > li ').addClass("share-network");
	$('.sharedaddy ul.medium-block-grid-3 li a > span').addClass("social-network");
	$('#content.widecolumn').addClass("row");
	$('#content.widecolumn.row h2').addClass("medium-10 medium-centered columns text-center");	
	$('#content.widecolumn.row p.lead-in').addClass("medium-10 medium-centered columns");
	$('#content.widecolumn.row #signup-welcome').addClass("medium-10 medium-centered columns");
	$('#content.widecolumn.row p.view').addClass("medium-10 medium-centered columns text-center");
	$('#content.widecolumn.row p.view a').addClass("underline");
	

    if ( $.fn.hyphenate ) {
        $('#product-categories li a').hyphenate('de');
		$('.mobile h1.page-title').hyphenate('de');
		$('.mobile h1.archive-title').hyphenate('de');
		$('.mobile h1.entry-title').hyphenate('de');
		$('.tablet h1.entry-title').hyphenate('de');
		$('.mobile h2.entry-title a').hyphenate('de');
		$('h3.site-section').hyphenate('de');
		$('.mobile h1.product-title').hyphenate('de');
		$('.mobile ul#press-reviews blockquote').hyphenate('de');
		$('#blogs-posts .masonry-brick header.entry-header h3.entry-title a').hyphenate('de');
		$('.page-template-page-template-buchhandle-php #agents-contact li .agent-description p a').hyphenate('de');
		$('.page-template-page-template-buchhandle-php h1.page-title').hyphenate('de');		
		$('figure figcaption').hyphenate('de');		
	}
	        
	//Books.init();

	$(document).foundation();
	

    if ( $.fn.masonry ) {
        var timeOutMasonry = 0;
        paramsMasonry = {
            itemSelector: '.masonry-brick',
            columnWidth: 273,
            //gutter: 10,
        };
        $('#masonryContainer .masonry-brick img').on('load',function() {
            if ( timeOutMasonry > 0 ) clearTimeout( timeOutMasonry );
            timeOutMasonry = setTimeout(function() {
                $('#masonryContainer').masonry( paramsMasonry );
            },100);
        });

        setTimeout(function() {
            $('#masonryContainer').masonry( paramsMasonry );
        },50);


        var timeOutMasonry = 0;
        var paramsBookMasonryContainer = {
            itemSelector: '.BookMasonry-brick',
        };
        $('#BookMasonryContainer .BookMasonry-brick img').on('load',function() {
            var timeOutMasonry = setTimeout(function() {
                $('#BookMasonryContainer').masonry( paramsBookMasonryContainer );
            },100);
        });

        setTimeout(function() {
            $('#BookMasonryContainer').masonry( paramsBookMasonryContainer );
        },100)

        $('#blogs-posts > .row').resize(function(e) {
            var w = $(this).width();
            if ( w <= 805 && w > 500 ) {
                var s = ( w / 3 );
                $(this).find('.masonry-brick').css({ width: Math.ceil( s * 0.97 ) });

                setTimeout(function( paramsMasonry ) {
                    var newParamsMasonry = paramsMasonry;
                    newParamsMasonry['columnWidth'] = Math.ceil(s - ( s * 1 ) );
                    newParamsMasonry['gutter'] = 5;
                    $('#masonryContainer').masonry( newParamsMasonry );
                },200, paramsMasonry);
            }
            else {
                paramsMasonry['columnWidth'] = 273;
                paramsMasonry['gutter'] = false;
                $(this).find('.masonry-brick').css({ width: 250 });
                setTimeout(function( paramsMasonry ) {
                    $('#masonryContainer').masonry( paramsMasonry );
                },200, paramsMasonry);
            }
        });

        
        

		/*
		var timeOutMasonry = 0;
		var paramsKategorieMasonryContainer = {
		    itemSelector: '.KategorieMasonry-brick',
            columnWidth: '.medium-2'
            gutter: 0
		};
		$('#KategorieMasonryContainer .KategorieMasonry-brick img').on('load',function() {
		    var timeOutMasonry = setTimeout(function() {
		        $('#KategorieMasonryContainer').masonry( paramsKategorieMasonryContainer );
		    },100);
		});
		
		setTimeout(function() {
            console.log( paramsKategorieMasonryContainer );
		    $('#KategorieMasonryContainer').masonry( paramsKategorieMasonryContainer );
		},10000); 
		*/       
	}
	
    // -------------------------------------------------------------
	//   Basic Navigation
	// -------------------------------------------------------------

    if ( $.fn.sly ) {
        setTimeout(function () {
            var $frame  = $('#basic');
            var $slidee = $frame.children('ul').eq(0);
            var $wrap   = $frame.parent();

            // Call Sly on frame
            $frame.sly({
                horizontal: 1,
                itemNav: 'basic',
                smart: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                startAt: 0,
                scrollBar: $wrap.find('.scrollbar'),
                scrollBy: 1,
                pagesBar: $wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                easing: 'easeOutExpo',
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,

                // Buttons
                forward: $wrap.find('.forward'),
                backward: $wrap.find('.backward'),
                prev: $wrap.find('.prev'),
                next: $wrap.find('.next'),
                prevPage: $wrap.find('.prevPage'),
                nextPage: $wrap.find('.nextPage')
            });
        },200);
    }


    $('[data-tttmagentorest-login]').on('tttmagentorest:onload',function(event,response) {
        if ( $(this).attr('data-tttmagentorest-login-subtemplate') == 'loginheader' ) {
            $('[data-tttmagentorest-basket]').trigger('tttmagentorest:reload');
            $('[data-tttmagentorest-wishlist]').trigger('tttmagentorest:reload');
        }
    });

       
    $('[data-tttmagentorest-basket]').on('tttmagentorest:onload',function(event, addProduct, removeProduct) {

        $('[data-basket-flag-id]').removeClass('active');
        $(this).find('[data-product]').each(function() {
            var post_id = $(this).attr('data-product');
            $('[data-basket-flag-id="'+post_id+'"]').addClass('active');
        });

        if ( addProduct > 0 || removeProduct > 0 ) {
            if ( $('[data-tttmagentorest-noscroll]').length <= 0 ) {
                if ( addProduct > 0 ) {
                    $('#openbasket').click();
                }

                $('html, body').animate({
                    scrollTop: 0,
                    complete: function() {
                        $('[data-tttmagentorest-basket]').fadeIn();
                    }
                }, 1000);
            }
        }
    });

    setInterval(function() {
        var bookmark = $('#magento-connect .bookmark');
        if ( bookmark.find('a[data-dropdown]').hasClass('open') ) {
            bookmark.addClass('open');
        }
        else if ( bookmark.hasClass('open') ) {
            bookmark.removeClass('open');
        }
    },100);

    $('[data-tttmagentorest-wishlist]').on('tttmagentorest:onload',function(event, addProduct, removeProduct) {

        $('[data-wishlist-flag-id]').removeClass('active');
        
        var a =  $( this ).children('a[data-dropdown]:first');
        
        a.mouseenter(function(event) {
            $(this).data('mouseenter',true);
        })
        .mouseleave(function(event) {
            $(this).data('mouseenter',false);
        })
        .on('click',function(event) {
            if ( $('body').hasClass('mobile') || $('body').hasClass('tablet') ) {
                event.stopPropagation();
                return true;
            }
            if ( $(this).data('mouseenter') == true && !$(this).hasClass('logged') ) {
                event.stopPropagation();
                return true;
            }
        });

        $(this).find('[data-product]').each(function() {
            var post_id = $(this).attr('data-product');
            $('[data-wishlist-flag-id="'+post_id+'"]').addClass('active');
        });

        if ( addProduct > 0 || removeProduct > 0 ) {
            
            if ( $('body').hasClass('mobile') || $('body').hasClass('tablet') ) {
                if ( a.hasClass('logged') ) {
                    window.location.href = a.attr('href');
                    return true;
                }
                else {
                    if ( confirm( $(this).children('[data-dropdown-content]').find('.text-center a').html() ) ) {
                        window.location.href = a.attr('href');
                        return true;
                    }
                }
            }
            else {
                a.click();
                $('html, body').animate({
                    scrollTop: 0,
                    complete: function() {
                        $('[data-tttmagentorest-wishlist]').fadeIn();
                    }
                }, 1000);
            }
        }
    });

   

    $('#bestseller .load-more a').on('click',function(event) {
        event.preventDefault();

        if (!$(this).data('show')) {
            var height = $('#homeproducts').height();
            $('#homeproducts').data('origHeight',height).css({ height: height, overflow: 'hidden' });

            var count = 0;
            var plusHeight = 0;
            $('#homeproducts .hidden').each(function() {
                if ( $(this).outerHeight(true) > plusHeight )
                    plusHeight = $(this).outerHeight(true);
                
                count++;
            });

            plusHeight = Math.ceil( count / 4 ) * plusHeight + 20;

            $('#homeproducts').animate({ height: height + plusHeight });

            // To show
            $('#homeproducts .hidden').fadeIn();
            
            $(this).data('show',true).addClass('close');
        }
        else {
            // To hidden
            $('#homeproducts .hidden').fadeOut(function() {
                $('#homeproducts').animate({ height: $('#homeproducts').data('origHeight') });
            });
            
            $(this).data('show',false).removeClass('close');
        }
    });


    $('#masonryContainer').on('ttt-loadmore:after-load',function(event,data,el) {
        var masonryLastImage = 0;
        setTimeout(function( el, data ) {
            var toAdd = [];
            $('.masonry-brick', el ).each(function() {
                var style = $(this).attr('style');

                if ( !style ) {
                    if ( $(this).find('img').length > 0 ) {
                        $(this).find('img').load(function() {
                            var o = $(this).parents('.masonry-brick').get(0);
                            $(el).masonry('appended',o);

                        }).each(function() {
                            if(this.complete) $(this).load();
                        });
                    }
                    else {
                        toAdd.push( $(this).get(0) );
                    }
                }

            });
            $(el).masonry('appended',toAdd);
        },50, this, data );
    });

    if ( $.fn.fancybox ) {
        $.mobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

        $('.fancyarea a').each(function() {
            var img = $(this).find('img');
            if ( img.length <= 0) return;

            var post_id = $(this).parents('article').attr('id');

            $(this).attr('data-fancybox','');
            $(this).attr('rel','article-'+post_id);
            
            $(this).attr('data-count', $('[rel="article-'+post_id+'"]').length  );
            $(this).attr('data-fancybox-path', 'foto-'+post_id+'-'+$('[rel="article-'+post_id+'"]').length  );


            if ( !$(this).attr('title') )
                $(this).attr('title',$(img).attr('title'));
        });

        if( $('body').hasClass('desktop') ) {
            $("[data-fancybox]").fancybox({
                helpers: {
                    title: null
                },
                nextEffect: 'elastic',
                prevEffect: 'elastic',
                margin: 100,
                afterShow: function(opts, obj) {

                    var path = '#!/' + $(this.element).attr('data-fancybox-path');

                    if (typeof(window.history.pushState) == 'function') {
                        window.history.pushState(null, path, path);
                    } else {
                        window.location.hash = path;
                    }

                    var logo = $('<div class="callwey-logo"></div>');
                    logo.append( $('#logotype img').clone() );
                    $( this.wrap ).append( logo );


                    var callwey = $('<div class="callwey row"><div class="columns medium-5 author"></div><div class="columns medium-8 title"></div><div class="columns medium-5 count"></div></div>');
                    $( this.wrap ).append( callwey );

                    var title = $('<span class="title">'+ $(this.element).attr('title')+'</span>');
                    $( callwey ).find('.title').append( title );

                    if ( $(this.element).attr('data-author') ) {
                        var author = $('<span>Foto: '+ $(this.element).attr('data-author')+'</span>');
                        $( callwey ).find('.author').append( author );
                    }
                    else {
                        $( callwey ).find('.author').append( '&nbsp;' );
                    }

                    var group = $('[rel="'+$(this.element).attr('rel')+'"]');
                    if ( group.length > 1 ) {
                        var count = $('<span> '+ $(this.element).attr('data-count') +'/'+ group.length +'</span>');
                        $( callwey ).find('.count').append( count );
                    }

                    $('.fancybox-close').html('<span>SCHLIESSEN</span>');

                    //$('.fancybox-close').appendTo( '.fancybox-overlay' );
                }
            });
        }
        else {
            $("[data-fancybox]").swipebox({
            });
        }

    
        if ( window.location.hash ) {
            var regexp = new RegExp('#!\/(foto-.*)');
            var s = String( window.location.hash );
            var r = regexp.exec( s );
            if ( r ) {
                setTimeout(function(r) {
                    $('[data-fancybox-path="'+r[1]+'"]').click();
                },100,r);
            }
        }


    }


    // Scroll to the top

    $('.goto-top a').on('click',function(event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: 0 },1500);
    });



    if ( _cwm ) {
        for ( var i in _cwm ) {
            var act = _cwm[i][0],
                val = _cwm[i][1];

            if ( act == 'menu' ) {
                $( val ).addClass('current-menu-item');
            }
            else if ( act  == 'flag' ) {
                var p = $('#menu-main .current-menu-item');
                p.css({ position: 'relative', overflow: 'visible' });
                var height = p.outerHeight() - 5;
                var width = p.outerWidth() + 25;
                var flag = $('<div class="ebookflag"></div>');
                flag.css({
                    position: 'absolute',
                    top: height,
                });
                flag.on('menu:resize',function() {
                    var width = $(this).parent().width() + 25;
                    $(this).width( width );
                });
                p.append(flag);
                flag.trigger('menu:resize');
            }

        }
    }

    if ( _sharing_dialog ) {
        for ( var i in _sharing_dialog ) {
             var name = _sharing_dialog[i][0],
                 ev = _sharing_dialog[i][1],
                 func = _sharing_dialog[i][2];
             
             jQuery( 'a.share-'+name).on( ev, func );
        }
    }


    menuInterval = setInterval(function() {
        var ww = $(window).width(),
            nav = $('#site-navigation'),
            men = $('#menu-main'),
            els = $(men).find('.menu-item a'),
            outHeight = ( parseInt( men.height() ) >  parseInt( nav.height()+3 ) ),
            outDim = false;

        if ( outHeight == false ) outDim = false;
        if ( men.outerWidth(true)+1 < nav.width() ) outDim = true;
        if ( els.first().css('padding-left') == 'auto' || els.first().css('padding-left') <= 1 ) outDim = true;


        if ( ww >= 767 ) {
            men.css({ 'width': 'auto' });
            if ( outDim == true && outHeight == false ) {
                var inc = 0,
                    padding = 'padding-left',
                    opacity = 0;
                
                inc = ( ( ( nav.width() - men.outerWidth(true) ) / els.length ) / 2 );
                inc = inc - ( inc * 10 / 100 );
                //inc = 1

                var p = parseFloat( els.last().css(padding).replace('px','') );
                var r = p + inc;
                // if ($('body').hasClass('safari')) {
                    //console.log('safari', p, Math.floor(p),  Math.floor(r), r);
                    if ( Math.floor(p) == Math.floor(r) ) {
                        r = r + ( nav.width() - men.outerWidth(true) ) - 1;
                        //console.log( r );
                        els.last().css({ 'padding-left': r+'px', 'opacity':  0 });
                    }
                    else {
                        els.css({ 'padding-left': r+'px', 'padding-right': r+'px', 'opacity':  0 });
                    }
                // }
                // else {
                //     els.css({ 'padding-left': r+'px', 'padding-right': r+'px', 'opacity':  0 });
                // }
                nav.find('.ebookflag').trigger('menu:resize');
            }
            else if ( outHeight === true ) {
                els.css({ 'padding-left': '0px', 'padding-right': '0px', 'opacity': 0 });
                men.css({ 'width': 'auto' });
                //console.log( 2 );
            }
            else {
                els.animate({ 'opacity': 1 },100);
                //console.log( 3 );
            }
        }
        else {
            els.css({ 'width': 'auto', 'opacity': 1 });
            men.css({ 'width': '100%' });
        }
    },300);

    $('.click_mobile_search_dropdown').click(function(event) {
        event.preventDefault();
        event.stopPropagation();
        if ($('#mobile_search_dropdown').css('display') != 'block') {
            $('#mobile_search_dropdown').css('display','block');
        }
        else {
            $('#mobile_search_dropdown').css('display','none');
        }
    });

    $('body').on('callwey:secondary_top',function(event) {

    
        if ( $(this).hasClass('desktop') == false ) return;
        if ( $(this).hasClass('archive') == false && $(this).hasClass('single') == false ) return;
        if ( $(this).hasClass('category-campus') ) return;
        if ( $(this).hasClass('category-hotels') ) return;
        if ( $(this).hasClass('category-bildergalerie') ) return; // Intranet

        if ( $(this).hasClass('einfamilienhaeuser') ) {
            if ( $(this).hasClass('category-produkte') ) return;
            if ( $(this).hasClass('category-profile') ) return;
            if ( $(this).hasClass('category-') ) return;
        }

        setInterval(function() {
            var valueTop =  Number($('article header').height()) - 20;
            if ( valueTop > 0 ) {
                if ( Number($('#secondary').css('top')) != valueTop ) {
                    $('#secondary').css('top', valueTop );
                    $('.desktop.single.single-post #primary').trigger('callwey:autoheight');
                }
            }
        },1000);
    }).trigger('callwey:secondary_top');


    // Auto ajust sidebar height
    var secondary_time_id = 0;

    $('.desktop.single.single-post #primary').on('callwey:autoheight',function(event) {
        if (secondary_time_id > 0 ) {
            clearTimeout(secondary_time_id);
        }
        secondary_time_id = setTimeout(function() {
            if ( $('.desktop.single.single-post #primary').height() < $('.desktop.single.single-post #secondary').height() + 540 ){
                $('.desktop.single.single-post #primary').parents('.row').height( $('.desktop.single.single-post #secondary').height() + 540 );
            }
        },500);
    });


    $('.desktop.single.single-post #primary img').on('load',function(event) {
        $('.desktop.single.single-post #primary').trigger('callwey:autoheight');
    });

    $('.desktop.single.single-post #secondary img').on('load',function(event) {
        $('.desktop.single.single-post #primary').trigger('callwey:autoheight');
    });
    
    $('html').on('adrotate-ajax:before',function(event) {
        $('.desktop.single.single-post #primary').trigger('callwey:autoheight');
    });

    $('.desktop.single.single-post #primary').trigger('callwey:autoheight');

});
