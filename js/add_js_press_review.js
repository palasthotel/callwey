jQuery(document).ready(function($) {


    $('#_clwy_reviews_q').on('change',function(event)  {
        $('#ttt_magento_fields').trigger('change:selector',[ $(this).val() ] );
    });



    $('#ttt_magento_fields').on('change:selector',function(event,num) {

        var limit = 20;
        if ( !num ) num = 1;

        for( var i = 1; i <= limit; i++ ) {
            $('.cmb_id__clwy_press_review_'+i).css({ display: 'none' });
            $('.cmb_id__clwy_press_review_cite_'+i).css({ display: 'none' });
            $('.cmb_id__clwy_divider_'+i).css({ display: 'none' });
        }

        for( var i = 1; i <= num; i++ ) {
            $('.cmb_id__clwy_press_review_'+i).fadeIn();
            $('.cmb_id__clwy_press_review_cite_'+i).fadeIn();
            $('.cmb_id__clwy_divider_'+i).fadeIn();
        }
        
    });

    $('#ttt_magento_fields').trigger('change:selector',[ $('#_clwy_reviews_q').val() ] );

    
});
