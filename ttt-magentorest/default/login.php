<?php if ( !$this->isLogged() ): ?>
	<p class="login-text"><?php _e('Sie haben einen bestehenden Account bei Callwey.de? Hier können Sie sich mit ihrem Zugangsdaten einloggen:', 'callwey'); ?></p>
	<div id="login" data-dropdown-content>
	    <form id="tttmagentorest_form_login" method="post" class="custom">
            <div class="progressel">
		        <div class="button round button-noshadow"><?php _e('Login...', 'callwey') ?></div>
            </div>
			<fieldset>
			    <legend class="hide"><?php _e('Kunden-Login', 'callwey'); ?></legend>    
		        <label class="hide"><?php _e('Email', 'tttmagentorest' );?></label>
		        <input type="text" class="text round text-center" name="email" placeholder="<?php _e('E-mail', 'tttmagentorest' );?>">
		        <label class="hide"><?php _e('Password', 'tttmagentorest' );?></label>
		        <input type="password" class="text round text-center" name="password" placeholder="<?php _e('Passwort', 'tttmagentorest' );?>">
		        <input class="button round button-noshadow" type="submit" value="<?php _e('Login', 'callwey') ?>">
			</fieldset>
	    </form>
	</div>
<?php else: ?>
    <a class="logged close button round button-noshadow" href="#"><?php echo $this->getClient()->firstname; ?>, <?php _e('Abmelden','tttmagentorest');?></a>
<?php endif; ?>
