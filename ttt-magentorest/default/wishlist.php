<?php if ( !$this->getWishlist() ): ?>
	<div class="medium-18 columns text-center"><h2 class="uppercase"><?php _e('Ihre Wunschliste ist noch leer', 'callwey'); ?></h2></div>
<?php else: ?>
<ul id="wishlist-products" class="medium-block-grid-5">
    <?php foreach( $this->getWishlist()->products as $_post_id => $product ): ?>
    <li>
	    <div class="book-item" itemscope itemtype="http://schema.org/Book">
				<div class="book">
	                <?php
		                global $post;
		                $post = get_post( $_post_id );
		                setup_postdata( $post );
	                ?>				
                    <a class="book-thumbnail" href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('book-home'); ?>
                    </a>
				</div>
				<header class="book-header">
					<h3 class="book-title" itemprop="name">
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h3>
				</header>
				<link itemprop="bookFormat" href="http://schema.org/Paperback">				
		</div>
		<div class="product-actions text-center">
			<?php echo do_shortcode('[tttmagentorest action="add2basket"]');?>
			<a class="button round button-noshadow remove tttmangetorest_remove2wishlist" href="#" data-product="<?php echo $_post_id; ?>"><?php _e('VON LISTE ENTFERNEN', 'callwey'); /* remove from wishlist */?></a>
		</div>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>