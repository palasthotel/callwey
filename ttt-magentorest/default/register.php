<form class="custom" id="tttmagentorest_form_register" method="post" action="?">
	<fieldset>
	    <legend class="hide"><?php _e('Registrieren', 'callwey'); ?></legend>
		
		<input type="hidden" name="issubmit" value="1" >

    <?php if (isset($errors['_main'])): ?>
        <div data-alert class="alert-box alert">
            <?php if ( $_errors['_main'] == 'This customer email already exists' ): ?>
            	Diese E-Mail Adresse existiert bereits
            <?php else: ?>
	            <?php echo $errors['_main']; ?>
	        <?php endif; ?>
        </div>
    <?php endif; ?>

	<div class="row">
        <div class="medium-5 columns">
            <label for="name-label" class="left inline"><?php _e('Vorname', 'callwey'); ?></label>
        </div>
        <div class="medium-7 columns">
            <input id="name-label" type="text" class="text round" name="name" value="<?php echo $attr['name']; ?>">
		    <?php if (isset($errors['name'])): ?>
	            <small class="error"><?php _e('Bitte geben Sie hier Ihren Vornamen ein','callwey'); ?><?php //echo $errors['name']; ?></small>
		    <?php endif; ?>
		</div>
		<div class="medium-6 columns"></div>
	</div>

	<div class="row">
        <div class="medium-5 columns">
            <label for="lastname-label" class="left inline"><?php _e('Nachnahme', 'callwey'); ?></label>
        </div>
        <div class="medium-7 columns">
            <input id="lastname-label" type="text" class="text round" name="lastname" value="<?php echo $attr['lastname']; ?>">
		    <?php if (isset($errors['lastname'])): ?>
		        <small class="error"><?php _e('Bitte geben Sie hier Ihren Nachname ein','callwey'); ?><?php //echo $errors['lastname']; ?></small>
		    <?php endif; ?>
        </div>
		<div class="medium-6 columns"></div>
	</div>

	<div class="row">
        <div class="medium-5 columns">
            <label for="email-label" class="left inline"><?php _e('E-Mail', 'callwey'); ?></label>
        </div>
        <div class="medium-7 columns">
            <input id="email-label" type="text" class="text round" name="email" value="<?php echo $attr['email']; ?>">
		    <?php if (isset($errors['email'])): ?>
	            <small class="error"><?php _e('Die E-Mail Adresse scheint nicht zu stimmen.','callwey'); ?><?php //echo $errors['email']; ?></small>
		    <?php endif; ?>
        </div>
        <div class="medium-6 columns">       
	        <small class="field-text"><?php _e('Die Bestätigung wird an diese E-Mail-Adresse gesendet', 'callwey') ?></small>
        </div>
        <div class="row"></div>
        <div class="medium-5 columns">
            <label for="emailr-label" class="left inline"><?php _e('E-Mail bestätigen', 'callwey'); ?></label>
        </div>
        <div class="medium-7 columns">
            <input id="emailr-label" type="text" class="text round" name="emailconfirm" value="<?php echo $attr['emailconfirm']; ?>">
		    <?php if (isset($errors['email'])): ?>
	            <small class="error"><?php _e('Die E-Mail Adresse scheint nicht zu stimmen.','callwey'); ?><?php //echo $errors['email']; ?></small>
		    <?php endif; ?>
        </div>
        <div class="medium-6 columns"></div>
	</div>
            
	<div class="row">
		<div class="medium-5 columns">
	        <label for="password-label" class="left inline"><?php _e('Passwort', 'callwey'); ?></label>
		</div>
		<div class="medium-7 columns">
	        <input id="password-label" type="password" class="text round" name="password">
		    <?php if (isset($errors['password'])): ?>
		        <small class="error"><?php _e('Die Passwörter stimmen nicht überein','callwey'); ?><?php //echo $errors['password']; ?></small>
		    <?php endif; ?>
		</div>
		<div class="medium-6 columns">
			<small><?php _e('6 Zeichen oder länger mit minderstens einer Zahl', 'callery'); ?></small>
		</div>
        <div class="row"></div>		
		<div class="medium-5 columns">
	        <label for="passwordr-label" class="left inline"><?php _e('Passwort bestätigen', 'callwey'); ?></label>
		</div>
		<div class="medium-7 columns">
	        <input id="passwordr-label" type="password" class="text round" name="passwordconfirm">
		    <?php if (isset($errors['password'])): ?>
		        <small class="error"><?php _e('Die Passwörter stimmen nicht überein','callwey'); ?><?php //echo $errors['password']; ?></small>
		    <?php endif; ?>	        
		</div>
		<div class="medium-6 columns"></div>
	</div>

	<div class="row">	
		<label for="checkbox1" class="medium-18 columns newsletter">
			<input type="checkbox" id="checkbox1" style="display: none;" name="newsletter">
			<span class="custom checkbox"></span> <?php _e('Ich möchte den zweiwöchigen Callwey Newsletter abonnieren', 'callwey')?>
		</label>
	</div>

    <div class="row">
    	<div class="medium-18 columns">
    		<input type="submit" class="button round button-noshadow" value="<?php _e('Registrieren','callwey'); ?>">
    	</div>
    </div>

    </fieldset>
</form>

