<?php if ( !$this->getBasket() ): ?>
    <a href="<?php bloginfo ('url'); ?>/warenkorb"><?php _e('warenkorb', 'callwey'); ?> <span>(0)</span></a>
<?php else: ?>
    <a href="<?php bloginfo ('url'); ?>/warenkorb"><?php _e('warenkorb', 'callwey'); ?> <span>(<?php echo $this->getBasket()->count; ?>)</span></a>
<?php endif; ?>
