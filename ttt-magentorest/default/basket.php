
<?php if ( !$this->getBasket() ): ?>
    <div class="medium-18 columns text-center"><h2 class="uppercase"><?php _e('Ihr Warenkorb ist noch leer', 'callwey'); ?></h2></div>
<?php else: ?>
        <form action="<?php echo $this->get('basketurl');?>" method="post" target="_blank" data-tttmagentorest-noscroll>

            <table>
				<thead>
					<tr>
						<th><?php _e('ARTIKEL', 'callwey'); ?></th>
						<th><?php _e('BESCHREIBUNG', 'callwey'); ?></th>
						<th><?php _e('EINZELPREIS', 'callwey'); ?></th>
						<th><?php _e('ANZAHL', 'callwey'); ?></th>
						<th><?php _e('ZWISCHENSUMME', 'callwey'); ?></th>
					</tr>
				</thead>
				<tbody>

                <?php $_basket = $this->getBasket(); ?>
                <?php foreach( $_basket->products as $_post_id => $product ): ?>
                    <?php
                    global $post;
                    $post = get_post( $_post_id );
                    setup_postdata( $post );
                    $_total_tax[ $product['tax_class_id'] ] += $product['_total'];
                    ?>
                    <?php $_getquery[] = $product['product_id'].','.$product['_count']; ?>
                    
                    <tr>
                        <td><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('book-home'); ?></a></td>
                        <td class="text-left">
                        	<h3 class="book-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        	<h3 class="subheader"><?php echo $product['subtitle']; ?><!--<br/><small><?php echo book_ebook_tax($product['tax_class_id']); ?></small>--></h3>
                        </td>
                        <td><span class="price"><?php echo number_format( (float) $product['price'],2 ); ?>€</span></td>
                        <td>
                            <a class="remove tttmangetorest_remove2basket" href="#" data-product="<?php echo $_post_id; ?>">-</a>
                            <span class="count"><div><?php echo $product['_count']; ?></div></span>
                            <a class="add tttmangetorest_add2basket" href="#" data-product="<?php echo $_post_id; ?>">+</a>
                        </td>
                        <td><span class="price"><?php echo number_format( (float) $product['_total'],2 ); ?>€</span></td>
                    </tr>
                    <tr class="action-links">
                    	<td colspan="5">
							<?php echo do_shortcode('[tttmagentorest action="add2wishlist" style="link"]');?> | <a class="remove tttmangetorest_remove2basket" href="#" data-product="<?php echo $_post_id; ?>"><?php _e('AUS WARENKORB ENTFERNEN', 'callwey'); ?></a>
                    	</td>
                    </tr>
                <?php endforeach; ?>

				</tbody>
				<tfoot>
					<tr class="actions-basket">
						<td colspan="5">
							<a href="#"><?php _e('weiter einkaufen', 'callwey'); ?></a>
						</td>
					</tr>

                    <!--
                    <?php if ( isset($_total_tax) && is_array($_total_tax) ): ?>
                        <?php foreach ($_total_tax as $tax_id => $_total ): $tax_val = book_ebook_tax_float($tax_id); ?>
                            <?php 
                            $_last_tax = $_total * $tax_val / 100;
                            $_total_tax[ $tax_id ] = $_last_tax;
                            $_sum_tax += $_last_tax;
                            ?>
                        <?php endforeach; ?>

                        <tr class="subtotal-price">
                            <td colspan="3"></td>
                            <td class="text-right">
                                <span class="subtotal"><?php _e('ZWISCHENSUMME', 'callwey'); ?></span>
                            </td>
                            <td>
                                <span class="subtotal"><?php echo number_format( (float) $_basket->total - $_sum_tax ,2 ); ?> €</span>
                            </td>
                        </tr>

                    <?php endif; ?>


                    <?php if ( isset($_total_tax) && is_array($_total_tax) ): ?>
                        <?php foreach ($_total_tax as $tax_id => $_total ): $tax_val = book_ebook_tax_float($tax_id); ?>
                            
                        <tr class="total-tax">
                            <td colspan="3"></td>
                            <td class="text-right">
                                <span class="tax"><?php _e('STEUER '.$tax_val.'% MwSt', 'callwey'); ?></span>
                            </td>
                            <td>
                                <span class="tax"><?php echo number_format( $_total,2 ); ?> €</span>
                            </td>
                        </tr>

                        <?php endforeach; ?>
                    <?php endif; ?>
                    -->
                   

                    <tr class="total-price">
                        <td colspan="3"></td>
                        <td class="text-right">
                        	<span class="price"><?php _e('GESAMTSUMME', 'callwey'); ?></span>
                        </td>
                        <td>
                        	<span class="price"><?php echo number_format( (float) $_basket->total,2 ); ?> €</span>
                        </td>
                    </tr>
					<tr>
						<td colspan="4">
			                <input type="hidden" name="products" value="<?php echo join(';',$_getquery); ?>">
			                <?php if ( $this->isLogged() ): ?>
			                <input type="hidden" name="email" value="<?php echo $this->getClient()->email; ?>">
			                <?php endif; ?>
						</td>
						<td colspan="1" class="text-right">
				            <div class="button-wrapper"><input class="button button-noshadow button-white" type="submit" value="<?php _e('zur Kasse gehen','callwey'); ?>"></div>
						</td>
					</tr>
				</tfoot>

                <?php wp_reset_postdata(); ?>

            </table>


        </form>
<?php endif; ?>
