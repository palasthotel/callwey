
<?php if ( $this->isLogged() ): ?>
<a href="<?php bloginfo ('url'); ?>/wunschliste" data-dropdown="wishlist_dropdown" class="logged">
   <?php _e('Wunschliste', 'callwey'); ?> <span>(<?php echo (int) $this->getWishlist()->count; ?>)</span>
</a>
<?php else: ?>
<a href="<?php bloginfo ('url'); ?>/wunschliste" data-dropdown="wishlist_dropdown">
    <?php _e('Wunschliste', 'callwey'); ?> <span>(0)</span>
</a>
<?php endif; ?>



<div id="wishlist_dropdown" class="f-dropdown content custom-left" data-dropdown-content>
	<a href="#" data-dropdown="wishlist_dropdown" class="close-wishlist bookmark"><?php _e('Wunschliste', 'callwey'); ?> <span>(<?php echo (int) $this->getWishlist()->count; ?>)</span></a>

    <?php if ( $this->isLogged() ): ?>
        <?php if ( !$this->getWishlist() ): ?>
        	<span class="empty-wishlist"><?php _e('Ihre Wunschliste ist noch leer', 'callwey'); ?></span>
        <?php else: ?>
            <table>
            	<thead>
        			<tr>
        				<th colspan="3"><?php _e('zuletzt hinzugefügt', 'callwey') ?></th>
        			</tr>
            	</thead>
            	<tbody>
                <?php $_count = 0; ?>
                <?php foreach( array_reverse($this->getWishlist()->products, true) as $_post_id => $product ): $_count++; ?>
                    <?php if ($_count > 3 ) break; ?>
                    <?php
                    global $post;
                    $post = get_post( $_post_id );
                    setup_postdata( $post );
                    ?>
                    <tr>
                    	<td>
                            <a class="product-thumbnail" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('book-home'); ?>
                            </a>
                        </td>
                        <td>
                        	<a class="product-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        					<strong class="product-price"><?php echo number_format( (float) $product['price'],2 ); ?>€</strong>
        				</td>
                        <td><a class="fi-x remove tttmangetorest_remove2wishlist" href="#" data-product="<?php echo $_post_id; ?>"></a></td>
                    </tr>
                <?php endforeach; ?>
            	</tbody>
            </table>
            <?php wp_reset_postdata(); ?>
        
        <?php endif; ?>
        <div class="row text-center"><a href="<?php bloginfo ('url'); ?>/wunschliste" class="button round button-noshadow"><?php _e('MEINE WUNSCHLISTE ANSEHEN', 'callwey'); ?></a></div>
    <?php else: ?>
        <div class="row text-center"><a href="<?php bloginfo ('url'); ?>/login" class="button round button-noshadow"><?php _e('Bitte melden Sie sich zuerst an, um die Wunschliste nutzen zu können', 'callwey'); ?></a></div>
    <?php endif; ?>
</div>

<?php if ( !$this->isLogged() && isset($_REQUEST['addproduct']) ): ?>
<script type="text/javascript">
//    jQuery('.ttt_magentorest_wishlist > a[data-dropdown]').click();
</script>
<?php endif; ?>
