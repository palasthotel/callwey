<div id="login" data-dropdown-content>
<?php if ( !$this->isLogged() ): ?>
    <a class="log-in" href="<?php bloginfo( 'url' ); ?>/login"><?php _e('ANMELDEN', 'callwey') ?></a>
<?php else: ?>
    <a class="log-in logged close" href="<?php bloginfo( 'url' ); ?>/login"><?php _e('ABMELDEN', 'callwey') ?></a>
<?php endif; ?>
</div>
