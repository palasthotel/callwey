<?php if ($style == 'button'): ?>
<a href="#" class="button radius button-noshadow button-white add-to-wishlist tttmangetorest_add2wishlist" data-product="<?php the_ID(); ?>" data-wishlist-flag-id="<?php the_ID(); ?>"><?php _e('AUF MEINE WUNSCHLISTE', 'callwey') ?></a>
<?php elseif ($style == 'bookmark'): ?>
<a href="#" class="bookmark-icon bookmark-link add-to-wishlist tttmangetorest_add2wishlist" data-product="<?php the_ID(); ?>" data-wishlist-flag-id="<?php the_ID(); ?>"></a>
<?php elseif ($style == 'link'): ?>
<a href="#" class="bookmark-link add-to-wishlist tttmangetorest_add2wishlist" data-product="<?php the_ID(); ?>" data-wishlist-flag-id="<?php the_ID(); ?>"><?php _e('AUF WUNSCHLISTE VERSCHIEBEN', 'callwey'); ?></a>
<?php endif; ?>
