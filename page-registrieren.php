<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('BEI CALLWEY als Kunde', 'callwey'); ?></h4>
	
	<h3 class="site-section text-center"><?php the_title(); ?></h3>
	
	<p class="site-section-text text-center"><?php _e('Nutzen Sie die Vorteile als registriertes Callwey Mitglied.<br>Füllen Sie dazu nur dieses Formular aus.', 'callwey') ?></p>

	<div id="primary" class="content-area medium-10 medium-centered columns">
		<main id="main" class="site-main row" role="main">	
			<hr>
			<?php while ( have_posts() ) : the_post(); ?>


				<?php //get_template_part( 'content', 'page' ); ?>
				
                <?php the_widget('TTTMagentorest_register_widget'); ?>
                <?php //the_content(); ?>


			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

</div>
<?php get_footer(); ?>
