<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header();?>

	<?php if ( is_tttdevice('desktop') || is_tttdevice('tablet') ): ?>
		<?php get_template_part( 'front-page-first-block' ); ?>
	
		<?php get_template_part( 'front-page-second-block' ); ?>
		
		<?php get_template_part( 'front-page-third-block' ); ?>
	<?php elseif ( is_tttdevice('mobile') ): ?>
		<?php get_template_part( 'front-page-first-block-mobile' ); ?>
	<?php endif; ?>

<?php get_footer(); ?>