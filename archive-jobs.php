<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); 
$cc = get_the_content();
?>
<div class="row">


	
	<header class="archive-header row">
		<h4 class="site-section-title text-center">
			<?php _e('Callwey', 'callwey'); ?>
		</h4>
		<h1 class="archive-title text-center">
			<?php _e('Jobs', 'callwey') ?>
		</h1>
		<div class="medium-17 medium-centered columns">
			<div class="page-description medium-18 columns">
				<hr>
			</div>
		</div>
	</header><!-- .page-header -->

	<div class="large-13 medium-18 columns">
	<section id="primary" class="content-area large-17 large-uncentered large-push-1 medium-17 medium-centered small-18 small-centered columns">
		<main id="main" class="site-main row" role="main">		
			<?php if($cc != '') : ?>
				<?php the_content(); ?>
			<?php else: ?>
			<?php endif; ?>		
			<h4 class="section-title text-center"><?php _e('AKTUELLE STELLENANGEBOTE', 'callwey'); ?></h4>
			<?php
				$jobs = array(
					'post_type'	 =>	'jobs',
					'posts_per_page' => -1,
					'order' => 'DESC',
					'orderby' => 'date'
				);					
				$jobs_query = new WP_Query($jobs);
			?>
			<?php if ($jobs_query->have_posts()) : ?>
			<div id="jobs-list" class="section-container accordion" data-section="accordion">
				<?php while ($jobs_query->have_posts()) : $jobs_query->the_post(); ?>
					<?php $pdf = get_post_meta( $post->ID, '_clwy_job_pdf', true ); ?>
					<section>
						<div class="section-inner">
							<div class="medium-18 columns">
								<h3 class="job-title">
									<?php the_title(); ?>
								</h3>
								<div class="medium-9 columns">
									<p class="job-location">
										<span><?php _e('Ort:', 'callwey'); ?></span>
										<?php
											$location = wp_get_object_terms(
								                $post->ID, 
								                array(
								                    'location', 
								                    'post_tag'
								                ) 
								            );
											if(!empty($location)){
											  if(!is_wp_error( $location )){
											    foreach($location as $term){
											      echo '<strong>'.$term->name.'</strong>'; 
											    }
											  }
											}				
										?>
									</p>
								</div>
								<div class="medium-9 columns">
								<?php //if ( get_post_meta( $post->ID, '_clwy_job_pdf', true ) ): ?>
<!-- 									<a class="download-pdf" target="_blank" href="<?php echo $pdf; ?>"><?php _e('ALS PDF<br>HERUNTERLADEN', 'callwey') ?></a> -->
								<?php //endif; ?>
									<a class="download-pdf" target="_blank" href="<?php pdf_file_url(); ?>"><?php _e('ALS PDF<br>HERUNTERLADEN', 'callwey') ?></a>
									<?php //$filepdf = pdf_file_url(); ?>
									<?php //var_dump($filepdf); ?>
								</div>
							</div>
						</div>
						<p class="open" data-section-title>
							<a href="#">
								<span class="open-arrow">
									<?php _e('ANZEIGEN', 'callwey') ?>
								</span>
								<span class="job-description-icon">
									<?php _e('JOBBESCHREIBUNG', 'callwey') ?>
								</span>
							</a>
						</p>
						<div class="content job-description" data-section-content>
							<?php the_content(); ?>
						</div>
					</section>
				<?php endwhile; ?>
			</div>
			<?php endif; wp_reset_postdata(); ?>
		</main><!-- #main -->
	</section><!-- #primary -->
	</div>

	<?php get_sidebar('page'); ?>

</div>
<?php get_footer(); ?>
