<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package _s
 */
?>
<?php if ( is_tttdevice('desktop') ): ?>
	<div id="secondary" class="widget-area large-4 large-pull-1 medium-4 medium-pull-1 hide-for-medium-down columns" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php if ( ! dynamic_sidebar( 'sidebar-general' ) ) : ?>
		<?php endif; // end sidebar widget area ?>
		<?php if (!is_search()): ?>
			<?php if ( ! dynamic_sidebar( 'sidebar-blog' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		<?php endif; ?>
	</div><!-- #secondary -->
<?php endif; ?>