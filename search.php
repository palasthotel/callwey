<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php if (is_main_site()): ?><?php _e('Callwey', 'callwey'); ?><?php else: ?><?php bloginfo('name'); ?><?php endif; ?></h4>

    <?php if (have_posts()) : ?>
	    
		<h3 class="site-section text-center">
			<?php printf( __( 'Suchergebnisse %s', 'callwey' ), '<span>' . get_search_query() . '</span>' ); ?>		
		</h3>
	
		<div id="primary" class="content-area large-13 medium-13 small-13 columns">
			<main id="main" class="site-main row" role="main">		
			<?php
	/*
				$searchresult = array(
			        'post_type'	 =>	array('post','produkt','ebook','rezepte'),
			        'posts_per_page' => -1,
					'tax_query' => array(			
						array(
							'taxonomy' => 'kategorie',
							'field' => 'id',
							'terms' => array(5),
							'operator' => 'NOT IN'
						)
					)
			    );
			    
			    $searchresult_query = new WP_Query($searchresult);	
	*/
		    ?>
		    	<?php while (have_posts()) : the_post(); ?>
	
					<?php get_template_part( 'partials/content', 'search' ); ?>
	
				<?php endwhile; ?>
	
			</main><!-- #main -->
			
			<?php //_s_content_nav( 'nav-below' ); ?>
	<!--
			<div class="medium-16 medium-centered columns">
				<div class="medium-17 medium-push-2 columns">
					<div class="load-more"><a href="#" data-tttloadmore-do="blogposts" data-tttloadmore-to="#main"><?php _e('Weitere Suchergebnisse anzeigen', 'callwey') ?></a></div>
				</div>
			</div>
	-->
	
		</div><!-- #primary -->

	<?php else: ?>

		<h3 class="site-section text-center">
			<?php _e('Keine Suchergebnisse gefunden','callwey') ?>
		</h3>
	
		<div id="primary" class="content-area large-13 medium-13 small-13 columns">
			<main id="main" class="site-main row" role="main">		
			</main>
		</div>
		
	<?php endif; ?>
	
	
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>