<div class="row">
	<section id="custom-related-content" class="medium-17 medium-centered columns">
	<?php if ( is_tttdevice('desktop') ): ?>
		<div class="medium-4 columns">
			<h3 class="section-title">
				<?php _e('ENTDECKEN<br>SIE<br>WEITERE<br>INHALTE', 'callwey') ?>
			</h3>
		</div>
		<div class="medium-14 columns">
			<div class="row">
			<ul class="medium-block-grid-4 related-content text-center">
	<?php elseif ( is_tttdevice('tablet') ): ?>
		<div class="medium-18 columns">
			<div class="row">
			<ul class="medium-block-grid-4 related-content text-center">
	<?php else: ?>
		<div class="small-18 columns">
			<div class="row">
			<ul class="small-block-grid-2 related-content text-center">
	<?php endif; ?>
                <?php foreach( array(1,2,3,4) as $box_id ): ?>
				<li>
					<aside>						
						<div class="content-image">
							<a href="<?php echo of_get_option('box_page_'.$box_id.'_url'); ?>">
								<?php //the_post_thumbnail('autor-widget'); ?>
								<?php $thumb_id = prefix_get_attachment_id_from_url( of_get_option('box_page_'.$box_id.'_image') ); ?>
								<?php $attachement = wp_get_attachment_image_src($thumb_id, 'autor-widget'); ?>
                                <img src="<?php echo $attachement[0]; ?>">
							</a>
						</div>
					</aside>
				</li>
                <?php endforeach; ?>
			</ul>
			</div>
		</div>
		<div class="row"></div>
	</section>
</div>