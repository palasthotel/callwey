<?php
	$_first = true;
	$CrossOrder = new CrossOrder();
	$home_slider_query = $CrossOrder->order('home_slider');
?>
<?php if ($home_slider_query->have_posts()) : ?>

    <?php $_count_mobile = 0; ?>		

	<?php while ($home_slider_query->have_posts()) : $home_slider_query->the_post();  $_count_mobile++; ?>

        <?php if ( $_count_mobile > 6 ) break; ?>

		<a class="entry-thumbnail mobile-orbit" href="<?php the_permalink(); ?>">								
            <?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'slider');?>
            <img src="<?php echo $large_image_url[0]; ?>">
	        <div class="orbit-caption">
	            <div class="caption-container">
	            <div class="caption">
	            <div class="caption-box">
	                <h2 class="entry-title">
	                    <?php the_title(); ?>
	                </h2>
	            </div>
	            </div>
	            </div>
	        </div>
		</a>

			<div class="first-block row">
			
				<div id="primary" class="content-area small-18 columns">
					<main id="main" class="site-main row" role="main">

					<?php if ($_count_mobile == 1) : ?>
											
						<section id="bestseller" class="small-18 columns">
			
							<h2 class="section-title text-left"><?php _e('Bestseller und Neuheiten','callwey'); ?></h2>
						
			                <style>
			                #homeproducts .hidden {
			                    display: none;
			
			                }
			                </style>
							<?php
								$_count = 0;
								$CrossOrder = new CrossOrder();
								$products_query = $CrossOrder->order('bestseller_home');
							?>
							<?php if ($products_query->have_posts()) : ?>
								<ul id="homeproducts" class="large-block-grid-4 medium-block-grid-3 small-block-grid-2">
								<?php while ($products_query->have_posts()) : $products_query->the_post(); $_count++; ?>
										<li class="<?php echo ( $_count <= 2 ? '':'hidden' ); ?>">
											<?php get_template_part( 'partials/content', 'home-bestseller' ); ?>
										</li>
								<?php endwhile; ?>
								</ul>
								<div class="load-more">
		                            <a href="#">
		                                <div class="down"><?php _e('Weitere Besteller anzeigen', 'callwey') ?></div>
		                                <div class="up"><?php _e('Weitere Bestseller wieder ausblenden', 'callwey') ?></div>
		                            </a>
		                        </div>
							<?php endif; wp_reset_postdata();?>
						</section>
												

					<?php elseif ($_count_mobile == 2) : ?>

						<div id="secondary" class="widget-area" role="complementary">
							<?php do_action( 'before_sidebar' ); ?>
								<?php if ( ! dynamic_sidebar( 'sidebar-mobile' ) ) : ?>
								<?php endif; // end sidebar widget area ?>
						</div>

						<div id="adspace" class="small-18 columns">
							<div class="row">
								<?php echo ajax_adrotate_group(1); ?>
							</div>
						</div><!-- #adspace -->

					<?php elseif ($_count_mobile == 3) : ?>
						
						<section id="promoted-product-category" class="small-18 columns">
							<div class="row">
								<h2 class="section-title text-left small-18 columns"><?php _e('Wichtige buch- kategorien','callwey'); ?></h2>
			                    <?php
									$promo_kategories = of_get_option('promoted_kategories');
									$promo_kategories_orderby = of_get_option('promoted_kategories_orderby');
									$promo_kategories_order = of_get_option('promoted_kategories_order');
									$homeParam;
									foreach($promo_kategories as $key => $value){
										if($value == 1){
											$homeParam[] = $key;
										}
									}                    
			                    	$terms = get_terms( 'kategorie', array(
			                    		'orderby'    => $promo_kategories_orderby, /*id, count, name, slug, none, term_order */
										'order'         => $promo_kategories_order,                    		
			                    		'hide_empty' => 0,
			                    		'include'    => $homeParam	
			                    	) );
			                    	$count = 1;	
			                    	foreach( (array) $terms as $term){
			                            echo '<h4 class="kategorie-name"><a class="kategorie-link" href="' . esc_url( get_term_link( $term, $term->taxonomy ) ) . '?filter=buecher">';
			                            echo sprintf(__('%s', 'callwey'), $term->name);
			                            echo '</a></h4>';
			                    	}
			                    ?>
								<a class="view-all-categories text-center" href="<?php bloginfo('url'); ?>/buecher"><?php _e('Zu ALLEN BUCHKATEGORIEN', 'callwey'); ?></a>
							</div>
						</section>
							
					<?php endif; ?>
					
					</main>
				</div><!-- #primary -->
			
			</div><!-- .first-block.row -->

	<?php endwhile; ?>
<?php endif; wp_reset_postdata();?>	
