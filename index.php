<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

<div class="row">

	<h4 class="site-section-title text-center"><?php _e('Callwey', 'callwey'); ?></h4>
	
	<h3 class="site-section text-center"><?php _e('Blog', 'callwey'); ?></h3>

	<?php if ( is_tttdevice('desktop') ): ?>
	<div id="primary" class="content-area large-13 medium-13 small-18 columns">
		<main id="main" class="site-main row" role="main">
	<?php else: ?>
	<div id="primary" class="content-area medium-18 columns">
		<main id="main" class="site-main row" role="main">
	<?php endif; ?>
        <?php rewind_posts(); ?>
        <?php wp_reset_query(); ?>
        <?php
            $query = new WP_Query(array( 
                'post_type' => array('post'),
                'offset' => 0,
                'posts_per_page' => 3,
                'paged' => 0,
                'order' => 'DESC',
                'orderby' => 'date',
                'ignore_sticky_posts' => 1,
            ));
        ?>
		<?php if ( $query->have_posts() ) : ?>
            <?php $_count = 1; ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<?php get_template_part( 'partials/content', 'content' ); ?>

				<?php if ($_count == 1) : ?>
				<div class="medium-16 medium-centered columns blog-space">
					<?php $ajax_adrotate_group = of_get_option('ajax_adrotate_group'); ?>
					<?php if ($ajax_adrotate_group == '1'): /* RAND */ ?>
						<?php echo ajax_adrotate_group(round(rand(2,3))); ?>
					<?php elseif ($ajax_adrotate_group == '2'): /* EXTERNAL */ ?>
						<?php echo ajax_adrotate_group(2); ?>					
					<?php elseif ($ajax_adrotate_group == '3'): /* INTERNAL */ ?>
						<?php echo ajax_adrotate_group(3); ?>
					<?php elseif ($ajax_adrotate_group == '4'): /* OFF */ ?>
					<?php endif; ?>

					<div class="row"></div>
				</div>
				<?php endif; $_count++; ?>

                <?php wp_reset_postdata(); ?>

			
			<?php endwhile; ?>
		</main><!-- #main -->
		
			<?php //_s_content_nav( 'nav-below' ); ?>
		<?php if ( !is_tttdevice('mobile') ): ?>
			<div class="medium-16 medium-centered small-18 columns">
				<div class="medium-17 medium-push-2 small-18 small-push-0 columns">
		<?php else: ?>
			<div class="small-18 columns">
				<div class="row">
		<?php endif; ?>
					<div class="load-more"><a href="#" data-tttloadmore-do="blogposts" data-tttloadmore-to="#main"><div class="down"><?php _e('Weitere Blogbeiträge anzeigen', 'callwey') ?></div></a></div>
				</div>
			</div>
		<?php endif; ?>

	</div><!-- #primary -->

	<?php get_sidebar(); ?>
	
</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>
