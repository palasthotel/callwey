<?php
/**
 * The template for displaying Archive CTP-produkt TAX-kategorie.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>
<div class="row">

	<?php if ( have_posts() ) : ?>

	<header class="archive-header">
		<h4 class="site-section-title text-center">
			<?php _e('Callwey', 'callwey'); ?>
		</h4>
		<h1 class="archive-title text-center">
			<?php $term_title = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term_title->name; ?>
		</h1>
	</header><!-- .archive-header -->



		<?php if ( is_tttdevice('desktop') ): ?>
		<div id="primary" class="content-area large-13 medium-13 columns">
			<main id="main" class="site-main row" role="main">
				<section id="bestseller" class="large-16 large-centered medium-16 medium-centered columns ">
		<?php else: ?>
		<div id="primary" class="content-area medium-18 columns">
			<main id="main" class="site-main row" role="main">
				<section id="bestseller" class="medium-16 medium-centered small-16 small-centered columns ">
				<?php if ( is_tttdevice('mobile') ): ?>
					<h2 class="section-title text-left">
				<?php else: ?>
					<h2 class="section-title text-center">
				<?php endif; ?>
						<?php _e('Bestseller und Neuheiten','callwey'); ?>
					</h2>
		<?php endif; ?>
				<?php
                    $_post_types_filter = array('produkt','ebook');
                    if (isset($_REQUEST['filter'])) {
                        if ( $_REQUEST['filter'] == 'buecher' ) {
                            unset( $_post_types_filter[1]);
                            ?>
                            <script type="text/javascript">
                            var _cwm = _cwm || [];
                            _cwm.push(['menu','#menu-item-4']);
                            </script>
                            <?php
                        }
                        elseif ( $_REQUEST['filter'] == 'ebooks' ) {
                            unset( $_post_types_filter[0]);
                            ?>
                            <script type="text/javascript">
                            var _cwm = _cwm || [];
                            _cwm.push(['menu','#menu-item-12']);
                            _cwm.push(['flag','ebook']);
                            </script>
                            <?php
                        }
                    }


					$products = array(
							'post_type'	 =>	$_post_types_filter,
							'posts_per_page' => 7,
							'orderby' => 'date',
							'order' => 'DESC',
							'post_status' => 'publish',
							'tax_query' => array (
                                'relation' => 'AND',
						    	array (
						    		'taxonomy' => 'kategorie',
						    		'field' => 'id',
						    		'terms' => array( 19, $term_title->term_id ),
								    'operator' => 'AND'
						    	)
						    )						
						);
					
					$products_query = new WP_Query($products);
				?>
				<div class="row">
					<?php if ($products_query->have_posts()) : ?>
						<ul id="filter-products" class="large-block-grid-4 medium-block-grid-3 small-block-grid-2">
						<?php if ( is_tttdevice('desktop') ): ?>
							<li>
								<h2 class="section-title text-left"><?php _e('Bestseller und Neuheiten','callwey'); ?></h2>
								<h3 class="section-subtitle">
									<?php $term_title = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term_title->name; ?>
								</h3>
							</li>
						<?php endif; ?>
						<?php while ($products_query->have_posts()) : $products_query->the_post();?>
							<li>
								<?php get_template_part( 'partials/content', 'home-bestseller' ); ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; wp_reset_postdata();?>
				</div>
				</section>
				
				<div id="adspace" class="large-16 large-centered medium-16 medium-centered small-18 columns "><?php echo ajax_adrotate_group(1); ?></div>

		<?php if ( is_tttdevice('desktop') ): ?>
				<section id="filtered-produkts" class="large-16 large-centered medium-16 medium-centered columns ">
		<?php else: ?>
				<section id="filtered-produkts" class="medium-16 medium-centered small-16 small-centered columns ">
				<?php if ( is_tttdevice('mobile') ): ?>
					<h2 class="section-title text-left">
				<?php else: ?>
					<h2 class="section-title text-center">
				<?php endif; ?>
						<?php _e('mehr bücher aus unserem shop','callwey'); ?>
					</h2>
		<?php endif; ?>

	                <style>
	                #homeproducts .hidden {
	                    display: none;
	
	                }
	                </style>

				<?php
					$products_rest = array(
						'post_type'	 =>	$_post_types_filter,
						'posts_per_page' => 7,
						'post_status' => 'publish',
					    'tax_query' => array (
                            'relation' => 'AND',
					    	array (
					    		'taxonomy' => 'kategorie',
					    		'field' => 'slug',
								'terms' => array('zeitschriften','bestseller'),
								'operator' => 'NOT IN'
					    	),
					        array (
                                'taxonomy' => 'kategorie',
                                'field' => 'id',
                                'terms' => array( $term_title->term_id )
                            )
					    )
					);
					$_count_rest = 0;					
					$products_rest_query = new WP_Query($products_rest);
				?>
				<div class="row">
					<?php if ($products_rest_query->have_posts()) : ?>
						<ul id="filter-products-rest" class="large-block-grid-4 medium-block-grid-3 small-block-grid-2">
						<?php if ( is_tttdevice('desktop') ): ?>
							<li>
								<h2 class="section-title text-left"><?php _e('mehr<br>bücher aus<br>unserem<br>shop','callwey'); ?></h2>
								<h3 class="section-subtitle">
									<?php $term_title = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term_title->name; ?>
								</h3>
							</li>
						<?php endif; ?>
						<?php while ($products_rest_query->have_posts()) : $products_rest_query->the_post(); $_count++; ?>
							<li class="<?php echo ( $_count_rest <= 3 ? '':'hidden' ); ?>">
								<?php get_template_part( 'partials/content', 'home-bestseller' ); ?>
							</li>
						<?php endwhile; ?>
						</ul>
						<div class="load-more">
                            <a href="#" data-tttloadmore-do="taxonomyproducts" data-tttloadmore-to="#filter-products-rest" data-tttloadmore-args="post_type:<?php echo implode(',',$_post_types_filter); ?>;term_id:<?php echo $term_title->term_id; ?>;">
                                <div class="down">
                                	<?php
				                    $_post_types_filter = array('produkt','ebook');
				                    if (isset($_REQUEST['filter'])) {
				                        if ( $_REQUEST['filter'] == 'buecher' ) {
				                            unset( $_post_types_filter[1]);
				                            ?>
												<?php _e('Mehr Bücher anzeigen', 'callwey') ?>
				                            <?php
				                        }
				                        elseif ( $_REQUEST['filter'] == 'ebooks' ) {
				                            unset( $_post_types_filter[0]);
				                            ?>
												<?php _e('Mehr eBooks anzeigen', 'callwey') ?>
				                            <?php
				                        }
				                    }
				                    else {
				                        unset( $_post_types_filter[0]);
				                        ?>
											<?php _e('Mehr Books anzeigen', 'callwey') ?>
				                        <?php
				                    }
				                    ?>
                                </div>
                            </a>
                        </div>
					<?php endif; wp_reset_postdata();?>
				</div>
				</section>
							
			</main>
		</div>
		<?php get_sidebar('kategorie'); ?>





	<?php else : ?>

		<?php get_template_part( 'no-results', 'archive' ); ?>

	<?php endif; ?>

</div>
<?php get_template_part( 'related-site-content' ); ?>
<?php get_footer(); ?>
