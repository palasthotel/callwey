<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); 
$cc = get_the_content();
?>
<div class="row">


	
	<header class="archive-header">
		<h4 class="site-section-title text-center">
			<?php _e('Callwey', 'callwey'); ?>
		</h4>
		<h1 class="archive-title text-center">
			<?php _e('Organisation', 'callwey') ?>
		</h1>
		<?php if($cc != '') : ?>
		<div class="medium-17 medium-centered columns">
			<div class="page-description medium-18 columns">
				<hr>
				<?php the_content(); ?>
			</div>
		</div>
		<?php else: ?>
		<?php endif; ?>
	</header><!-- .page-header -->

	<section id="primary" class="content-area medium-17 medium-centered columns">
		<main id="main" class="site-main row" role="main">
			<?php                    
        	$terms = get_terms( 'group', array(
        		'hide_empty' => 0,
				'exclude' => array(79),
				'include' => array(87),								
        	) );
        	foreach( (array) $terms as $term) : ?>
        	<div id="ID-<?php echo sprintf(__('%s', 'callwey'), $term->term_id); ?>" class="medium-18 columns">
				<h4 class="section-title text-left"><?php echo sprintf(__('%s', 'callwey'), $term->name); ?></h4>
				<?php
					$staff = array(
						'post_type'	 =>	'staff',
						'posts_per_page' => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'group',
								'field' => 'slug',
								'terms' => $term
							)
						)
					);					
					$staff_query = new WP_Query($staff);
				?>
				<?php if ($staff_query->have_posts()) : ?>
					<ul class="medium-block-grid-2 members-area">
					<?php while ($staff_query->have_posts()) : $staff_query->the_post(); ?>
						<?php
							$position = get_post_meta( $post->ID, '_clwy_position', true );
							$email = get_post_meta( $post->ID, '_clwy_email', true );
							$fon = get_post_meta( $post->ID, '_clwy_fon', true );
							$fax = get_post_meta( $post->ID, '_clwy_fax', true );
							$cc = get_the_content();	
						?>
						<li>
							<div class="large-10 medium-18 small-18 columns member-avatar"><?php the_post_thumbnail('staff'); ?></div>
							<div class="large-8 medium-18 small-18 columns member-details">
								<div class="member-details-table">
									<div class="member-details-cell">
										<ul class="vcard">
											<li class="fn"><?php the_title(); ?></li>
										<?php if ( get_post_meta( $post->ID, '_clwy_position', true ) ): ?>
											<li class="position"><?php echo $position; ?></li>
										<?php endif; ?>
										<?php if ( get_post_meta( $post->ID, '_clwy_email', true ) ): ?>
											<li class="email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
										<?php endif; ?>
										<?php if ( get_post_meta( $post->ID, '_clwy_fon', true ) ): ?>
											<li class="tel"><?php echo $fon; ?></li>
										<?php endif; ?>
										<?php if ( get_post_meta( $post->ID, '_clwy_fax', true ) ): ?>
											<li class="fax"><?php echo $fax; ?></li>
										<?php endif; ?>
										</ul>
									</div>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; wp_reset_query(); ?>
			</div>				
        	<?php endforeach; ?>
        	<div class="medium-18 columns">
        	<ul class="medium-block-grid-2">
	            <?php                    
	        	$terms2 = get_terms( 'group', array(
	        		'orderby'    => 'term_order',
	        		'hide_empty' => 0,  
	        		'exclude'	=> array(87,139), //IMPORTANT: check the group "Ihre ansprechpartner im verlag" taxonomy id, STAGE (137) have a different one than LIVE (139).
	        	) );
				foreach( (array) $terms2 as $term2) : ?>
	        	<li>
	        	<div id="ID-<?php echo sprintf(__('%s', 'callwey'), $term2->term_id); ?>">
					<h4 class="section-title text-left"><?php echo sprintf(__('%s', 'callwey'), $term2->name); ?></h4>
					<?php
						$staff2 = array(
							'post_type'	 =>	'staff',
							'posts_per_page' => -1, 
							'tax_query' => array(
								array(
									'taxonomy' => 'group',
									'field' => 'slug',
									'terms' => $term2
								)
							)
						);					
						$staff2_query = new WP_Query($staff2);
					?>
					<ul class="medium-block-grid-1 members-area">
					<?php if ($staff2_query->have_posts()) : ?>
	
						<?php while ($staff2_query->have_posts()) : $staff2_query->the_post(); ?>
							<?php
								$position = get_post_meta( $post->ID, '_clwy_position', true );
								$email = get_post_meta( $post->ID, '_clwy_email', true );
								$fon = get_post_meta( $post->ID, '_clwy_fon', true );
								$fax = get_post_meta( $post->ID, '_clwy_fax', true );
								$cc = get_the_content();	
							?>
							<li>
								<div class="medium-10 columns member-avatar row"><?php the_post_thumbnail('staff'); ?></div>
								<div class="medium-8 columns member-details">
									<div class="member-details-table">
										<div class="member-details-cell">
											<ul class="vcard">
												<li class="fn"><?php the_title(); ?></li>
											<?php if ( get_post_meta( $post->ID, '_clwy_position', true ) ): ?>
												<li class="position"><?php echo $position; ?></li>
											<?php endif; ?>
											<?php if ( get_post_meta( $post->ID, '_clwy_email', true ) ): ?>
												<li class="email"><?php echo $email; ?></li>
											<?php endif; ?>
											<?php if ( get_post_meta( $post->ID, '_clwy_fon', true ) ): ?>
												<li class="tel"><?php echo $fon; ?></li>
											<?php endif; ?>
											<?php if ( get_post_meta( $post->ID, '_clwy_fax', true ) ): ?>
												<li class="fax"><?php echo $fax; ?></li>
											<?php endif; ?>
											</ul>
										</div>
									</div>
								</div>
							</li>
						<?php endwhile; ?>
					<?php endif; wp_reset_query(); ?>
						</ul>				
				</div>
	        	</li>				
				<?php endforeach; ?>
        	</ul>
        	</div>
		</main><!-- #main -->
	</section><!-- #primary -->

</div>
<?php get_template_part( 'custom-related-content' ); ?>
<?php get_footer(); ?>
