	<div class="second-block row">

	<?php if ( is_tttdevice('desktop') ): ?>
		<div id="secondary-left" class="widget-area medium-4 medium-offset-1 columns show-for-large-up" role="complementary">
			<?php do_action( 'before_sidebar' ); ?>
			<?php if ( ! dynamic_sidebar( 'sidebar-home-left' ) ) : ?>
			<?php endif; // end sidebar widget area ?>
		</div><!-- #secondary-left -->
	<?php endif; ?>

	<?php if ( is_tttdevice('mobile') ): ?>
	<?php else: ?>
		<?php if ( is_tttdevice('tablet') ): ?>	
			<div id="complementary" class="content-area medium-18 columns">
				<main id="secondary-main" class="site-main row" role="main">
                    <div class="medium-15 medium-centered columns">
                    	<h2 class="section-title text-center row"><?php _e('lassen sie sich inspirieren', 'callwey'); ?></h2>
						<h3 class="section-subtitle text-center row"><?php _e('News-Stream', 'callwey'); ?></h3>                    	
                    </div>
                    
					<section id="blogs-posts" class="medium-15 medium-centered columns">
		<?php else: ?>	
			<div id="complementary" class="content-area large-13 medium-13 small-18 columns">
				<main id="secondary-main" class="site-main row" role="main">
					<section id="blogs-posts" class="large-16 large-centered medium-16 medium-centered small-16 small-centered columns">
		<?php endif; ?>
						<div class="row">
						<?php if ( is_tttdevice('desktop') ): ?>						
							<?php
	
								$masonry_produkt_query = new WP_Query(
                                    array( 
                                        'ignore_sticky_posts' => 1,
										'post_type' => array('produkt'),
                                        'post_status' => array('publish'),
										'posts_per_page' => 3,
										'orderby' => 'rand',
										'tax_query' => array(
											'relation' => 'AND',									
											array(
												'taxonomy' => 'kategorie',
												'field' => 'id',
												'terms' => array(5,19),
												'operator' => 'NOT IN'
											)
										)									
									)
								);

								$masonry_ebook_query = new WP_Query(
                                    array( 
                                        'ignore_sticky_posts' => 1,
										'post_type' => array('ebook'),
                                        'post_status' => array('publish'),
										'posts_per_page' => 2,
										'orderby' => 'rand',
										'tax_query' => array(
											'relation' => 'AND',									
											array(
												'taxonomy' => 'kategorie',
												'field' => 'id',
												'terms' => array(5,19),
												'operator' => 'NOT IN'
											)
										)									
									)
								);


								$masonry_posts_query = new WP_Query(
                                    array( 
                                        'ignore_sticky_posts' => 1,
                                        'post_type' => 'post',
                                        'posts_per_page' => 7,
                                        'orderby' => 'rand',
                                        'post_status' => array('publish'),
									)
								);
							?>
						<?php elseif ( is_tttdevice('tablet') ): ?>
							<?php
	
								$masonry_produkt_query = new WP_Query(
                                    array( 
                                        'ignore_sticky_posts' => 1,
										'post_type' => array('produkt'),
                                        'post_status' => array('publish'),
										'posts_per_page' => 1,
										'orderby' => 'rand',
										'tax_query' => array(
											'relation' => 'AND',									
											array(
												'taxonomy' => 'kategorie',
												'field' => 'id',
												'terms' => array(5,19),
												'operator' => 'NOT IN'
											)
										)									
									)
								);

								$masonry_ebook_query = new WP_Query(
                                    array( 
                                        'ignore_sticky_posts' => 1,
										'post_type' => array('ebook'),
                                        'post_status' => array('publish'),
										'posts_per_page' => 1,
										'orderby' => 'rand',
										'tax_query' => array(
											'relation' => 'AND',									
											array(
												'taxonomy' => 'kategorie',
												'field' => 'id',
												'terms' => array(5,19),
												'operator' => 'NOT IN'
											)
										)									
									)
								);


								$masonry_posts_query = new WP_Query(
                                    array( 
                                        'ignore_sticky_posts' => 1,
                                        'post_type' => 'post',
                                        'posts_per_page' => 4,
                                        'orderby' => 'rand',
                                        'post_status' => array('publish'),
									)
								);
							?>
						<?php endif; ?>							
							<?php
								
								$masonrytresult = new WP_Query();
								
								// start putting the contents in the new object
								$masonrytresult->posts = array_merge( $masonry_posts_query->posts, $masonry_produkt_query->posts, $masonry_ebook_query->posts );
                                shuffle( $masonrytresult->posts );
								
								// here you might wanna apply some sort of sorting on $result->posts
								
								// we also need to set post count correctly so as to enable the looping
								$masonrytresult->post_count = count( $masonrytresult->posts );
								
							?>
							<?php if ($masonrytresult->have_posts()) : ?>
								<div id="masonryContainer">
									<?php if ( is_tttdevice('desktop') ): ?>
										<div class="masonry-brick">
											<h2 class="section-title"><?php _e('lassen<br>sie<br>sich<br>inspirieren', 'callwey'); ?></h2>
											<h3 class="section-subtitle"><?php _e('News-Stream', 'callwey'); ?></h3>
										</div>
									<?php endif; ?>
									<?php while ($masonrytresult->have_posts()) : $masonrytresult->the_post(); ?>
										<?php get_template_part( 'partials/content', 'home-masonrybrick' ); ?>
									<?php endwhile; ?>
								</div>
								<div class="load-more"><a href="#" data-tttloadmore-do="homenewsfeed" data-tttloadmore-to="#masonryContainer"><div class="down"><?php _e('Weitere Inspirationen anzeigen', 'callwey') ?></div><div class="up"><?php _e('Close', 'callwey') ?></div></a></div>							
							<?php endif; wp_reset_postdata();?>
						</div>
					</section>
				</main>
			</div><!-- #complementary -->
			
		</div><!-- .second-block.row -->
	<?php endif; ?>
